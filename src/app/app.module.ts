import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { MosstCardModule } from './mosst-card/mosst-card.module';

const mosstCardModuleConfig = {
  api: {
    host: '',
    sign: {
      key: '',
      memberPointId: 0
    }
  }
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MosstCardModule.forRoot(mosstCardModuleConfig)
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
