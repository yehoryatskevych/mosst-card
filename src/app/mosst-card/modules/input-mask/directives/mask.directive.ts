import { Directive, EventEmitter, Input, Output, OnInit, ElementRef } from '@angular/core';
import { NgModel, AbstractControl } from '@angular/forms';

import { MaskWorker } from '../workers/mask.worker';

import { IMask } from '../interfaces';

export function createMaskWorker(mask: IMask): MaskWorker {
  return new MaskWorker(
    mask.mask,
    mask.output,
    mask.allowedInputs,
    mask.advancedSetter,
    mask.replacement,
    mask.showConstants
  );
}

export function format(mask: IMask) {
  return (value: string) => {
    const maskWorker = createMaskWorker(mask);
    return maskWorker.useMask(value);
  };
}

@Directive({
  selector: '[inputMask]',
  providers: [NgModel],
  host: {
    '(ngModelChange)': 'onInputChange($event)',
    '(keydown)': 'onKeyDown($event)'
  }
})
export class InputMaskDirective implements OnInit {
  @Input()
  public formControl: AbstractControl;

  @Input()
  public mask: IMask;

  @Output()
  public valueChanges: EventEmitter<string> = new EventEmitter<string>();

  protected previousValue = '';
  protected maskWorker: MaskWorker;

  public constructor(protected model: NgModel, protected el: ElementRef) {
    this.valueChanges.subscribe(val => {
      if (this.formControl) {
        this.formControl.setValue(val, {
          onlySelf: true,
          emitEvent: false,
          emitModelToViewChange: false,
          emitViewToModelChange: false
        });
      }
    });
  }

  public ngOnInit() {
    if (!this.maskWorker) {
      if (this.mask) {
        this.initMaskWorker();
      }
    }

    if (this.formControl) {
      if (this.mask.maskedValue) {
        const maskedValue = this.maskWorker.useMask(this.formControl.value || '');
        this.model.valueAccessor.writeValue(maskedValue);
        this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
      } else {
        this.formControl.setValue(this.maskWorker.useMask(this.formControl.value || ''));
      }
    }
  }

  public onKeyDown(event) {
    if (event.key === 'Delete' && this.customDelete(event)) {
      event.preventDefault();
      return;
    }

    if (event.key === 'Backspace' && this.customBackspace(event)) {
      event.preventDefault();
      return;
    }

    const inputStart = this.maskWorker.getStartPosition();

    const isHomeKey = event.key === 'Home';
    const isArrowLeftKey = event.key === 'ArrowLeft';

    const {selectionStart, selectionEnd, value} = event.target;
    if (!selectionStart && selectionEnd === value.length) {
      return;
    }

    if (selectionStart <= inputStart || isHomeKey) {
      this.el.nativeElement.selectionStart = inputStart;
      this.el.nativeElement.selectionEnd = inputStart;

      if (!isHomeKey && isArrowLeftKey) {
        event.preventDefault();
      }
    }

    if (isHomeKey) {
      event.preventDefault();
    }
  }

  public onInputChange(event) {
    if (!this.maskWorker) {
      if (!this.mask) {
        return;
      }

      this.initMaskWorker();
    }

    const start = this.el.nativeElement.selectionStart;
    const maskedValue = this.maskWorker.useMask(event);

    this.model.valueAccessor.writeValue(maskedValue);

    const deletion = this.previousValue.length >= maskedValue.length;
    this.el.nativeElement.selectionStart = this.maskWorker.getCursorPositionWithOffset(start, deletion);
    this.el.nativeElement.selectionEnd = this.maskWorker.getCursorPositionWithOffset(start, deletion);

    this.previousValue = maskedValue;
    this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
  }

  protected customDelete(event, left = false): boolean {
    const {selectionStart, selectionEnd} = event.target;
    const inputStart = this.maskWorker.getStartPosition();

    let value = '';
    let movePos = selectionStart;
    if (selectionStart > 0 && selectionEnd <= event.target.value.length) {
      const offset = left
        ? -this.maskWorker.getCursorOffsetLeft(selectionStart) - 1
        : this.maskWorker.getCursorOffsetRight(selectionStart + 1);

      const pos = selectionStart + offset;

      if (pos < 0) {
        return true;
      }

      if (left && pos >= this.maskWorker.getStartPosition()) {
        movePos = pos;
      }

      const arr = event.target.value.split('');
      arr.splice(pos, 1);
      value = arr.join('');
    } else {
      movePos = inputStart;
    }

    const maskedValue = this.maskWorker.useMask(value);

    this.model.valueAccessor.writeValue(maskedValue);

    event.target.selectionStart = movePos;
    event.target.selectionEnd = movePos;

    this.previousValue = maskedValue;
    this.valueChanges.emit(this.maskWorker.unmask(maskedValue));

    return true;
  }

  protected customBackspace(event): boolean {
    return this.customDelete(event, true);
  }

  protected initMaskWorker(): void {
    this.maskWorker = createMaskWorker(this.mask);
  }
}
