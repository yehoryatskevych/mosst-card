export class MaskWorker {
  private allowedSymbols: RegExp;
  private constantPositions: number[];

  public constructor(
    private mask: any = (val) => val,
    private output: any = (val) => val,
    private allowedInputs: string = '.*',
    private advancedSetter = null,
    private replacement = 'X',
    private showConstants = false
  ) {
    this.allowedSymbols = new RegExp(allowedInputs);
    this.constantPositions = this.getConstantPositions(mask);
  }

  private removeNotAllowedSymbolsFromString(inputString = '', ignoreReplacement = false) {
    let stringWithoutNotAllowedSymbols = '';

    for (let i = 0; i < inputString.length; i++) {
      if (this.allowedSymbols.test(inputString[i]) || (ignoreReplacement && inputString[i] === this.replacement)) {
        stringWithoutNotAllowedSymbols += inputString[i];
      }
    }

    return stringWithoutNotAllowedSymbols;
  }

  private restoreToTextOutput(string) {
    const meaningPart = this.getMeaningPart(string);
    let result = this.output;

    for (let i = 0; i < meaningPart.length; i++) {
      result = result.replace(/X/, meaningPart[i]);
    }

    result = result.replace(/X/g, '');
    return result;
  }

  private getMeaningPart(string) {
    let meaningPart = '';
    if (string.length > this.mask.length) {
      string = string.slice(0, this.mask.length);
    }
    for (let i = 0; i < string.length; i++) {
      if (this.mask[i] === 'X') {
        meaningPart += string[i];
      }
    }
    return meaningPart;
  }

  private removePredefinedOutput(str) {
    const getFirstMatchTail = (_str: string, symbol: string) => {
      for (let i = 0; i < _str.length; i++) {
        if (_str[i] === symbol) {
          return _str.slice(i);
        }
      }
      return '';
    };

    const equalPartsLength = (string1, string2) => {
      let length = 0;
      for (let i = 0; i < string1.length; i++) {
        if (string1[i] === string2[i]) {
          length++;
        } else {
          return length;
        }
      }
      return length;
    };

    const outputTemplate = this.removeNotAllowedSymbolsFromString(this.output);
    const outputTail = getFirstMatchTail(outputTemplate, str[0]);
    return str.slice(equalPartsLength(outputTail, str));
  }

  private cutTail(string) {
    const getPreviousReplacerIndex = (index) => {
      if (!index) {
        return 0;
      }

      for (let i = index - 1; i > 0; i--) {
        if (this.mask[i] === this.replacement) {
          return i + 1;
        }
      }
      return index;
    };

    const re = new RegExp('X', 'g');
    const findReplacer = re.exec(string);

    return findReplacer ? string.slice(0, getPreviousReplacerIndex(findReplacer.index)) : string;
  }


  private useTextMask(string) {
    const allowedLength = this.mask.length - this.mask.replace(/X/g, '').length;
    const stringWithoutNotAllowedSymbols = this.removeNotAllowedSymbolsFromString(string);
    string = this.removePredefinedOutput(stringWithoutNotAllowedSymbols).slice(0, allowedLength);
    let currentString = this.mask;

    for (let i = 0; i < allowedLength; i++) {
      if (i < string.length) {
        currentString = currentString.replace(/X/, string[i]);
      } else if (this.showConstants) {
        currentString = currentString.replace(/X/, '');
      }
    }
    currentString = this.cutTail(currentString);
    return currentString;
  }

  private applyAdvancedSetter(string) {
    if (typeof this.advancedSetter === 'function') {
      return this.advancedSetter(string);
    }

    if (this.advancedSetter instanceof RegExp) {
      return string.match(this.advancedSetter) ? string : '';
    }

    return string;
  }

  public useMask(string = '') {
    string = string.toString();
    if (typeof this.mask === 'function') {
      string = this.removeNotAllowedSymbolsFromString(string);
      return this.mask(string);
    }

    return this.useTextMask(this.applyAdvancedSetter(string));
  }

  public unmask(string = '') {
    if (typeof this.output === 'function') {
      return this.output(string);
    }
    return this.restoreToTextOutput(string);
  }

  private getConstantPositions(str) {
    if (typeof str === 'function') {
      return [];
    }

    const constantPositions = [];

    for (let i = 0; i < str.length; i++) {
      if (str[i] !== 'X') {
        constantPositions.push(i + 1);
      }
    }

    return constantPositions;
  }

  public getCursorPositionWithOffset(pos, deletion: boolean = false): number {
    const res = deletion
      ? pos - this.getCursorOffsetRight(pos)
      : pos + this.getCursorOffsetRight(pos);

    const startPos = this.getStartPosition();

    return res < startPos ? startPos : res;
  }

  public getStartPosition(): number {
    if (!this.constantPositions.length || this.constantPositions[0] !== 1) {
      return 0;
    }

    let i;
    for (i = 1; i < this.constantPositions.length; ++i) {
      if (this.constantPositions[i] - this.constantPositions[i - 1] !== 1) {
        break;
      }
    }

    return this.constantPositions[i - 1];
  }

  public getCursorOffsetRight(pos) {
    const constantMatchIndex = this.constantPositions.indexOf(pos);

    if (constantMatchIndex !== -1) {
      let i = constantMatchIndex;

      while (i < this.constantPositions.length) {
        if (this.constantPositions[i + 1] - this.constantPositions[i] !== 1) {
          break;
        }

        i++;
      }

      return i - constantMatchIndex + 1;
    }

    return 0;
  }

  public getCursorOffsetLeft(pos): number {
    const constantMatchIndex = this.constantPositions.indexOf(pos);

    if (constantMatchIndex !== -1) {
      let i = constantMatchIndex;

      while (i >= 0) {
        if (this.constantPositions[i] - this.constantPositions[i - 1] !== 1) {
          break;
        }

        i--;
      }

      return constantMatchIndex - i + 1;
    }

    return 0;
  }
}
