export { InputMaskModule } from './input-mask.module';

export {
  format
} from './directives/mask.directive';

export {
  IMask
} from './interfaces';
