import { Injectable } from '@angular/core';

import locales from '../locales';

import * as _ from 'lodash';

@Injectable()
export class LocaleService {
  private currentLang = 'ru';

  private locales: any;

  public constructor() {
    this.locales = locales;
  }

  public translate(value, args) {
    let currentValue = _.get(this.locales[this.currentLang], value) || value;

    args.forEach((arg, index) => {
      currentValue = currentValue.replace(`%S${index + 1}`, arg);
    });

    return currentValue;
  }

  public updateLocales(customLocales = {}) {
    for (const locale in customLocales) {
      if (customLocales.hasOwnProperty(locale)) {
        this.locales[locale] = Object.assign({}, this.locales[locale], customLocales[locale]);
      }
    }
  }

  public setLocale(lang: string) {
    if (Object.keys(this.locales).indexOf(lang) === -1) {
      return;
    }

    this.currentLang = lang;
  }
}
