import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

import * as _ from 'lodash';
import * as CryptoJS from 'crypto-js';

import { ConfigService } from './config.service';

import {
  API_CARD_LIST,
  API_CARD_ADD,
  API_CARD_DELETE
} from '../constants/endpoints';

import { ICard, IUser } from '../interfaces';

const CURRENT_YEAR_PREFIX = '20';

@Injectable()
export class ApiService {
  private apiHost: string;
  private user: IUser;

  public constructor(
    private http: Http,
    private configService: ConfigService
  ) {
    this.apiHost = configService.getApiHost();
  }

  public setUser(user: IUser): void {
    this.user = <IUser>_.pick(user, ['profile_guid', 'profile_session_guid']);
  }

  public getCardList(): Observable<ICard[]> {
    return this.request({
      path: API_CARD_LIST,
      method: 'POST'
    }).map((res) => res.cards.map((card) => {
      return Object.assign({}, card, {
        expiration: card.expiration.split(`/${CURRENT_YEAR_PREFIX}`).join('/')
      });
    }));
  }

  public addCard(params: {alias: string, pan: string, exp: string}): Observable<any> {
    const card = Object.assign({}, params, {
      exp: params.exp.split('/').join(`/${CURRENT_YEAR_PREFIX}`)
    });

    return this.request({
      path: API_CARD_ADD,
      method: 'POST',
      body: {card}
    });
  }

  public deleteCard(params: {guid: string}): Observable<any> {
    return this.request({
      path: API_CARD_DELETE,
      method: 'POST',
      body: {card_guid: params.guid}
    });
  }

  private request(params: {
    path: string;
    method: string;
    body?: Object;
  }): Observable<any> {
    const {path, method, body} = params;

    const host = this.apiHost[this.apiHost.length - 1] === '/'
      ? this.apiHost.slice(0, this.apiHost.length - 1)
      : this.apiHost;

    const url = host + (path[0] === '/'
      ? path
      : ('/' + path)
    );

    const options = new RequestOptions({url, method, body: Object.assign(body || {}, {
      auth: this.getAuthSign()
    })});

    return this.http
      .request(url, options)
      .catch(() => {
        return Observable.throw({code: -1, message: 'Server error.'});
      })
      .flatMap((res: Response) => {
        const json = res.json();
        const {error} = json;

        if (res.status !== 200 || error) {
          if (error) {
            return Observable.throw(typeof(error) === 'object' ? error : {code: -1, message: error});
          } else {
            return Observable.throw(error || {code: -1, message: 'Invalid request.'});
          }
        }

        return Observable.of(json);
      })
      .catch(error => Observable.throw(error));
  }

  private getAuthSign(): any {
    const {memberPointId, key} = this.configService.getSign();

    const dateISO = (new Date()).toISOString();

    return Object.assign({
      memberPointId,
      time: dateISO,
      sign: CryptoJS.MD5(memberPointId + dateISO + key).toString(CryptoJS.enc.Hex)
    }, this.user);
  }
}
