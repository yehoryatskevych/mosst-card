import { AbstractControl } from '@angular/forms';
import { CCValidator } from './CCValidator';

export function creditCardValidator(control: AbstractControl): {[s: string]: boolean} {
  const card: string = control.value.toString();
  if (card.length > 0) {
    // length test
    if (card.length < CCValidator.MIN_LENGTH) {
      return { 'minlength': true };
    }

    // general validation test
    if (!CCValidator.isValid(card)) {
      return { 'invalid': true };
    }
  }
}
