(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/forms'), require('@angular/http'), require('rxjs/Observable'), require('rxjs/add/operator/map'), require('rxjs/add/operator/mergeMap'), require('rxjs/add/operator/catch'), require('rxjs/add/observable/throw'), require('rxjs/add/observable/of'), require('lodash'), require('crypto-js')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/common', '@angular/forms', '@angular/http', 'rxjs/Observable', 'rxjs/add/operator/map', 'rxjs/add/operator/mergeMap', 'rxjs/add/operator/catch', 'rxjs/add/observable/throw', 'rxjs/add/observable/of', 'lodash', 'crypto-js'], factory) :
	(factory((global['mosst-card'] = {}),global.ng.core,global.ng.common,global.ng.forms,global.ng.http,global.Rx,global.Rx.Observable.prototype,global.Rx.Observable.prototype,global.Rx.Observable.prototype,global.Rx.Observable,global.Rx.Observable,global.lodash,global['crypto-js']));
}(this, (function (exports,core,common,forms,http,Observable,map,mergeMap,_catch,_throw,of,lodash,cryptoJs) { 'use strict';

var MosstCardModuleConfig = (function () {
    /**
     * @param {?} config
     */
    function MosstCardModuleConfig(config) {
        this.api = config.api;
    }
    return MosstCardModuleConfig;
}());
var ConfigService = (function () {
    /**
     * @param {?} config
     */
    function ConfigService(config) {
        this.config = config;
    }
    /**
     * @return {?}
     */
    ConfigService.prototype.getApiHost = function () {
        return this.config.api.host;
    };
    /**
     * @return {?}
     */
    ConfigService.prototype.getSign = function () {
        return Object.assign({}, this.config.api.sign);
    };
    return ConfigService;
}());
ConfigService.decorators = [
    { type: core.Injectable },
];
/**
 * @nocollapse
 */
ConfigService.ctorParameters = function () { return [
    { type: MosstCardModuleConfig, },
]; };
var API_CARD_LIST = '/card/list';
var API_CARD_ADD = '/card/store';
var API_CARD_DELETE = '/card/delete';
var CURRENT_YEAR_PREFIX = '20';
var ApiService = (function () {
    /**
     * @param {?} http
     * @param {?} configService
     */
    function ApiService(http$$1, configService) {
        this.http = http$$1;
        this.configService = configService;
        this.apiHost = configService.getApiHost();
    }
    /**
     * @param {?} user
     * @return {?}
     */
    ApiService.prototype.setUser = function (user) {
        this.user = (lodash.pick(user, ['profile_guid', 'profile_session_guid']));
    };
    /**
     * @return {?}
     */
    ApiService.prototype.getCardList = function () {
        return this.request({
            path: API_CARD_LIST,
            method: 'POST'
        }).map(function (res) { return res.cards.map(function (card) {
            return Object.assign({}, card, {
                expiration: card.expiration.split("/" + CURRENT_YEAR_PREFIX).join('/')
            });
        }); });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    ApiService.prototype.addCard = function (params) {
        var /** @type {?} */ card = Object.assign({}, params, {
            exp: params.exp.split('/').join("/" + CURRENT_YEAR_PREFIX)
        });
        return this.request({
            path: API_CARD_ADD,
            method: 'POST',
            body: { card: card }
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    ApiService.prototype.deleteCard = function (params) {
        return this.request({
            path: API_CARD_DELETE,
            method: 'POST',
            body: { card_guid: params.guid }
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    ApiService.prototype.request = function (params) {
        var path = params.path, method = params.method, body = params.body;
        var /** @type {?} */ host = this.apiHost[this.apiHost.length - 1] === '/'
            ? this.apiHost.slice(0, this.apiHost.length - 1)
            : this.apiHost;
        var /** @type {?} */ url = host + (path[0] === '/'
            ? path
            : ('/' + path));
        var /** @type {?} */ options = new http.RequestOptions({ url: url, method: method, body: Object.assign(body || {}, {
                auth: this.getAuthSign()
            }) });
        return this.http
            .request(url, options)
            .catch(function () {
            return Observable.Observable.throw({ code: -1, message: 'Server error.' });
        })
            .flatMap(function (res) {
            var /** @type {?} */ json = res.json();
            var error = json.error;
            if (res.status !== 200 || error) {
                if (error) {
                    return Observable.Observable.throw(typeof (error) === 'object' ? error : { code: -1, message: error });
                }
                else {
                    return Observable.Observable.throw(error || { code: -1, message: 'Invalid request.' });
                }
            }
            return Observable.Observable.of(json);
        })
            .catch(function (error) { return Observable.Observable.throw(error); });
    };
    /**
     * @return {?}
     */
    ApiService.prototype.getAuthSign = function () {
        var _a = this.configService.getSign(), memberPointId = _a.memberPointId, key = _a.key;
        var /** @type {?} */ dateISO = (new Date()).toISOString();
        return Object.assign({
            memberPointId: memberPointId,
            time: dateISO,
            sign: cryptoJs.MD5(memberPointId + dateISO + key).toString(cryptoJs.enc.Hex)
        }, this.user);
    };
    return ApiService;
}());
ApiService.decorators = [
    { type: core.Injectable },
];
/**
 * @nocollapse
 */
ApiService.ctorParameters = function () { return [
    { type: http.Http, },
    { type: ConfigService, },
]; };
var en = {};
var ru = {
    DELETE_CARD: {
        TITLE: 'Удалить карту',
        DESCRIPTION: 'После удаления карты вы не сможете использовать возможность автозаполнения номера карты',
        SUBMIT_BUTTON: 'Удалить карту',
        CANCEL_BUTTON: 'Отмена'
    },
    ADD_CARD: {
        TITLE: 'Добавить карту',
        DESCRIPTION: 'В названии карты должно быть минимум 3 неспециальных символа',
        SUBMIT_BUTTON: 'Добавить',
        CANCEL_BUTTON: 'Отмена'
    },
    CARD: {
        ALIAS: 'Название карты',
        PAN: 'Номер карты',
        EXP: 'Срок действия'
    }
};
var uk = {};
var locales = { en: en, ru: ru, uk: uk };
var LocaleService = (function () {
    function LocaleService() {
        this.currentLang = 'ru';
        this.locales = locales;
    }
    /**
     * @param {?} value
     * @param {?} args
     * @return {?}
     */
    LocaleService.prototype.translate = function (value, args) {
        var /** @type {?} */ currentValue = lodash.get(this.locales[this.currentLang], value) || value;
        args.forEach(function (arg, index) {
            currentValue = currentValue.replace("%S" + (index + 1), arg);
        });
        return currentValue;
    };
    /**
     * @param {?=} customLocales
     * @return {?}
     */
    LocaleService.prototype.updateLocales = function (customLocales) {
        if (customLocales === void 0) { customLocales = {}; }
        for (var /** @type {?} */ locale in customLocales) {
            if (customLocales.hasOwnProperty(locale)) {
                this.locales[locale] = Object.assign({}, this.locales[locale], customLocales[locale]);
            }
        }
    };
    /**
     * @param {?} lang
     * @return {?}
     */
    LocaleService.prototype.setLocale = function (lang) {
        if (Object.keys(this.locales).indexOf(lang) === -1) {
            return;
        }
        this.currentLang = lang;
    };
    return LocaleService;
}());
LocaleService.decorators = [
    { type: core.Injectable },
];
/**
 * @nocollapse
 */
LocaleService.ctorParameters = function () { return []; };
var MosstCardComponent = (function () {
    /**
     * @param {?} localeService
     * @param {?} apiService
     */
    function MosstCardComponent(localeService, apiService) {
        this.localeService = localeService;
        this.apiService = apiService;
        this.screen = 'select';
        this.onCardSelect = new core.EventEmitter();
        this.onAddCard = new core.EventEmitter();
        this.onCardDelete = new core.EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    MosstCardComponent.prototype.ngOnChanges = function (changes) {
        if (changes.user) {
            this.apiService.setUser(this.user);
        }
        if (changes.lang) {
            this.localeService.setLocale(this.lang);
        }
    };
    /**
     * @param {?} card
     * @return {?}
     */
    MosstCardComponent.prototype.addCard = function (card) {
        this.onAddCard.emit(card);
        this.setScreen('select');
    };
    /**
     * @param {?} card
     * @return {?}
     */
    MosstCardComponent.prototype.selectCard = function (card) {
        this.onCardSelect.emit(card);
        this.setScreen('select');
    };
    /**
     * @param {?} card
     * @return {?}
     */
    MosstCardComponent.prototype.deleteCard = function (card) {
        this.onCardDelete.emit(card);
        this.setScreen('select');
    };
    /**
     * @param {?} screen
     * @param {?=} card
     * @return {?}
     */
    MosstCardComponent.prototype.setScreen = function (screen, card) {
        if (card === void 0) { card = this.card; }
        this.screen = screen;
        this.card = card;
    };
    return MosstCardComponent;
}());
MosstCardComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'mosst-card',
                template: "\n    <app-select-card *ngIf=\"screen === 'select'\"\n      (onSelect)=\"selectCard($event)\"\n      (onDelete)=\"setScreen('delete', $event)\"\n      (onAdd)=\"setScreen('add')\"\n    ></app-select-card>\n\n    <app-add-card *ngIf=\"screen === 'add'\"\n      (onAdd)=\"addCard($event)\"\n      (onCancel)=\"setScreen('select')\"\n    ></app-add-card>\n\n    <app-delete-card *ngIf=\"screen === 'delete'\"\n      [data]=\"card\"\n      (onDelete)=\"deleteCard($event)\"\n      (onCancel)=\"setScreen('select')\"\n    ></app-delete-card>\n  ",
                styles: ["\n    @font-face {\n      font-family: \"Roboto\";\n      src: url(\"./assets/fonts/RobotoRegular/RobotoRegular.eot\");\n      src: url(\"./assets/fonts/RobotoRegular/RobotoRegular.eot?#iefix\") format(\"embedded-opentype\"), url(\"./assets/fonts/RobotoRegular/RobotoRegular.woff\") format(\"woff\"), url(\"./assets/fonts/RobotoRegular/RobotoRegular.ttf\") format(\"truetype\");\n      font-style: normal;\n      font-weight: 400; }\n\n    @font-face {\n      font-family: \"RobotoMedium\";\n      src: url(\"./assets/fonts/RobotoMedium/RobotoMedium.eot\");\n      src: url(\"./assets/fonts/RobotoMedium/RobotoMedium.eot?#iefix\") format(\"embedded-opentype\"), url(\"./assets/fonts/RobotoMedium/RobotoMedium.woff\") format(\"woff\"), url(\"./assets/fonts/RobotoMedium/RobotoMedium.ttf\") format(\"truetype\");\n      font-style: normal;\n      font-weight: 500; }\n\n    @font-face {\n      font-family: \"Material Design Icons\";\n      src: url(\"./assets/fonts/icons/materialdesignicons-webfont.eot?v=1.8.36\");\n      src: url(\"./assets/fonts/icons/materialdesignicons-webfont.eot?#iefix&v=1.8.36\") format(\"embedded-opentype\"), url(\"./assets/fonts/icons/materialdesignicons-webfont.woff2?v=1.8.36\") format(\"woff2\"), url(\"./assets/fonts/icons/materialdesignicons-webfont.woff?v=1.8.36\") format(\"woff\"), url(\"./assets/fonts/icons/materialdesignicons-webfont.ttf?v=1.8.36\") format(\"truetype\"), url(\"./assets/fonts/icons/materialdesignicons-webfont.svg?v=1.8.36#materialdesigniconsregular\") format(\"svg\");\n      font-weight: normal;\n      font-style: normal; }\n\n    * {\n      font-family: \"Roboto\", sans-serif; }\n\n    :host ::ng-deep {\n      /* ----------  Height ---------- */\n      /* ---------- Colors ---------- */\n      /* ---------- Backgrounds ---------- */\n      /* ---------- Shadows ---------- */\n      /* ---------- Radius ---------- */\n      /* ----------  Icons ---------- */\n      /* ----------  Icons ---------- */ }\n      :host ::ng-deep * {\n        max-height: 100000px;\n        -webkit-box-sizing: border-box;\n                box-sizing: border-box; }\n      :host ::ng-deep *:before,\n      :host ::ng-deep *:after {\n        -webkit-box-sizing: border-box;\n                box-sizing: border-box; }\n      :host ::ng-deep img {\n        vertical-align: top;\n        max-width: 100%;\n        border: 0;\n        -ms-interpolation-mode: bicubic; }\n      :host ::ng-deep form, :host ::ng-deep fieldset {\n        margin: 0;\n        padding: 0;\n        border-style: none; }\n      :host ::ng-deep input[type=text]::-ms-clear {\n        display: none; }\n      :host ::ng-deep input[type=\"search\"]::-webkit-search-decoration {\n        display: none; }\n      :host ::ng-deep button,\n      :host ::ng-deep textarea,\n      :host ::ng-deep input[type=text],\n      :host ::ng-deep input[type=file],\n      :host ::ng-deep input[type=submit],\n      :host ::ng-deep input[type=button],\n      :host ::ng-deep input[type=search],\n      :host ::ng-deep input[type=reset],\n      :host ::ng-deep input[type=password],\n      :host ::ng-deep input[type=\"search\"]::-webkit-search-cancel-button {\n        -webkit-appearance: none;\n        outline: none; }\n      :host ::ng-deep button,\n      :host ::ng-deep input[type=button],\n      :host ::ng-deep input[type=reset],\n      :host ::ng-deep input[type=submit] {\n        cursor: pointer; }\n      :host ::ng-deep textarea,\n      :host ::ng-deep input {\n        border-radius: 0; }\n      :host ::ng-deep input[type=image] {\n        padding: 0;\n        border: none; }\n      :host ::ng-deep button::-moz-focus-inner,\n      :host ::ng-deep input::-moz-focus-inner {\n        padding: 0;\n        border: 0; }\n      :host ::ng-deep *:focus {\n        outline: none; }\n      :host ::ng-deep main, :host ::ng-deep article, :host ::ng-deep aside, :host ::ng-deep details, :host ::ng-deep figcaption, :host ::ng-deep figure, :host ::ng-deep footer,\n      :host ::ng-deep header, :host ::ng-deep hgroup, :host ::ng-deep menu, :host ::ng-deep nav, :host ::ng-deep section {\n        display: block; }\n      :host ::ng-deep mark {\n        background: none; }\n      :host ::ng-deep blockquote, :host ::ng-deep q {\n        quotes: none; }\n        :host ::ng-deep blockquote:before, :host ::ng-deep blockquote:after, :host ::ng-deep q:before, :host ::ng-deep q:after {\n          content: '';\n          content: none; }\n      :host ::ng-deep p, :host ::ng-deep dl, :host ::ng-deep menu, :host ::ng-deep ol, :host ::ng-deep ul {\n        margin: 1em 0; }\n      :host ::ng-deep dd {\n        margin: 0 0 0 40px; }\n      :host ::ng-deep menu, :host ::ng-deep ol, :host ::ng-deep ul {\n        padding: 0; }\n      :host ::ng-deep nav ul, :host ::ng-deep nav ol {\n        padding: 0;\n        margin: 0;\n        list-style: none;\n        list-style-type: none; }\n      :host ::ng-deep table {\n        border-collapse: collapse;\n        border-spacing: 0; }\n      :host ::ng-deep h1, :host ::ng-deep h2, :host ::ng-deep h3, :host ::ng-deep h4, :host ::ng-deep h5, :host ::ng-deep h6 {\n        margin: 0; }\n      :host ::ng-deep .mdi-access-point:before {\n        content: \"\\F002\"; }\n      :host ::ng-deep .mdi-access-point-network:before {\n        content: \"\\F003\"; }\n      :host ::ng-deep .mdi-account:before {\n        content: \"\\F004\"; }\n      :host ::ng-deep .mdi-account-alert:before {\n        content: \"\\F005\"; }\n      :host ::ng-deep .mdi-account-box:before {\n        content: \"\\F006\"; }\n      :host ::ng-deep .mdi-account-box-outline:before {\n        content: \"\\F007\"; }\n      :host ::ng-deep .mdi-account-card-details:before {\n        content: \"\\F5D2\"; }\n      :host ::ng-deep .mdi-account-check:before {\n        content: \"\\F008\"; }\n      :host ::ng-deep .mdi-account-circle:before {\n        content: \"\\F009\"; }\n      :host ::ng-deep .mdi-account-convert:before {\n        content: \"\\F00A\"; }\n      :host ::ng-deep .mdi-account-edit:before {\n        content: \"\\F6BB\"; }\n      :host ::ng-deep .mdi-account-key:before {\n        content: \"\\F00B\"; }\n      :host ::ng-deep .mdi-account-location:before {\n        content: \"\\F00C\"; }\n      :host ::ng-deep .mdi-account-minus:before {\n        content: \"\\F00D\"; }\n      :host ::ng-deep .mdi-account-multiple:before {\n        content: \"\\F00E\"; }\n      :host ::ng-deep .mdi-account-multiple-minus:before {\n        content: \"\\F5D3\"; }\n      :host ::ng-deep .mdi-account-multiple-outline:before {\n        content: \"\\F00F\"; }\n      :host ::ng-deep .mdi-account-multiple-plus:before {\n        content: \"\\F010\"; }\n      :host ::ng-deep .mdi-account-network:before {\n        content: \"\\F011\"; }\n      :host ::ng-deep .mdi-account-off:before {\n        content: \"\\F012\"; }\n      :host ::ng-deep .mdi-account-outline:before {\n        content: \"\\F013\"; }\n      :host ::ng-deep .mdi-account-plus:before {\n        content: \"\\F014\"; }\n      :host ::ng-deep .mdi-account-remove:before {\n        content: \"\\F015\"; }\n      :host ::ng-deep .mdi-account-search:before {\n        content: \"\\F016\"; }\n      :host ::ng-deep .mdi-account-settings:before {\n        content: \"\\F630\"; }\n      :host ::ng-deep .mdi-account-settings-variant:before {\n        content: \"\\F631\"; }\n      :host ::ng-deep .mdi-account-star:before {\n        content: \"\\F017\"; }\n      :host ::ng-deep .mdi-account-star-variant:before {\n        content: \"\\F018\"; }\n      :host ::ng-deep .mdi-account-switch:before {\n        content: \"\\F019\"; }\n      :host ::ng-deep .mdi-adjust:before {\n        content: \"\\F01A\"; }\n      :host ::ng-deep .mdi-air-conditioner:before {\n        content: \"\\F01B\"; }\n      :host ::ng-deep .mdi-airballoon:before {\n        content: \"\\F01C\"; }\n      :host ::ng-deep .mdi-airplane:before {\n        content: \"\\F01D\"; }\n      :host ::ng-deep .mdi-airplane-landing:before {\n        content: \"\\F5D4\"; }\n      :host ::ng-deep .mdi-airplane-off:before {\n        content: \"\\F01E\"; }\n      :host ::ng-deep .mdi-airplane-takeoff:before {\n        content: \"\\F5D5\"; }\n      :host ::ng-deep .mdi-airplay:before {\n        content: \"\\F01F\"; }\n      :host ::ng-deep .mdi-alarm:before {\n        content: \"\\F020\"; }\n      :host ::ng-deep .mdi-alarm-check:before {\n        content: \"\\F021\"; }\n      :host ::ng-deep .mdi-alarm-multiple:before {\n        content: \"\\F022\"; }\n      :host ::ng-deep .mdi-alarm-off:before {\n        content: \"\\F023\"; }\n      :host ::ng-deep .mdi-alarm-plus:before {\n        content: \"\\F024\"; }\n      :host ::ng-deep .mdi-alarm-snooze:before {\n        content: \"\\F68D\"; }\n      :host ::ng-deep .mdi-album:before {\n        content: \"\\F025\"; }\n      :host ::ng-deep .mdi-alert:before {\n        content: \"\\F026\"; }\n      :host ::ng-deep .mdi-alert-box:before {\n        content: \"\\F027\"; }\n      :host ::ng-deep .mdi-alert-circle:before {\n        content: \"\\F028\"; }\n      :host ::ng-deep .mdi-alert-circle-outline:before {\n        content: \"\\F5D6\"; }\n      :host ::ng-deep .mdi-alert-octagon:before {\n        content: \"\\F029\"; }\n      :host ::ng-deep .mdi-alert-octagram:before {\n        content: \"\\F6BC\"; }\n      :host ::ng-deep .mdi-alert-outline:before {\n        content: \"\\F02A\"; }\n      :host ::ng-deep .mdi-all-inclusive:before {\n        content: \"\\F6BD\"; }\n      :host ::ng-deep .mdi-alpha:before {\n        content: \"\\F02B\"; }\n      :host ::ng-deep .mdi-alphabetical:before {\n        content: \"\\F02C\"; }\n      :host ::ng-deep .mdi-altimeter:before {\n        content: \"\\F5D7\"; }\n      :host ::ng-deep .mdi-amazon:before {\n        content: \"\\F02D\"; }\n      :host ::ng-deep .mdi-amazon-clouddrive:before {\n        content: \"\\F02E\"; }\n      :host ::ng-deep .mdi-ambulance:before {\n        content: \"\\F02F\"; }\n      :host ::ng-deep .mdi-amplifier:before {\n        content: \"\\F030\"; }\n      :host ::ng-deep .mdi-anchor:before {\n        content: \"\\F031\"; }\n      :host ::ng-deep .mdi-android:before {\n        content: \"\\F032\"; }\n      :host ::ng-deep .mdi-android-debug-bridge:before {\n        content: \"\\F033\"; }\n      :host ::ng-deep .mdi-android-studio:before {\n        content: \"\\F034\"; }\n      :host ::ng-deep .mdi-angular:before {\n        content: \"\\F6B1\"; }\n      :host ::ng-deep .mdi-angularjs:before {\n        content: \"\\F6BE\"; }\n      :host ::ng-deep .mdi-animation:before {\n        content: \"\\F5D8\"; }\n      :host ::ng-deep .mdi-apple:before {\n        content: \"\\F035\"; }\n      :host ::ng-deep .mdi-apple-finder:before {\n        content: \"\\F036\"; }\n      :host ::ng-deep .mdi-apple-ios:before {\n        content: \"\\F037\"; }\n      :host ::ng-deep .mdi-apple-keyboard-caps:before {\n        content: \"\\F632\"; }\n      :host ::ng-deep .mdi-apple-keyboard-command:before {\n        content: \"\\F633\"; }\n      :host ::ng-deep .mdi-apple-keyboard-control:before {\n        content: \"\\F634\"; }\n      :host ::ng-deep .mdi-apple-keyboard-option:before {\n        content: \"\\F635\"; }\n      :host ::ng-deep .mdi-apple-keyboard-shift:before {\n        content: \"\\F636\"; }\n      :host ::ng-deep .mdi-apple-mobileme:before {\n        content: \"\\F038\"; }\n      :host ::ng-deep .mdi-apple-safari:before {\n        content: \"\\F039\"; }\n      :host ::ng-deep .mdi-application:before {\n        content: \"\\F614\"; }\n      :host ::ng-deep .mdi-apps:before {\n        content: \"\\F03B\"; }\n      :host ::ng-deep .mdi-archive:before {\n        content: \"\\F03C\"; }\n      :host ::ng-deep .mdi-arrange-bring-forward:before {\n        content: \"\\F03D\"; }\n      :host ::ng-deep .mdi-arrange-bring-to-front:before {\n        content: \"\\F03E\"; }\n      :host ::ng-deep .mdi-arrange-send-backward:before {\n        content: \"\\F03F\"; }\n      :host ::ng-deep .mdi-arrange-send-to-back:before {\n        content: \"\\F040\"; }\n      :host ::ng-deep .mdi-arrow-all:before {\n        content: \"\\F041\"; }\n      :host ::ng-deep .mdi-arrow-bottom-left:before {\n        content: \"\\F042\"; }\n      :host ::ng-deep .mdi-arrow-bottom-right:before {\n        content: \"\\F043\"; }\n      :host ::ng-deep .mdi-arrow-compress:before {\n        content: \"\\F615\"; }\n      :host ::ng-deep .mdi-arrow-compress-all:before {\n        content: \"\\F044\"; }\n      :host ::ng-deep .mdi-arrow-down:before {\n        content: \"\\F045\"; }\n      :host ::ng-deep .mdi-arrow-down-bold:before {\n        content: \"\\F046\"; }\n      :host ::ng-deep .mdi-arrow-down-bold-circle:before {\n        content: \"\\F047\"; }\n      :host ::ng-deep .mdi-arrow-down-bold-circle-outline:before {\n        content: \"\\F048\"; }\n      :host ::ng-deep .mdi-arrow-down-bold-hexagon-outline:before {\n        content: \"\\F049\"; }\n      :host ::ng-deep .mdi-arrow-down-box:before {\n        content: \"\\F6BF\"; }\n      :host ::ng-deep .mdi-arrow-down-drop-circle:before {\n        content: \"\\F04A\"; }\n      :host ::ng-deep .mdi-arrow-down-drop-circle-outline:before {\n        content: \"\\F04B\"; }\n      :host ::ng-deep .mdi-arrow-expand:before {\n        content: \"\\F616\"; }\n      :host ::ng-deep .mdi-arrow-expand-all:before {\n        content: \"\\F04C\"; }\n      :host ::ng-deep .mdi-arrow-left:before {\n        content: \"\\F04D\"; }\n      :host ::ng-deep .mdi-arrow-left-bold:before {\n        content: \"\\F04E\"; }\n      :host ::ng-deep .mdi-arrow-left-bold-circle:before {\n        content: \"\\F04F\"; }\n      :host ::ng-deep .mdi-arrow-left-bold-circle-outline:before {\n        content: \"\\F050\"; }\n      :host ::ng-deep .mdi-arrow-left-bold-hexagon-outline:before {\n        content: \"\\F051\"; }\n      :host ::ng-deep .mdi-arrow-left-box:before {\n        content: \"\\F6C0\"; }\n      :host ::ng-deep .mdi-arrow-left-drop-circle:before {\n        content: \"\\F052\"; }\n      :host ::ng-deep .mdi-arrow-left-drop-circle-outline:before {\n        content: \"\\F053\"; }\n      :host ::ng-deep .mdi-arrow-right:before {\n        content: \"\\F054\"; }\n      :host ::ng-deep .mdi-arrow-right-bold:before {\n        content: \"\\F055\"; }\n      :host ::ng-deep .mdi-arrow-right-bold-circle:before {\n        content: \"\\F056\"; }\n      :host ::ng-deep .mdi-arrow-right-bold-circle-outline:before {\n        content: \"\\F057\"; }\n      :host ::ng-deep .mdi-arrow-right-bold-hexagon-outline:before {\n        content: \"\\F058\"; }\n      :host ::ng-deep .mdi-arrow-right-box:before {\n        content: \"\\F6C1\"; }\n      :host ::ng-deep .mdi-arrow-right-drop-circle:before {\n        content: \"\\F059\"; }\n      :host ::ng-deep .mdi-arrow-right-drop-circle-outline:before {\n        content: \"\\F05A\"; }\n      :host ::ng-deep .mdi-arrow-top-left:before {\n        content: \"\\F05B\"; }\n      :host ::ng-deep .mdi-arrow-top-right:before {\n        content: \"\\F05C\"; }\n      :host ::ng-deep .mdi-arrow-up:before {\n        content: \"\\F05D\"; }\n      :host ::ng-deep .mdi-arrow-up-bold:before {\n        content: \"\\F05E\"; }\n      :host ::ng-deep .mdi-arrow-up-bold-circle:before {\n        content: \"\\F05F\"; }\n      :host ::ng-deep .mdi-arrow-up-bold-circle-outline:before {\n        content: \"\\F060\"; }\n      :host ::ng-deep .mdi-arrow-up-bold-hexagon-outline:before {\n        content: \"\\F061\"; }\n      :host ::ng-deep .mdi-arrow-up-box:before {\n        content: \"\\F6C2\"; }\n      :host ::ng-deep .mdi-arrow-up-drop-circle:before {\n        content: \"\\F062\"; }\n      :host ::ng-deep .mdi-arrow-up-drop-circle-outline:before {\n        content: \"\\F063\"; }\n      :host ::ng-deep .mdi-assistant:before {\n        content: \"\\F064\"; }\n      :host ::ng-deep .mdi-asterisk:before {\n        content: \"\\F6C3\"; }\n      :host ::ng-deep .mdi-at:before {\n        content: \"\\F065\"; }\n      :host ::ng-deep .mdi-attachment:before {\n        content: \"\\F066\"; }\n      :host ::ng-deep .mdi-audiobook:before {\n        content: \"\\F067\"; }\n      :host ::ng-deep .mdi-auto-fix:before {\n        content: \"\\F068\"; }\n      :host ::ng-deep .mdi-auto-upload:before {\n        content: \"\\F069\"; }\n      :host ::ng-deep .mdi-autorenew:before {\n        content: \"\\F06A\"; }\n      :host ::ng-deep .mdi-av-timer:before {\n        content: \"\\F06B\"; }\n      :host ::ng-deep .mdi-baby:before {\n        content: \"\\F06C\"; }\n      :host ::ng-deep .mdi-baby-buggy:before {\n        content: \"\\F68E\"; }\n      :host ::ng-deep .mdi-backburger:before {\n        content: \"\\F06D\"; }\n      :host ::ng-deep .mdi-backspace:before {\n        content: \"\\F06E\"; }\n      :host ::ng-deep .mdi-backup-restore:before {\n        content: \"\\F06F\"; }\n      :host ::ng-deep .mdi-bandcamp:before {\n        content: \"\\F674\"; }\n      :host ::ng-deep .mdi-bank:before {\n        content: \"\\F070\"; }\n      :host ::ng-deep .mdi-barcode:before {\n        content: \"\\F071\"; }\n      :host ::ng-deep .mdi-barcode-scan:before {\n        content: \"\\F072\"; }\n      :host ::ng-deep .mdi-barley:before {\n        content: \"\\F073\"; }\n      :host ::ng-deep .mdi-barrel:before {\n        content: \"\\F074\"; }\n      :host ::ng-deep .mdi-basecamp:before {\n        content: \"\\F075\"; }\n      :host ::ng-deep .mdi-basket:before {\n        content: \"\\F076\"; }\n      :host ::ng-deep .mdi-basket-fill:before {\n        content: \"\\F077\"; }\n      :host ::ng-deep .mdi-basket-unfill:before {\n        content: \"\\F078\"; }\n      :host ::ng-deep .mdi-battery:before {\n        content: \"\\F079\"; }\n      :host ::ng-deep .mdi-battery-10:before {\n        content: \"\\F07A\"; }\n      :host ::ng-deep .mdi-battery-20:before {\n        content: \"\\F07B\"; }\n      :host ::ng-deep .mdi-battery-30:before {\n        content: \"\\F07C\"; }\n      :host ::ng-deep .mdi-battery-40:before {\n        content: \"\\F07D\"; }\n      :host ::ng-deep .mdi-battery-50:before {\n        content: \"\\F07E\"; }\n      :host ::ng-deep .mdi-battery-60:before {\n        content: \"\\F07F\"; }\n      :host ::ng-deep .mdi-battery-70:before {\n        content: \"\\F080\"; }\n      :host ::ng-deep .mdi-battery-80:before {\n        content: \"\\F081\"; }\n      :host ::ng-deep .mdi-battery-90:before {\n        content: \"\\F082\"; }\n      :host ::ng-deep .mdi-battery-alert:before {\n        content: \"\\F083\"; }\n      :host ::ng-deep .mdi-battery-charging:before {\n        content: \"\\F084\"; }\n      :host ::ng-deep .mdi-battery-charging-100:before {\n        content: \"\\F085\"; }\n      :host ::ng-deep .mdi-battery-charging-20:before {\n        content: \"\\F086\"; }\n      :host ::ng-deep .mdi-battery-charging-30:before {\n        content: \"\\F087\"; }\n      :host ::ng-deep .mdi-battery-charging-40:before {\n        content: \"\\F088\"; }\n      :host ::ng-deep .mdi-battery-charging-60:before {\n        content: \"\\F089\"; }\n      :host ::ng-deep .mdi-battery-charging-80:before {\n        content: \"\\F08A\"; }\n      :host ::ng-deep .mdi-battery-charging-90:before {\n        content: \"\\F08B\"; }\n      :host ::ng-deep .mdi-battery-minus:before {\n        content: \"\\F08C\"; }\n      :host ::ng-deep .mdi-battery-negative:before {\n        content: \"\\F08D\"; }\n      :host ::ng-deep .mdi-battery-outline:before {\n        content: \"\\F08E\"; }\n      :host ::ng-deep .mdi-battery-plus:before {\n        content: \"\\F08F\"; }\n      :host ::ng-deep .mdi-battery-positive:before {\n        content: \"\\F090\"; }\n      :host ::ng-deep .mdi-battery-unknown:before {\n        content: \"\\F091\"; }\n      :host ::ng-deep .mdi-beach:before {\n        content: \"\\F092\"; }\n      :host ::ng-deep .mdi-beaker:before {\n        content: \"\\F68F\"; }\n      :host ::ng-deep .mdi-beats:before {\n        content: \"\\F097\"; }\n      :host ::ng-deep .mdi-beer:before {\n        content: \"\\F098\"; }\n      :host ::ng-deep .mdi-behance:before {\n        content: \"\\F099\"; }\n      :host ::ng-deep .mdi-bell:before {\n        content: \"\\F09A\"; }\n      :host ::ng-deep .mdi-bell-off:before {\n        content: \"\\F09B\"; }\n      :host ::ng-deep .mdi-bell-outline:before {\n        content: \"\\F09C\"; }\n      :host ::ng-deep .mdi-bell-plus:before {\n        content: \"\\F09D\"; }\n      :host ::ng-deep .mdi-bell-ring:before {\n        content: \"\\F09E\"; }\n      :host ::ng-deep .mdi-bell-ring-outline:before {\n        content: \"\\F09F\"; }\n      :host ::ng-deep .mdi-bell-sleep:before {\n        content: \"\\F0A0\"; }\n      :host ::ng-deep .mdi-beta:before {\n        content: \"\\F0A1\"; }\n      :host ::ng-deep .mdi-bible:before {\n        content: \"\\F0A2\"; }\n      :host ::ng-deep .mdi-bike:before {\n        content: \"\\F0A3\"; }\n      :host ::ng-deep .mdi-bing:before {\n        content: \"\\F0A4\"; }\n      :host ::ng-deep .mdi-binoculars:before {\n        content: \"\\F0A5\"; }\n      :host ::ng-deep .mdi-bio:before {\n        content: \"\\F0A6\"; }\n      :host ::ng-deep .mdi-biohazard:before {\n        content: \"\\F0A7\"; }\n      :host ::ng-deep .mdi-bitbucket:before {\n        content: \"\\F0A8\"; }\n      :host ::ng-deep .mdi-black-mesa:before {\n        content: \"\\F0A9\"; }\n      :host ::ng-deep .mdi-blackberry:before {\n        content: \"\\F0AA\"; }\n      :host ::ng-deep .mdi-blender:before {\n        content: \"\\F0AB\"; }\n      :host ::ng-deep .mdi-blinds:before {\n        content: \"\\F0AC\"; }\n      :host ::ng-deep .mdi-block-helper:before {\n        content: \"\\F0AD\"; }\n      :host ::ng-deep .mdi-blogger:before {\n        content: \"\\F0AE\"; }\n      :host ::ng-deep .mdi-bluetooth:before {\n        content: \"\\F0AF\"; }\n      :host ::ng-deep .mdi-bluetooth-audio:before {\n        content: \"\\F0B0\"; }\n      :host ::ng-deep .mdi-bluetooth-connect:before {\n        content: \"\\F0B1\"; }\n      :host ::ng-deep .mdi-bluetooth-off:before {\n        content: \"\\F0B2\"; }\n      :host ::ng-deep .mdi-bluetooth-settings:before {\n        content: \"\\F0B3\"; }\n      :host ::ng-deep .mdi-bluetooth-transfer:before {\n        content: \"\\F0B4\"; }\n      :host ::ng-deep .mdi-blur:before {\n        content: \"\\F0B5\"; }\n      :host ::ng-deep .mdi-blur-linear:before {\n        content: \"\\F0B6\"; }\n      :host ::ng-deep .mdi-blur-off:before {\n        content: \"\\F0B7\"; }\n      :host ::ng-deep .mdi-blur-radial:before {\n        content: \"\\F0B8\"; }\n      :host ::ng-deep .mdi-bomb:before {\n        content: \"\\F690\"; }\n      :host ::ng-deep .mdi-bomb-off:before {\n        content: \"\\F6C4\"; }\n      :host ::ng-deep .mdi-bone:before {\n        content: \"\\F0B9\"; }\n      :host ::ng-deep .mdi-book:before {\n        content: \"\\F0BA\"; }\n      :host ::ng-deep .mdi-book-minus:before {\n        content: \"\\F5D9\"; }\n      :host ::ng-deep .mdi-book-multiple:before {\n        content: \"\\F0BB\"; }\n      :host ::ng-deep .mdi-book-multiple-variant:before {\n        content: \"\\F0BC\"; }\n      :host ::ng-deep .mdi-book-open:before {\n        content: \"\\F0BD\"; }\n      :host ::ng-deep .mdi-book-open-page-variant:before {\n        content: \"\\F5DA\"; }\n      :host ::ng-deep .mdi-book-open-variant:before {\n        content: \"\\F0BE\"; }\n      :host ::ng-deep .mdi-book-plus:before {\n        content: \"\\F5DB\"; }\n      :host ::ng-deep .mdi-book-variant:before {\n        content: \"\\F0BF\"; }\n      :host ::ng-deep .mdi-bookmark:before {\n        content: \"\\F0C0\"; }\n      :host ::ng-deep .mdi-bookmark-check:before {\n        content: \"\\F0C1\"; }\n      :host ::ng-deep .mdi-bookmark-music:before {\n        content: \"\\F0C2\"; }\n      :host ::ng-deep .mdi-bookmark-outline:before {\n        content: \"\\F0C3\"; }\n      :host ::ng-deep .mdi-bookmark-plus:before {\n        content: \"\\F0C5\"; }\n      :host ::ng-deep .mdi-bookmark-plus-outline:before {\n        content: \"\\F0C4\"; }\n      :host ::ng-deep .mdi-bookmark-remove:before {\n        content: \"\\F0C6\"; }\n      :host ::ng-deep .mdi-boombox:before {\n        content: \"\\F5DC\"; }\n      :host ::ng-deep .mdi-bootstrap:before {\n        content: \"\\F6C5\"; }\n      :host ::ng-deep .mdi-border-all:before {\n        content: \"\\F0C7\"; }\n      :host ::ng-deep .mdi-border-bottom:before {\n        content: \"\\F0C8\"; }\n      :host ::ng-deep .mdi-border-color:before {\n        content: \"\\F0C9\"; }\n      :host ::ng-deep .mdi-border-horizontal:before {\n        content: \"\\F0CA\"; }\n      :host ::ng-deep .mdi-border-inside:before {\n        content: \"\\F0CB\"; }\n      :host ::ng-deep .mdi-border-left:before {\n        content: \"\\F0CC\"; }\n      :host ::ng-deep .mdi-border-none:before {\n        content: \"\\F0CD\"; }\n      :host ::ng-deep .mdi-border-outside:before {\n        content: \"\\F0CE\"; }\n      :host ::ng-deep .mdi-border-right:before {\n        content: \"\\F0CF\"; }\n      :host ::ng-deep .mdi-border-style:before {\n        content: \"\\F0D0\"; }\n      :host ::ng-deep .mdi-border-top:before {\n        content: \"\\F0D1\"; }\n      :host ::ng-deep .mdi-border-vertical:before {\n        content: \"\\F0D2\"; }\n      :host ::ng-deep .mdi-bow-tie:before {\n        content: \"\\F677\"; }\n      :host ::ng-deep .mdi-bowl:before {\n        content: \"\\F617\"; }\n      :host ::ng-deep .mdi-bowling:before {\n        content: \"\\F0D3\"; }\n      :host ::ng-deep .mdi-box:before {\n        content: \"\\F0D4\"; }\n      :host ::ng-deep .mdi-box-cutter:before {\n        content: \"\\F0D5\"; }\n      :host ::ng-deep .mdi-box-shadow:before {\n        content: \"\\F637\"; }\n      :host ::ng-deep .mdi-bridge:before {\n        content: \"\\F618\"; }\n      :host ::ng-deep .mdi-briefcase:before {\n        content: \"\\F0D6\"; }\n      :host ::ng-deep .mdi-briefcase-check:before {\n        content: \"\\F0D7\"; }\n      :host ::ng-deep .mdi-briefcase-download:before {\n        content: \"\\F0D8\"; }\n      :host ::ng-deep .mdi-briefcase-upload:before {\n        content: \"\\F0D9\"; }\n      :host ::ng-deep .mdi-brightness-1:before {\n        content: \"\\F0DA\"; }\n      :host ::ng-deep .mdi-brightness-2:before {\n        content: \"\\F0DB\"; }\n      :host ::ng-deep .mdi-brightness-3:before {\n        content: \"\\F0DC\"; }\n      :host ::ng-deep .mdi-brightness-4:before {\n        content: \"\\F0DD\"; }\n      :host ::ng-deep .mdi-brightness-5:before {\n        content: \"\\F0DE\"; }\n      :host ::ng-deep .mdi-brightness-6:before {\n        content: \"\\F0DF\"; }\n      :host ::ng-deep .mdi-brightness-7:before {\n        content: \"\\F0E0\"; }\n      :host ::ng-deep .mdi-brightness-auto:before {\n        content: \"\\F0E1\"; }\n      :host ::ng-deep .mdi-broom:before {\n        content: \"\\F0E2\"; }\n      :host ::ng-deep .mdi-brush:before {\n        content: \"\\F0E3\"; }\n      :host ::ng-deep .mdi-buffer:before {\n        content: \"\\F619\"; }\n      :host ::ng-deep .mdi-bug:before {\n        content: \"\\F0E4\"; }\n      :host ::ng-deep .mdi-bulletin-board:before {\n        content: \"\\F0E5\"; }\n      :host ::ng-deep .mdi-bullhorn:before {\n        content: \"\\F0E6\"; }\n      :host ::ng-deep .mdi-bullseye:before {\n        content: \"\\F5DD\"; }\n      :host ::ng-deep .mdi-burst-mode:before {\n        content: \"\\F5DE\"; }\n      :host ::ng-deep .mdi-bus:before {\n        content: \"\\F0E7\"; }\n      :host ::ng-deep .mdi-cached:before {\n        content: \"\\F0E8\"; }\n      :host ::ng-deep .mdi-cake:before {\n        content: \"\\F0E9\"; }\n      :host ::ng-deep .mdi-cake-layered:before {\n        content: \"\\F0EA\"; }\n      :host ::ng-deep .mdi-cake-variant:before {\n        content: \"\\F0EB\"; }\n      :host ::ng-deep .mdi-calculator:before {\n        content: \"\\F0EC\"; }\n      :host ::ng-deep .mdi-calendar:before {\n        content: \"\\F0ED\"; }\n      :host ::ng-deep .mdi-calendar-blank:before {\n        content: \"\\F0EE\"; }\n      :host ::ng-deep .mdi-calendar-check:before {\n        content: \"\\F0EF\"; }\n      :host ::ng-deep .mdi-calendar-clock:before {\n        content: \"\\F0F0\"; }\n      :host ::ng-deep .mdi-calendar-multiple:before {\n        content: \"\\F0F1\"; }\n      :host ::ng-deep .mdi-calendar-multiple-check:before {\n        content: \"\\F0F2\"; }\n      :host ::ng-deep .mdi-calendar-plus:before {\n        content: \"\\F0F3\"; }\n      :host ::ng-deep .mdi-calendar-question:before {\n        content: \"\\F691\"; }\n      :host ::ng-deep .mdi-calendar-range:before {\n        content: \"\\F678\"; }\n      :host ::ng-deep .mdi-calendar-remove:before {\n        content: \"\\F0F4\"; }\n      :host ::ng-deep .mdi-calendar-text:before {\n        content: \"\\F0F5\"; }\n      :host ::ng-deep .mdi-calendar-today:before {\n        content: \"\\F0F6\"; }\n      :host ::ng-deep .mdi-call-made:before {\n        content: \"\\F0F7\"; }\n      :host ::ng-deep .mdi-call-merge:before {\n        content: \"\\F0F8\"; }\n      :host ::ng-deep .mdi-call-missed:before {\n        content: \"\\F0F9\"; }\n      :host ::ng-deep .mdi-call-received:before {\n        content: \"\\F0FA\"; }\n      :host ::ng-deep .mdi-call-split:before {\n        content: \"\\F0FB\"; }\n      :host ::ng-deep .mdi-camcorder:before {\n        content: \"\\F0FC\"; }\n      :host ::ng-deep .mdi-camcorder-box:before {\n        content: \"\\F0FD\"; }\n      :host ::ng-deep .mdi-camcorder-box-off:before {\n        content: \"\\F0FE\"; }\n      :host ::ng-deep .mdi-camcorder-off:before {\n        content: \"\\F0FF\"; }\n      :host ::ng-deep .mdi-camera:before {\n        content: \"\\F100\"; }\n      :host ::ng-deep .mdi-camera-burst:before {\n        content: \"\\F692\"; }\n      :host ::ng-deep .mdi-camera-enhance:before {\n        content: \"\\F101\"; }\n      :host ::ng-deep .mdi-camera-front:before {\n        content: \"\\F102\"; }\n      :host ::ng-deep .mdi-camera-front-variant:before {\n        content: \"\\F103\"; }\n      :host ::ng-deep .mdi-camera-iris:before {\n        content: \"\\F104\"; }\n      :host ::ng-deep .mdi-camera-off:before {\n        content: \"\\F5DF\"; }\n      :host ::ng-deep .mdi-camera-party-mode:before {\n        content: \"\\F105\"; }\n      :host ::ng-deep .mdi-camera-rear:before {\n        content: \"\\F106\"; }\n      :host ::ng-deep .mdi-camera-rear-variant:before {\n        content: \"\\F107\"; }\n      :host ::ng-deep .mdi-camera-switch:before {\n        content: \"\\F108\"; }\n      :host ::ng-deep .mdi-camera-timer:before {\n        content: \"\\F109\"; }\n      :host ::ng-deep .mdi-candle:before {\n        content: \"\\F5E2\"; }\n      :host ::ng-deep .mdi-candycane:before {\n        content: \"\\F10A\"; }\n      :host ::ng-deep .mdi-car:before {\n        content: \"\\F10B\"; }\n      :host ::ng-deep .mdi-car-battery:before {\n        content: \"\\F10C\"; }\n      :host ::ng-deep .mdi-car-connected:before {\n        content: \"\\F10D\"; }\n      :host ::ng-deep .mdi-car-wash:before {\n        content: \"\\F10E\"; }\n      :host ::ng-deep .mdi-cards:before {\n        content: \"\\F638\"; }\n      :host ::ng-deep .mdi-cards-outline:before {\n        content: \"\\F639\"; }\n      :host ::ng-deep .mdi-cards-playing-outline:before {\n        content: \"\\F63A\"; }\n      :host ::ng-deep .mdi-cards-variant:before {\n        content: \"\\F6C6\"; }\n      :host ::ng-deep .mdi-carrot:before {\n        content: \"\\F10F\"; }\n      :host ::ng-deep .mdi-cart:before {\n        content: \"\\F110\"; }\n      :host ::ng-deep .mdi-cart-off:before {\n        content: \"\\F66B\"; }\n      :host ::ng-deep .mdi-cart-outline:before {\n        content: \"\\F111\"; }\n      :host ::ng-deep .mdi-cart-plus:before {\n        content: \"\\F112\"; }\n      :host ::ng-deep .mdi-case-sensitive-alt:before {\n        content: \"\\F113\"; }\n      :host ::ng-deep .mdi-cash:before {\n        content: \"\\F114\"; }\n      :host ::ng-deep .mdi-cash-100:before {\n        content: \"\\F115\"; }\n      :host ::ng-deep .mdi-cash-multiple:before {\n        content: \"\\F116\"; }\n      :host ::ng-deep .mdi-cash-usd:before {\n        content: \"\\F117\"; }\n      :host ::ng-deep .mdi-cast:before {\n        content: \"\\F118\"; }\n      :host ::ng-deep .mdi-cast-connected:before {\n        content: \"\\F119\"; }\n      :host ::ng-deep .mdi-castle:before {\n        content: \"\\F11A\"; }\n      :host ::ng-deep .mdi-cat:before {\n        content: \"\\F11B\"; }\n      :host ::ng-deep .mdi-cellphone:before {\n        content: \"\\F11C\"; }\n      :host ::ng-deep .mdi-cellphone-android:before {\n        content: \"\\F11D\"; }\n      :host ::ng-deep .mdi-cellphone-basic:before {\n        content: \"\\F11E\"; }\n      :host ::ng-deep .mdi-cellphone-dock:before {\n        content: \"\\F11F\"; }\n      :host ::ng-deep .mdi-cellphone-iphone:before {\n        content: \"\\F120\"; }\n      :host ::ng-deep .mdi-cellphone-link:before {\n        content: \"\\F121\"; }\n      :host ::ng-deep .mdi-cellphone-link-off:before {\n        content: \"\\F122\"; }\n      :host ::ng-deep .mdi-cellphone-settings:before {\n        content: \"\\F123\"; }\n      :host ::ng-deep .mdi-certificate:before {\n        content: \"\\F124\"; }\n      :host ::ng-deep .mdi-chair-school:before {\n        content: \"\\F125\"; }\n      :host ::ng-deep .mdi-chart-arc:before {\n        content: \"\\F126\"; }\n      :host ::ng-deep .mdi-chart-areaspline:before {\n        content: \"\\F127\"; }\n      :host ::ng-deep .mdi-chart-bar:before {\n        content: \"\\F128\"; }\n      :host ::ng-deep .mdi-chart-bubble:before {\n        content: \"\\F5E3\"; }\n      :host ::ng-deep .mdi-chart-gantt:before {\n        content: \"\\F66C\"; }\n      :host ::ng-deep .mdi-chart-histogram:before {\n        content: \"\\F129\"; }\n      :host ::ng-deep .mdi-chart-line:before {\n        content: \"\\F12A\"; }\n      :host ::ng-deep .mdi-chart-pie:before {\n        content: \"\\F12B\"; }\n      :host ::ng-deep .mdi-chart-scatterplot-hexbin:before {\n        content: \"\\F66D\"; }\n      :host ::ng-deep .mdi-chart-timeline:before {\n        content: \"\\F66E\"; }\n      :host ::ng-deep .mdi-check:before {\n        content: \"\\F12C\"; }\n      :host ::ng-deep .mdi-check-all:before {\n        content: \"\\F12D\"; }\n      :host ::ng-deep .mdi-check-circle:before {\n        content: \"\\F5E0\"; }\n      :host ::ng-deep .mdi-check-circle-outline:before {\n        content: \"\\F5E1\"; }\n      :host ::ng-deep .mdi-checkbox-blank:before {\n        content: \"\\F12E\"; }\n      :host ::ng-deep .mdi-checkbox-blank-circle:before {\n        content: \"\\F12F\"; }\n      :host ::ng-deep .mdi-checkbox-blank-circle-outline:before {\n        content: \"\\F130\"; }\n      :host ::ng-deep .mdi-checkbox-blank-outline:before {\n        content: \"\\F131\"; }\n      :host ::ng-deep .mdi-checkbox-marked:before {\n        content: \"\\F132\"; }\n      :host ::ng-deep .mdi-checkbox-marked-circle:before {\n        content: \"\\F133\"; }\n      :host ::ng-deep .mdi-checkbox-marked-circle-outline:before {\n        content: \"\\F134\"; }\n      :host ::ng-deep .mdi-checkbox-marked-outline:before {\n        content: \"\\F135\"; }\n      :host ::ng-deep .mdi-checkbox-multiple-blank:before {\n        content: \"\\F136\"; }\n      :host ::ng-deep .mdi-checkbox-multiple-blank-circle:before {\n        content: \"\\F63B\"; }\n      :host ::ng-deep .mdi-checkbox-multiple-blank-circle-outline:before {\n        content: \"\\F63C\"; }\n      :host ::ng-deep .mdi-checkbox-multiple-blank-outline:before {\n        content: \"\\F137\"; }\n      :host ::ng-deep .mdi-checkbox-multiple-marked:before {\n        content: \"\\F138\"; }\n      :host ::ng-deep .mdi-checkbox-multiple-marked-circle:before {\n        content: \"\\F63D\"; }\n      :host ::ng-deep .mdi-checkbox-multiple-marked-circle-outline:before {\n        content: \"\\F63E\"; }\n      :host ::ng-deep .mdi-checkbox-multiple-marked-outline:before {\n        content: \"\\F139\"; }\n      :host ::ng-deep .mdi-checkerboard:before {\n        content: \"\\F13A\"; }\n      :host ::ng-deep .mdi-chemical-weapon:before {\n        content: \"\\F13B\"; }\n      :host ::ng-deep .mdi-chevron-double-down:before {\n        content: \"\\F13C\"; }\n      :host ::ng-deep .mdi-chevron-double-left:before {\n        content: \"\\F13D\"; }\n      :host ::ng-deep .mdi-chevron-double-right:before {\n        content: \"\\F13E\"; }\n      :host ::ng-deep .mdi-chevron-double-up:before {\n        content: \"\\F13F\"; }\n      :host ::ng-deep .mdi-chevron-down:before {\n        content: \"\\F140\"; }\n      :host ::ng-deep .mdi-chevron-left:before {\n        content: \"\\F141\"; }\n      :host ::ng-deep .mdi-chevron-right:before {\n        content: \"\\F142\"; }\n      :host ::ng-deep .mdi-chevron-up:before {\n        content: \"\\F143\"; }\n      :host ::ng-deep .mdi-chip:before {\n        content: \"\\F61A\"; }\n      :host ::ng-deep .mdi-church:before {\n        content: \"\\F144\"; }\n      :host ::ng-deep .mdi-cisco-webex:before {\n        content: \"\\F145\"; }\n      :host ::ng-deep .mdi-city:before {\n        content: \"\\F146\"; }\n      :host ::ng-deep .mdi-clipboard:before {\n        content: \"\\F147\"; }\n      :host ::ng-deep .mdi-clipboard-account:before {\n        content: \"\\F148\"; }\n      :host ::ng-deep .mdi-clipboard-alert:before {\n        content: \"\\F149\"; }\n      :host ::ng-deep .mdi-clipboard-arrow-down:before {\n        content: \"\\F14A\"; }\n      :host ::ng-deep .mdi-clipboard-arrow-left:before {\n        content: \"\\F14B\"; }\n      :host ::ng-deep .mdi-clipboard-check:before {\n        content: \"\\F14C\"; }\n      :host ::ng-deep .mdi-clipboard-flow:before {\n        content: \"\\F6C7\"; }\n      :host ::ng-deep .mdi-clipboard-outline:before {\n        content: \"\\F14D\"; }\n      :host ::ng-deep .mdi-clipboard-text:before {\n        content: \"\\F14E\"; }\n      :host ::ng-deep .mdi-clippy:before {\n        content: \"\\F14F\"; }\n      :host ::ng-deep .mdi-clock:before {\n        content: \"\\F150\"; }\n      :host ::ng-deep .mdi-clock-alert:before {\n        content: \"\\F5CE\"; }\n      :host ::ng-deep .mdi-clock-end:before {\n        content: \"\\F151\"; }\n      :host ::ng-deep .mdi-clock-fast:before {\n        content: \"\\F152\"; }\n      :host ::ng-deep .mdi-clock-in:before {\n        content: \"\\F153\"; }\n      :host ::ng-deep .mdi-clock-out:before {\n        content: \"\\F154\"; }\n      :host ::ng-deep .mdi-clock-start:before {\n        content: \"\\F155\"; }\n      :host ::ng-deep .mdi-close:before {\n        content: \"\\F156\"; }\n      :host ::ng-deep .mdi-close-box:before {\n        content: \"\\F157\"; }\n      :host ::ng-deep .mdi-close-box-outline:before {\n        content: \"\\F158\"; }\n      :host ::ng-deep .mdi-close-circle:before {\n        content: \"\\F159\"; }\n      :host ::ng-deep .mdi-close-circle-outline:before {\n        content: \"\\F15A\"; }\n      :host ::ng-deep .mdi-close-network:before {\n        content: \"\\F15B\"; }\n      :host ::ng-deep .mdi-close-octagon:before {\n        content: \"\\F15C\"; }\n      :host ::ng-deep .mdi-close-octagon-outline:before {\n        content: \"\\F15D\"; }\n      :host ::ng-deep .mdi-close-outline:before {\n        content: \"\\F6C8\"; }\n      :host ::ng-deep .mdi-closed-caption:before {\n        content: \"\\F15E\"; }\n      :host ::ng-deep .mdi-cloud:before {\n        content: \"\\F15F\"; }\n      :host ::ng-deep .mdi-cloud-check:before {\n        content: \"\\F160\"; }\n      :host ::ng-deep .mdi-cloud-circle:before {\n        content: \"\\F161\"; }\n      :host ::ng-deep .mdi-cloud-download:before {\n        content: \"\\F162\"; }\n      :host ::ng-deep .mdi-cloud-outline:before {\n        content: \"\\F163\"; }\n      :host ::ng-deep .mdi-cloud-outline-off:before {\n        content: \"\\F164\"; }\n      :host ::ng-deep .mdi-cloud-print:before {\n        content: \"\\F165\"; }\n      :host ::ng-deep .mdi-cloud-print-outline:before {\n        content: \"\\F166\"; }\n      :host ::ng-deep .mdi-cloud-sync:before {\n        content: \"\\F63F\"; }\n      :host ::ng-deep .mdi-cloud-upload:before {\n        content: \"\\F167\"; }\n      :host ::ng-deep .mdi-code-array:before {\n        content: \"\\F168\"; }\n      :host ::ng-deep .mdi-code-braces:before {\n        content: \"\\F169\"; }\n      :host ::ng-deep .mdi-code-brackets:before {\n        content: \"\\F16A\"; }\n      :host ::ng-deep .mdi-code-equal:before {\n        content: \"\\F16B\"; }\n      :host ::ng-deep .mdi-code-greater-than:before {\n        content: \"\\F16C\"; }\n      :host ::ng-deep .mdi-code-greater-than-or-equal:before {\n        content: \"\\F16D\"; }\n      :host ::ng-deep .mdi-code-less-than:before {\n        content: \"\\F16E\"; }\n      :host ::ng-deep .mdi-code-less-than-or-equal:before {\n        content: \"\\F16F\"; }\n      :host ::ng-deep .mdi-code-not-equal:before {\n        content: \"\\F170\"; }\n      :host ::ng-deep .mdi-code-not-equal-variant:before {\n        content: \"\\F171\"; }\n      :host ::ng-deep .mdi-code-parentheses:before {\n        content: \"\\F172\"; }\n      :host ::ng-deep .mdi-code-string:before {\n        content: \"\\F173\"; }\n      :host ::ng-deep .mdi-code-tags:before {\n        content: \"\\F174\"; }\n      :host ::ng-deep .mdi-code-tags-check:before {\n        content: \"\\F693\"; }\n      :host ::ng-deep .mdi-codepen:before {\n        content: \"\\F175\"; }\n      :host ::ng-deep .mdi-coffee:before {\n        content: \"\\F176\"; }\n      :host ::ng-deep .mdi-coffee-outline:before {\n        content: \"\\F6C9\"; }\n      :host ::ng-deep .mdi-coffee-to-go:before {\n        content: \"\\F177\"; }\n      :host ::ng-deep .mdi-coin:before {\n        content: \"\\F178\"; }\n      :host ::ng-deep .mdi-coins:before {\n        content: \"\\F694\"; }\n      :host ::ng-deep .mdi-collage:before {\n        content: \"\\F640\"; }\n      :host ::ng-deep .mdi-color-helper:before {\n        content: \"\\F179\"; }\n      :host ::ng-deep .mdi-comment:before {\n        content: \"\\F17A\"; }\n      :host ::ng-deep .mdi-comment-account:before {\n        content: \"\\F17B\"; }\n      :host ::ng-deep .mdi-comment-account-outline:before {\n        content: \"\\F17C\"; }\n      :host ::ng-deep .mdi-comment-alert:before {\n        content: \"\\F17D\"; }\n      :host ::ng-deep .mdi-comment-alert-outline:before {\n        content: \"\\F17E\"; }\n      :host ::ng-deep .mdi-comment-check:before {\n        content: \"\\F17F\"; }\n      :host ::ng-deep .mdi-comment-check-outline:before {\n        content: \"\\F180\"; }\n      :host ::ng-deep .mdi-comment-multiple-outline:before {\n        content: \"\\F181\"; }\n      :host ::ng-deep .mdi-comment-outline:before {\n        content: \"\\F182\"; }\n      :host ::ng-deep .mdi-comment-plus-outline:before {\n        content: \"\\F183\"; }\n      :host ::ng-deep .mdi-comment-processing:before {\n        content: \"\\F184\"; }\n      :host ::ng-deep .mdi-comment-processing-outline:before {\n        content: \"\\F185\"; }\n      :host ::ng-deep .mdi-comment-question-outline:before {\n        content: \"\\F186\"; }\n      :host ::ng-deep .mdi-comment-remove-outline:before {\n        content: \"\\F187\"; }\n      :host ::ng-deep .mdi-comment-text:before {\n        content: \"\\F188\"; }\n      :host ::ng-deep .mdi-comment-text-outline:before {\n        content: \"\\F189\"; }\n      :host ::ng-deep .mdi-compare:before {\n        content: \"\\F18A\"; }\n      :host ::ng-deep .mdi-compass:before {\n        content: \"\\F18B\"; }\n      :host ::ng-deep .mdi-compass-outline:before {\n        content: \"\\F18C\"; }\n      :host ::ng-deep .mdi-console:before {\n        content: \"\\F18D\"; }\n      :host ::ng-deep .mdi-contact-mail:before {\n        content: \"\\F18E\"; }\n      :host ::ng-deep .mdi-contacts:before {\n        content: \"\\F6CA\"; }\n      :host ::ng-deep .mdi-content-copy:before {\n        content: \"\\F18F\"; }\n      :host ::ng-deep .mdi-content-cut:before {\n        content: \"\\F190\"; }\n      :host ::ng-deep .mdi-content-duplicate:before {\n        content: \"\\F191\"; }\n      :host ::ng-deep .mdi-content-paste:before {\n        content: \"\\F192\"; }\n      :host ::ng-deep .mdi-content-save:before {\n        content: \"\\F193\"; }\n      :host ::ng-deep .mdi-content-save-all:before {\n        content: \"\\F194\"; }\n      :host ::ng-deep .mdi-content-save-settings:before {\n        content: \"\\F61B\"; }\n      :host ::ng-deep .mdi-contrast:before {\n        content: \"\\F195\"; }\n      :host ::ng-deep .mdi-contrast-box:before {\n        content: \"\\F196\"; }\n      :host ::ng-deep .mdi-contrast-circle:before {\n        content: \"\\F197\"; }\n      :host ::ng-deep .mdi-cookie:before {\n        content: \"\\F198\"; }\n      :host ::ng-deep .mdi-copyright:before {\n        content: \"\\F5E6\"; }\n      :host ::ng-deep .mdi-counter:before {\n        content: \"\\F199\"; }\n      :host ::ng-deep .mdi-cow:before {\n        content: \"\\F19A\"; }\n      :host ::ng-deep .mdi-creation:before {\n        content: \"\\F1C9\"; }\n      :host ::ng-deep .mdi-credit-card:before {\n        content: \"\\F19B\"; }\n      :host ::ng-deep .mdi-credit-card-multiple:before {\n        content: \"\\F19C\"; }\n      :host ::ng-deep .mdi-credit-card-off:before {\n        content: \"\\F5E4\"; }\n      :host ::ng-deep .mdi-credit-card-plus:before {\n        content: \"\\F675\"; }\n      :host ::ng-deep .mdi-credit-card-scan:before {\n        content: \"\\F19D\"; }\n      :host ::ng-deep .mdi-crop:before {\n        content: \"\\F19E\"; }\n      :host ::ng-deep .mdi-crop-free:before {\n        content: \"\\F19F\"; }\n      :host ::ng-deep .mdi-crop-landscape:before {\n        content: \"\\F1A0\"; }\n      :host ::ng-deep .mdi-crop-portrait:before {\n        content: \"\\F1A1\"; }\n      :host ::ng-deep .mdi-crop-rotate:before {\n        content: \"\\F695\"; }\n      :host ::ng-deep .mdi-crop-square:before {\n        content: \"\\F1A2\"; }\n      :host ::ng-deep .mdi-crosshairs:before {\n        content: \"\\F1A3\"; }\n      :host ::ng-deep .mdi-crosshairs-gps:before {\n        content: \"\\F1A4\"; }\n      :host ::ng-deep .mdi-crown:before {\n        content: \"\\F1A5\"; }\n      :host ::ng-deep .mdi-cube:before {\n        content: \"\\F1A6\"; }\n      :host ::ng-deep .mdi-cube-outline:before {\n        content: \"\\F1A7\"; }\n      :host ::ng-deep .mdi-cube-send:before {\n        content: \"\\F1A8\"; }\n      :host ::ng-deep .mdi-cube-unfolded:before {\n        content: \"\\F1A9\"; }\n      :host ::ng-deep .mdi-cup:before {\n        content: \"\\F1AA\"; }\n      :host ::ng-deep .mdi-cup-off:before {\n        content: \"\\F5E5\"; }\n      :host ::ng-deep .mdi-cup-water:before {\n        content: \"\\F1AB\"; }\n      :host ::ng-deep .mdi-currency-btc:before {\n        content: \"\\F1AC\"; }\n      :host ::ng-deep .mdi-currency-eur:before {\n        content: \"\\F1AD\"; }\n      :host ::ng-deep .mdi-currency-gbp:before {\n        content: \"\\F1AE\"; }\n      :host ::ng-deep .mdi-currency-inr:before {\n        content: \"\\F1AF\"; }\n      :host ::ng-deep .mdi-currency-ngn:before {\n        content: \"\\F1B0\"; }\n      :host ::ng-deep .mdi-currency-rub:before {\n        content: \"\\F1B1\"; }\n      :host ::ng-deep .mdi-currency-try:before {\n        content: \"\\F1B2\"; }\n      :host ::ng-deep .mdi-currency-usd:before {\n        content: \"\\F1B3\"; }\n      :host ::ng-deep .mdi-currency-usd-off:before {\n        content: \"\\F679\"; }\n      :host ::ng-deep .mdi-cursor-default:before {\n        content: \"\\F1B4\"; }\n      :host ::ng-deep .mdi-cursor-default-outline:before {\n        content: \"\\F1B5\"; }\n      :host ::ng-deep .mdi-cursor-move:before {\n        content: \"\\F1B6\"; }\n      :host ::ng-deep .mdi-cursor-pointer:before {\n        content: \"\\F1B7\"; }\n      :host ::ng-deep .mdi-cursor-text:before {\n        content: \"\\F5E7\"; }\n      :host ::ng-deep .mdi-database:before {\n        content: \"\\F1B8\"; }\n      :host ::ng-deep .mdi-database-minus:before {\n        content: \"\\F1B9\"; }\n      :host ::ng-deep .mdi-database-plus:before {\n        content: \"\\F1BA\"; }\n      :host ::ng-deep .mdi-debug-step-into:before {\n        content: \"\\F1BB\"; }\n      :host ::ng-deep .mdi-debug-step-out:before {\n        content: \"\\F1BC\"; }\n      :host ::ng-deep .mdi-debug-step-over:before {\n        content: \"\\F1BD\"; }\n      :host ::ng-deep .mdi-decimal-decrease:before {\n        content: \"\\F1BE\"; }\n      :host ::ng-deep .mdi-decimal-increase:before {\n        content: \"\\F1BF\"; }\n      :host ::ng-deep .mdi-delete:before {\n        content: \"\\F1C0\"; }\n      :host ::ng-deep .mdi-delete-circle:before {\n        content: \"\\F682\"; }\n      :host ::ng-deep .mdi-delete-empty:before {\n        content: \"\\F6CB\"; }\n      :host ::ng-deep .mdi-delete-forever:before {\n        content: \"\\F5E8\"; }\n      :host ::ng-deep .mdi-delete-sweep:before {\n        content: \"\\F5E9\"; }\n      :host ::ng-deep .mdi-delete-variant:before {\n        content: \"\\F1C1\"; }\n      :host ::ng-deep .mdi-delta:before {\n        content: \"\\F1C2\"; }\n      :host ::ng-deep .mdi-deskphone:before {\n        content: \"\\F1C3\"; }\n      :host ::ng-deep .mdi-desktop-mac:before {\n        content: \"\\F1C4\"; }\n      :host ::ng-deep .mdi-desktop-tower:before {\n        content: \"\\F1C5\"; }\n      :host ::ng-deep .mdi-details:before {\n        content: \"\\F1C6\"; }\n      :host ::ng-deep .mdi-developer-board:before {\n        content: \"\\F696\"; }\n      :host ::ng-deep .mdi-deviantart:before {\n        content: \"\\F1C7\"; }\n      :host ::ng-deep .mdi-dialpad:before {\n        content: \"\\F61C\"; }\n      :host ::ng-deep .mdi-diamond:before {\n        content: \"\\F1C8\"; }\n      :host ::ng-deep .mdi-dice-1:before {\n        content: \"\\F1CA\"; }\n      :host ::ng-deep .mdi-dice-2:before {\n        content: \"\\F1CB\"; }\n      :host ::ng-deep .mdi-dice-3:before {\n        content: \"\\F1CC\"; }\n      :host ::ng-deep .mdi-dice-4:before {\n        content: \"\\F1CD\"; }\n      :host ::ng-deep .mdi-dice-5:before {\n        content: \"\\F1CE\"; }\n      :host ::ng-deep .mdi-dice-6:before {\n        content: \"\\F1CF\"; }\n      :host ::ng-deep .mdi-dice-d20:before {\n        content: \"\\F5EA\"; }\n      :host ::ng-deep .mdi-dice-d4:before {\n        content: \"\\F5EB\"; }\n      :host ::ng-deep .mdi-dice-d6:before {\n        content: \"\\F5EC\"; }\n      :host ::ng-deep .mdi-dice-d8:before {\n        content: \"\\F5ED\"; }\n      :host ::ng-deep .mdi-dictionary:before {\n        content: \"\\F61D\"; }\n      :host ::ng-deep .mdi-directions:before {\n        content: \"\\F1D0\"; }\n      :host ::ng-deep .mdi-directions-fork:before {\n        content: \"\\F641\"; }\n      :host ::ng-deep .mdi-discord:before {\n        content: \"\\F66F\"; }\n      :host ::ng-deep .mdi-disk:before {\n        content: \"\\F5EE\"; }\n      :host ::ng-deep .mdi-disk-alert:before {\n        content: \"\\F1D1\"; }\n      :host ::ng-deep .mdi-disqus:before {\n        content: \"\\F1D2\"; }\n      :host ::ng-deep .mdi-disqus-outline:before {\n        content: \"\\F1D3\"; }\n      :host ::ng-deep .mdi-division:before {\n        content: \"\\F1D4\"; }\n      :host ::ng-deep .mdi-division-box:before {\n        content: \"\\F1D5\"; }\n      :host ::ng-deep .mdi-dna:before {\n        content: \"\\F683\"; }\n      :host ::ng-deep .mdi-dns:before {\n        content: \"\\F1D6\"; }\n      :host ::ng-deep .mdi-do-not-disturb:before {\n        content: \"\\F697\"; }\n      :host ::ng-deep .mdi-do-not-disturb-off:before {\n        content: \"\\F698\"; }\n      :host ::ng-deep .mdi-dolby:before {\n        content: \"\\F6B2\"; }\n      :host ::ng-deep .mdi-domain:before {\n        content: \"\\F1D7\"; }\n      :host ::ng-deep .mdi-dots-horizontal:before {\n        content: \"\\F1D8\"; }\n      :host ::ng-deep .mdi-dots-vertical:before {\n        content: \"\\F1D9\"; }\n      :host ::ng-deep .mdi-douban:before {\n        content: \"\\F699\"; }\n      :host ::ng-deep .mdi-download:before {\n        content: \"\\F1DA\"; }\n      :host ::ng-deep .mdi-drag:before {\n        content: \"\\F1DB\"; }\n      :host ::ng-deep .mdi-drag-horizontal:before {\n        content: \"\\F1DC\"; }\n      :host ::ng-deep .mdi-drag-vertical:before {\n        content: \"\\F1DD\"; }\n      :host ::ng-deep .mdi-drawing:before {\n        content: \"\\F1DE\"; }\n      :host ::ng-deep .mdi-drawing-box:before {\n        content: \"\\F1DF\"; }\n      :host ::ng-deep .mdi-dribbble:before {\n        content: \"\\F1E0\"; }\n      :host ::ng-deep .mdi-dribbble-box:before {\n        content: \"\\F1E1\"; }\n      :host ::ng-deep .mdi-drone:before {\n        content: \"\\F1E2\"; }\n      :host ::ng-deep .mdi-dropbox:before {\n        content: \"\\F1E3\"; }\n      :host ::ng-deep .mdi-drupal:before {\n        content: \"\\F1E4\"; }\n      :host ::ng-deep .mdi-duck:before {\n        content: \"\\F1E5\"; }\n      :host ::ng-deep .mdi-dumbbell:before {\n        content: \"\\F1E6\"; }\n      :host ::ng-deep .mdi-earth:before {\n        content: \"\\F1E7\"; }\n      :host ::ng-deep .mdi-earth-box:before {\n        content: \"\\F6CC\"; }\n      :host ::ng-deep .mdi-earth-box-off:before {\n        content: \"\\F6CD\"; }\n      :host ::ng-deep .mdi-earth-off:before {\n        content: \"\\F1E8\"; }\n      :host ::ng-deep .mdi-edge:before {\n        content: \"\\F1E9\"; }\n      :host ::ng-deep .mdi-eject:before {\n        content: \"\\F1EA\"; }\n      :host ::ng-deep .mdi-elevation-decline:before {\n        content: \"\\F1EB\"; }\n      :host ::ng-deep .mdi-elevation-rise:before {\n        content: \"\\F1EC\"; }\n      :host ::ng-deep .mdi-elevator:before {\n        content: \"\\F1ED\"; }\n      :host ::ng-deep .mdi-email:before {\n        content: \"\\F1EE\"; }\n      :host ::ng-deep .mdi-email-alert:before {\n        content: \"\\F6CE\"; }\n      :host ::ng-deep .mdi-email-open:before {\n        content: \"\\F1EF\"; }\n      :host ::ng-deep .mdi-email-open-outline:before {\n        content: \"\\F5EF\"; }\n      :host ::ng-deep .mdi-email-outline:before {\n        content: \"\\F1F0\"; }\n      :host ::ng-deep .mdi-email-secure:before {\n        content: \"\\F1F1\"; }\n      :host ::ng-deep .mdi-email-variant:before {\n        content: \"\\F5F0\"; }\n      :host ::ng-deep .mdi-emby:before {\n        content: \"\\F6B3\"; }\n      :host ::ng-deep .mdi-emoticon:before {\n        content: \"\\F1F2\"; }\n      :host ::ng-deep .mdi-emoticon-cool:before {\n        content: \"\\F1F3\"; }\n      :host ::ng-deep .mdi-emoticon-dead:before {\n        content: \"\\F69A\"; }\n      :host ::ng-deep .mdi-emoticon-devil:before {\n        content: \"\\F1F4\"; }\n      :host ::ng-deep .mdi-emoticon-excited:before {\n        content: \"\\F69B\"; }\n      :host ::ng-deep .mdi-emoticon-happy:before {\n        content: \"\\F1F5\"; }\n      :host ::ng-deep .mdi-emoticon-neutral:before {\n        content: \"\\F1F6\"; }\n      :host ::ng-deep .mdi-emoticon-poop:before {\n        content: \"\\F1F7\"; }\n      :host ::ng-deep .mdi-emoticon-sad:before {\n        content: \"\\F1F8\"; }\n      :host ::ng-deep .mdi-emoticon-tongue:before {\n        content: \"\\F1F9\"; }\n      :host ::ng-deep .mdi-engine:before {\n        content: \"\\F1FA\"; }\n      :host ::ng-deep .mdi-engine-outline:before {\n        content: \"\\F1FB\"; }\n      :host ::ng-deep .mdi-equal:before {\n        content: \"\\F1FC\"; }\n      :host ::ng-deep .mdi-equal-box:before {\n        content: \"\\F1FD\"; }\n      :host ::ng-deep .mdi-eraser:before {\n        content: \"\\F1FE\"; }\n      :host ::ng-deep .mdi-eraser-variant:before {\n        content: \"\\F642\"; }\n      :host ::ng-deep .mdi-escalator:before {\n        content: \"\\F1FF\"; }\n      :host ::ng-deep .mdi-ethernet:before {\n        content: \"\\F200\"; }\n      :host ::ng-deep .mdi-ethernet-cable:before {\n        content: \"\\F201\"; }\n      :host ::ng-deep .mdi-ethernet-cable-off:before {\n        content: \"\\F202\"; }\n      :host ::ng-deep .mdi-etsy:before {\n        content: \"\\F203\"; }\n      :host ::ng-deep .mdi-ev-station:before {\n        content: \"\\F5F1\"; }\n      :host ::ng-deep .mdi-evernote:before {\n        content: \"\\F204\"; }\n      :host ::ng-deep .mdi-exclamation:before {\n        content: \"\\F205\"; }\n      :host ::ng-deep .mdi-exit-to-app:before {\n        content: \"\\F206\"; }\n      :host ::ng-deep .mdi-export:before {\n        content: \"\\F207\"; }\n      :host ::ng-deep .mdi-eye:before {\n        content: \"\\F208\"; }\n      :host ::ng-deep .mdi-eye-off:before {\n        content: \"\\F209\"; }\n      :host ::ng-deep .mdi-eye-outline:before {\n        content: \"\\F6CF\"; }\n      :host ::ng-deep .mdi-eye-outline-off:before {\n        content: \"\\F6D0\"; }\n      :host ::ng-deep .mdi-eyedropper:before {\n        content: \"\\F20A\"; }\n      :host ::ng-deep .mdi-eyedropper-variant:before {\n        content: \"\\F20B\"; }\n      :host ::ng-deep .mdi-face:before {\n        content: \"\\F643\"; }\n      :host ::ng-deep .mdi-face-profile:before {\n        content: \"\\F644\"; }\n      :host ::ng-deep .mdi-facebook:before {\n        content: \"\\F20C\"; }\n      :host ::ng-deep .mdi-facebook-box:before {\n        content: \"\\F20D\"; }\n      :host ::ng-deep .mdi-facebook-messenger:before {\n        content: \"\\F20E\"; }\n      :host ::ng-deep .mdi-factory:before {\n        content: \"\\F20F\"; }\n      :host ::ng-deep .mdi-fan:before {\n        content: \"\\F210\"; }\n      :host ::ng-deep .mdi-fast-forward:before {\n        content: \"\\F211\"; }\n      :host ::ng-deep .mdi-fast-forward-outline:before {\n        content: \"\\F6D1\"; }\n      :host ::ng-deep .mdi-fax:before {\n        content: \"\\F212\"; }\n      :host ::ng-deep .mdi-feather:before {\n        content: \"\\F6D2\"; }\n      :host ::ng-deep .mdi-ferry:before {\n        content: \"\\F213\"; }\n      :host ::ng-deep .mdi-file:before {\n        content: \"\\F214\"; }\n      :host ::ng-deep .mdi-file-chart:before {\n        content: \"\\F215\"; }\n      :host ::ng-deep .mdi-file-check:before {\n        content: \"\\F216\"; }\n      :host ::ng-deep .mdi-file-cloud:before {\n        content: \"\\F217\"; }\n      :host ::ng-deep .mdi-file-delimited:before {\n        content: \"\\F218\"; }\n      :host ::ng-deep .mdi-file-document:before {\n        content: \"\\F219\"; }\n      :host ::ng-deep .mdi-file-document-box:before {\n        content: \"\\F21A\"; }\n      :host ::ng-deep .mdi-file-excel:before {\n        content: \"\\F21B\"; }\n      :host ::ng-deep .mdi-file-excel-box:before {\n        content: \"\\F21C\"; }\n      :host ::ng-deep .mdi-file-export:before {\n        content: \"\\F21D\"; }\n      :host ::ng-deep .mdi-file-find:before {\n        content: \"\\F21E\"; }\n      :host ::ng-deep .mdi-file-hidden:before {\n        content: \"\\F613\"; }\n      :host ::ng-deep .mdi-file-image:before {\n        content: \"\\F21F\"; }\n      :host ::ng-deep .mdi-file-import:before {\n        content: \"\\F220\"; }\n      :host ::ng-deep .mdi-file-lock:before {\n        content: \"\\F221\"; }\n      :host ::ng-deep .mdi-file-multiple:before {\n        content: \"\\F222\"; }\n      :host ::ng-deep .mdi-file-music:before {\n        content: \"\\F223\"; }\n      :host ::ng-deep .mdi-file-outline:before {\n        content: \"\\F224\"; }\n      :host ::ng-deep .mdi-file-pdf:before {\n        content: \"\\F225\"; }\n      :host ::ng-deep .mdi-file-pdf-box:before {\n        content: \"\\F226\"; }\n      :host ::ng-deep .mdi-file-powerpoint:before {\n        content: \"\\F227\"; }\n      :host ::ng-deep .mdi-file-powerpoint-box:before {\n        content: \"\\F228\"; }\n      :host ::ng-deep .mdi-file-presentation-box:before {\n        content: \"\\F229\"; }\n      :host ::ng-deep .mdi-file-restore:before {\n        content: \"\\F670\"; }\n      :host ::ng-deep .mdi-file-send:before {\n        content: \"\\F22A\"; }\n      :host ::ng-deep .mdi-file-tree:before {\n        content: \"\\F645\"; }\n      :host ::ng-deep .mdi-file-video:before {\n        content: \"\\F22B\"; }\n      :host ::ng-deep .mdi-file-word:before {\n        content: \"\\F22C\"; }\n      :host ::ng-deep .mdi-file-word-box:before {\n        content: \"\\F22D\"; }\n      :host ::ng-deep .mdi-file-xml:before {\n        content: \"\\F22E\"; }\n      :host ::ng-deep .mdi-film:before {\n        content: \"\\F22F\"; }\n      :host ::ng-deep .mdi-filmstrip:before {\n        content: \"\\F230\"; }\n      :host ::ng-deep .mdi-filmstrip-off:before {\n        content: \"\\F231\"; }\n      :host ::ng-deep .mdi-filter:before {\n        content: \"\\F232\"; }\n      :host ::ng-deep .mdi-filter-outline:before {\n        content: \"\\F233\"; }\n      :host ::ng-deep .mdi-filter-remove:before {\n        content: \"\\F234\"; }\n      :host ::ng-deep .mdi-filter-remove-outline:before {\n        content: \"\\F235\"; }\n      :host ::ng-deep .mdi-filter-variant:before {\n        content: \"\\F236\"; }\n      :host ::ng-deep .mdi-find-replace:before {\n        content: \"\\F6D3\"; }\n      :host ::ng-deep .mdi-fingerprint:before {\n        content: \"\\F237\"; }\n      :host ::ng-deep .mdi-fire:before {\n        content: \"\\F238\"; }\n      :host ::ng-deep .mdi-firefox:before {\n        content: \"\\F239\"; }\n      :host ::ng-deep .mdi-fish:before {\n        content: \"\\F23A\"; }\n      :host ::ng-deep .mdi-flag:before {\n        content: \"\\F23B\"; }\n      :host ::ng-deep .mdi-flag-checkered:before {\n        content: \"\\F23C\"; }\n      :host ::ng-deep .mdi-flag-outline:before {\n        content: \"\\F23D\"; }\n      :host ::ng-deep .mdi-flag-outline-variant:before {\n        content: \"\\F23E\"; }\n      :host ::ng-deep .mdi-flag-triangle:before {\n        content: \"\\F23F\"; }\n      :host ::ng-deep .mdi-flag-variant:before {\n        content: \"\\F240\"; }\n      :host ::ng-deep .mdi-flash:before {\n        content: \"\\F241\"; }\n      :host ::ng-deep .mdi-flash-auto:before {\n        content: \"\\F242\"; }\n      :host ::ng-deep .mdi-flash-off:before {\n        content: \"\\F243\"; }\n      :host ::ng-deep .mdi-flash-outline:before {\n        content: \"\\F6D4\"; }\n      :host ::ng-deep .mdi-flash-red-eye:before {\n        content: \"\\F67A\"; }\n      :host ::ng-deep .mdi-flashlight:before {\n        content: \"\\F244\"; }\n      :host ::ng-deep .mdi-flashlight-off:before {\n        content: \"\\F245\"; }\n      :host ::ng-deep .mdi-flask:before {\n        content: \"\\F093\"; }\n      :host ::ng-deep .mdi-flask-empty:before {\n        content: \"\\F094\"; }\n      :host ::ng-deep .mdi-flask-empty-outline:before {\n        content: \"\\F095\"; }\n      :host ::ng-deep .mdi-flask-outline:before {\n        content: \"\\F096\"; }\n      :host ::ng-deep .mdi-flattr:before {\n        content: \"\\F246\"; }\n      :host ::ng-deep .mdi-flip-to-back:before {\n        content: \"\\F247\"; }\n      :host ::ng-deep .mdi-flip-to-front:before {\n        content: \"\\F248\"; }\n      :host ::ng-deep .mdi-floppy:before {\n        content: \"\\F249\"; }\n      :host ::ng-deep .mdi-flower:before {\n        content: \"\\F24A\"; }\n      :host ::ng-deep .mdi-folder:before {\n        content: \"\\F24B\"; }\n      :host ::ng-deep .mdi-folder-account:before {\n        content: \"\\F24C\"; }\n      :host ::ng-deep .mdi-folder-download:before {\n        content: \"\\F24D\"; }\n      :host ::ng-deep .mdi-folder-google-drive:before {\n        content: \"\\F24E\"; }\n      :host ::ng-deep .mdi-folder-image:before {\n        content: \"\\F24F\"; }\n      :host ::ng-deep .mdi-folder-lock:before {\n        content: \"\\F250\"; }\n      :host ::ng-deep .mdi-folder-lock-open:before {\n        content: \"\\F251\"; }\n      :host ::ng-deep .mdi-folder-move:before {\n        content: \"\\F252\"; }\n      :host ::ng-deep .mdi-folder-multiple:before {\n        content: \"\\F253\"; }\n      :host ::ng-deep .mdi-folder-multiple-image:before {\n        content: \"\\F254\"; }\n      :host ::ng-deep .mdi-folder-multiple-outline:before {\n        content: \"\\F255\"; }\n      :host ::ng-deep .mdi-folder-outline:before {\n        content: \"\\F256\"; }\n      :host ::ng-deep .mdi-folder-plus:before {\n        content: \"\\F257\"; }\n      :host ::ng-deep .mdi-folder-remove:before {\n        content: \"\\F258\"; }\n      :host ::ng-deep .mdi-folder-star:before {\n        content: \"\\F69C\"; }\n      :host ::ng-deep .mdi-folder-upload:before {\n        content: \"\\F259\"; }\n      :host ::ng-deep .mdi-font-awesome:before {\n        content: \"\\F03A\"; }\n      :host ::ng-deep .mdi-food:before {\n        content: \"\\F25A\"; }\n      :host ::ng-deep .mdi-food-apple:before {\n        content: \"\\F25B\"; }\n      :host ::ng-deep .mdi-food-fork-drink:before {\n        content: \"\\F5F2\"; }\n      :host ::ng-deep .mdi-food-off:before {\n        content: \"\\F5F3\"; }\n      :host ::ng-deep .mdi-food-variant:before {\n        content: \"\\F25C\"; }\n      :host ::ng-deep .mdi-football:before {\n        content: \"\\F25D\"; }\n      :host ::ng-deep .mdi-football-australian:before {\n        content: \"\\F25E\"; }\n      :host ::ng-deep .mdi-football-helmet:before {\n        content: \"\\F25F\"; }\n      :host ::ng-deep .mdi-format-align-center:before {\n        content: \"\\F260\"; }\n      :host ::ng-deep .mdi-format-align-justify:before {\n        content: \"\\F261\"; }\n      :host ::ng-deep .mdi-format-align-left:before {\n        content: \"\\F262\"; }\n      :host ::ng-deep .mdi-format-align-right:before {\n        content: \"\\F263\"; }\n      :host ::ng-deep .mdi-format-annotation-plus:before {\n        content: \"\\F646\"; }\n      :host ::ng-deep .mdi-format-bold:before {\n        content: \"\\F264\"; }\n      :host ::ng-deep .mdi-format-clear:before {\n        content: \"\\F265\"; }\n      :host ::ng-deep .mdi-format-color-fill:before {\n        content: \"\\F266\"; }\n      :host ::ng-deep .mdi-format-color-text:before {\n        content: \"\\F69D\"; }\n      :host ::ng-deep .mdi-format-float-center:before {\n        content: \"\\F267\"; }\n      :host ::ng-deep .mdi-format-float-left:before {\n        content: \"\\F268\"; }\n      :host ::ng-deep .mdi-format-float-none:before {\n        content: \"\\F269\"; }\n      :host ::ng-deep .mdi-format-float-right:before {\n        content: \"\\F26A\"; }\n      :host ::ng-deep .mdi-format-font:before {\n        content: \"\\F6D5\"; }\n      :host ::ng-deep .mdi-format-header-1:before {\n        content: \"\\F26B\"; }\n      :host ::ng-deep .mdi-format-header-2:before {\n        content: \"\\F26C\"; }\n      :host ::ng-deep .mdi-format-header-3:before {\n        content: \"\\F26D\"; }\n      :host ::ng-deep .mdi-format-header-4:before {\n        content: \"\\F26E\"; }\n      :host ::ng-deep .mdi-format-header-5:before {\n        content: \"\\F26F\"; }\n      :host ::ng-deep .mdi-format-header-6:before {\n        content: \"\\F270\"; }\n      :host ::ng-deep .mdi-format-header-decrease:before {\n        content: \"\\F271\"; }\n      :host ::ng-deep .mdi-format-header-equal:before {\n        content: \"\\F272\"; }\n      :host ::ng-deep .mdi-format-header-increase:before {\n        content: \"\\F273\"; }\n      :host ::ng-deep .mdi-format-header-pound:before {\n        content: \"\\F274\"; }\n      :host ::ng-deep .mdi-format-horizontal-align-center:before {\n        content: \"\\F61E\"; }\n      :host ::ng-deep .mdi-format-horizontal-align-left:before {\n        content: \"\\F61F\"; }\n      :host ::ng-deep .mdi-format-horizontal-align-right:before {\n        content: \"\\F620\"; }\n      :host ::ng-deep .mdi-format-indent-decrease:before {\n        content: \"\\F275\"; }\n      :host ::ng-deep .mdi-format-indent-increase:before {\n        content: \"\\F276\"; }\n      :host ::ng-deep .mdi-format-italic:before {\n        content: \"\\F277\"; }\n      :host ::ng-deep .mdi-format-line-spacing:before {\n        content: \"\\F278\"; }\n      :host ::ng-deep .mdi-format-line-style:before {\n        content: \"\\F5C8\"; }\n      :host ::ng-deep .mdi-format-line-weight:before {\n        content: \"\\F5C9\"; }\n      :host ::ng-deep .mdi-format-list-bulleted:before {\n        content: \"\\F279\"; }\n      :host ::ng-deep .mdi-format-list-bulleted-type:before {\n        content: \"\\F27A\"; }\n      :host ::ng-deep .mdi-format-list-numbers:before {\n        content: \"\\F27B\"; }\n      :host ::ng-deep .mdi-format-page-break:before {\n        content: \"\\F6D6\"; }\n      :host ::ng-deep .mdi-format-paint:before {\n        content: \"\\F27C\"; }\n      :host ::ng-deep .mdi-format-paragraph:before {\n        content: \"\\F27D\"; }\n      :host ::ng-deep .mdi-format-pilcrow:before {\n        content: \"\\F6D7\"; }\n      :host ::ng-deep .mdi-format-quote:before {\n        content: \"\\F27E\"; }\n      :host ::ng-deep .mdi-format-rotate-90:before {\n        content: \"\\F6A9\"; }\n      :host ::ng-deep .mdi-format-section:before {\n        content: \"\\F69E\"; }\n      :host ::ng-deep .mdi-format-size:before {\n        content: \"\\F27F\"; }\n      :host ::ng-deep .mdi-format-strikethrough:before {\n        content: \"\\F280\"; }\n      :host ::ng-deep .mdi-format-strikethrough-variant:before {\n        content: \"\\F281\"; }\n      :host ::ng-deep .mdi-format-subscript:before {\n        content: \"\\F282\"; }\n      :host ::ng-deep .mdi-format-superscript:before {\n        content: \"\\F283\"; }\n      :host ::ng-deep .mdi-format-text:before {\n        content: \"\\F284\"; }\n      :host ::ng-deep .mdi-format-textdirection-l-to-r:before {\n        content: \"\\F285\"; }\n      :host ::ng-deep .mdi-format-textdirection-r-to-l:before {\n        content: \"\\F286\"; }\n      :host ::ng-deep .mdi-format-title:before {\n        content: \"\\F5F4\"; }\n      :host ::ng-deep .mdi-format-underline:before {\n        content: \"\\F287\"; }\n      :host ::ng-deep .mdi-format-vertical-align-bottom:before {\n        content: \"\\F621\"; }\n      :host ::ng-deep .mdi-format-vertical-align-center:before {\n        content: \"\\F622\"; }\n      :host ::ng-deep .mdi-format-vertical-align-top:before {\n        content: \"\\F623\"; }\n      :host ::ng-deep .mdi-format-wrap-inline:before {\n        content: \"\\F288\"; }\n      :host ::ng-deep .mdi-format-wrap-square:before {\n        content: \"\\F289\"; }\n      :host ::ng-deep .mdi-format-wrap-tight:before {\n        content: \"\\F28A\"; }\n      :host ::ng-deep .mdi-format-wrap-top-bottom:before {\n        content: \"\\F28B\"; }\n      :host ::ng-deep .mdi-forum:before {\n        content: \"\\F28C\"; }\n      :host ::ng-deep .mdi-forward:before {\n        content: \"\\F28D\"; }\n      :host ::ng-deep .mdi-foursquare:before {\n        content: \"\\F28E\"; }\n      :host ::ng-deep .mdi-fridge:before {\n        content: \"\\F28F\"; }\n      :host ::ng-deep .mdi-fridge-filled:before {\n        content: \"\\F290\"; }\n      :host ::ng-deep .mdi-fridge-filled-bottom:before {\n        content: \"\\F291\"; }\n      :host ::ng-deep .mdi-fridge-filled-top:before {\n        content: \"\\F292\"; }\n      :host ::ng-deep .mdi-fullscreen:before {\n        content: \"\\F293\"; }\n      :host ::ng-deep .mdi-fullscreen-exit:before {\n        content: \"\\F294\"; }\n      :host ::ng-deep .mdi-function:before {\n        content: \"\\F295\"; }\n      :host ::ng-deep .mdi-gamepad:before {\n        content: \"\\F296\"; }\n      :host ::ng-deep .mdi-gamepad-variant:before {\n        content: \"\\F297\"; }\n      :host ::ng-deep .mdi-garage:before {\n        content: \"\\F6D8\"; }\n      :host ::ng-deep .mdi-garage-open:before {\n        content: \"\\F6D9\"; }\n      :host ::ng-deep .mdi-gas-cylinder:before {\n        content: \"\\F647\"; }\n      :host ::ng-deep .mdi-gas-station:before {\n        content: \"\\F298\"; }\n      :host ::ng-deep .mdi-gate:before {\n        content: \"\\F299\"; }\n      :host ::ng-deep .mdi-gauge:before {\n        content: \"\\F29A\"; }\n      :host ::ng-deep .mdi-gavel:before {\n        content: \"\\F29B\"; }\n      :host ::ng-deep .mdi-gender-female:before {\n        content: \"\\F29C\"; }\n      :host ::ng-deep .mdi-gender-male:before {\n        content: \"\\F29D\"; }\n      :host ::ng-deep .mdi-gender-male-female:before {\n        content: \"\\F29E\"; }\n      :host ::ng-deep .mdi-gender-transgender:before {\n        content: \"\\F29F\"; }\n      :host ::ng-deep .mdi-ghost:before {\n        content: \"\\F2A0\"; }\n      :host ::ng-deep .mdi-gift:before {\n        content: \"\\F2A1\"; }\n      :host ::ng-deep .mdi-git:before {\n        content: \"\\F2A2\"; }\n      :host ::ng-deep .mdi-github-box:before {\n        content: \"\\F2A3\"; }\n      :host ::ng-deep .mdi-github-circle:before {\n        content: \"\\F2A4\"; }\n      :host ::ng-deep .mdi-github-face:before {\n        content: \"\\F6DA\"; }\n      :host ::ng-deep .mdi-glass-flute:before {\n        content: \"\\F2A5\"; }\n      :host ::ng-deep .mdi-glass-mug:before {\n        content: \"\\F2A6\"; }\n      :host ::ng-deep .mdi-glass-stange:before {\n        content: \"\\F2A7\"; }\n      :host ::ng-deep .mdi-glass-tulip:before {\n        content: \"\\F2A8\"; }\n      :host ::ng-deep .mdi-glassdoor:before {\n        content: \"\\F2A9\"; }\n      :host ::ng-deep .mdi-glasses:before {\n        content: \"\\F2AA\"; }\n      :host ::ng-deep .mdi-gmail:before {\n        content: \"\\F2AB\"; }\n      :host ::ng-deep .mdi-gnome:before {\n        content: \"\\F2AC\"; }\n      :host ::ng-deep .mdi-gondola:before {\n        content: \"\\F685\"; }\n      :host ::ng-deep .mdi-google:before {\n        content: \"\\F2AD\"; }\n      :host ::ng-deep .mdi-google-cardboard:before {\n        content: \"\\F2AE\"; }\n      :host ::ng-deep .mdi-google-chrome:before {\n        content: \"\\F2AF\"; }\n      :host ::ng-deep .mdi-google-circles:before {\n        content: \"\\F2B0\"; }\n      :host ::ng-deep .mdi-google-circles-communities:before {\n        content: \"\\F2B1\"; }\n      :host ::ng-deep .mdi-google-circles-extended:before {\n        content: \"\\F2B2\"; }\n      :host ::ng-deep .mdi-google-circles-group:before {\n        content: \"\\F2B3\"; }\n      :host ::ng-deep .mdi-google-controller:before {\n        content: \"\\F2B4\"; }\n      :host ::ng-deep .mdi-google-controller-off:before {\n        content: \"\\F2B5\"; }\n      :host ::ng-deep .mdi-google-drive:before {\n        content: \"\\F2B6\"; }\n      :host ::ng-deep .mdi-google-earth:before {\n        content: \"\\F2B7\"; }\n      :host ::ng-deep .mdi-google-glass:before {\n        content: \"\\F2B8\"; }\n      :host ::ng-deep .mdi-google-keep:before {\n        content: \"\\F6DB\"; }\n      :host ::ng-deep .mdi-google-maps:before {\n        content: \"\\F5F5\"; }\n      :host ::ng-deep .mdi-google-nearby:before {\n        content: \"\\F2B9\"; }\n      :host ::ng-deep .mdi-google-pages:before {\n        content: \"\\F2BA\"; }\n      :host ::ng-deep .mdi-google-photos:before {\n        content: \"\\F6DC\"; }\n      :host ::ng-deep .mdi-google-physical-web:before {\n        content: \"\\F2BB\"; }\n      :host ::ng-deep .mdi-google-play:before {\n        content: \"\\F2BC\"; }\n      :host ::ng-deep .mdi-google-plus:before {\n        content: \"\\F2BD\"; }\n      :host ::ng-deep .mdi-google-plus-box:before {\n        content: \"\\F2BE\"; }\n      :host ::ng-deep .mdi-google-translate:before {\n        content: \"\\F2BF\"; }\n      :host ::ng-deep .mdi-google-wallet:before {\n        content: \"\\F2C0\"; }\n      :host ::ng-deep .mdi-gradient:before {\n        content: \"\\F69F\"; }\n      :host ::ng-deep .mdi-grease-pencil:before {\n        content: \"\\F648\"; }\n      :host ::ng-deep .mdi-grid:before {\n        content: \"\\F2C1\"; }\n      :host ::ng-deep .mdi-grid-off:before {\n        content: \"\\F2C2\"; }\n      :host ::ng-deep .mdi-group:before {\n        content: \"\\F2C3\"; }\n      :host ::ng-deep .mdi-guitar-electric:before {\n        content: \"\\F2C4\"; }\n      :host ::ng-deep .mdi-guitar-pick:before {\n        content: \"\\F2C5\"; }\n      :host ::ng-deep .mdi-guitar-pick-outline:before {\n        content: \"\\F2C6\"; }\n      :host ::ng-deep .mdi-hackernews:before {\n        content: \"\\F624\"; }\n      :host ::ng-deep .mdi-hamburger:before {\n        content: \"\\F684\"; }\n      :host ::ng-deep .mdi-hand-pointing-right:before {\n        content: \"\\F2C7\"; }\n      :host ::ng-deep .mdi-hanger:before {\n        content: \"\\F2C8\"; }\n      :host ::ng-deep .mdi-hangouts:before {\n        content: \"\\F2C9\"; }\n      :host ::ng-deep .mdi-harddisk:before {\n        content: \"\\F2CA\"; }\n      :host ::ng-deep .mdi-headphones:before {\n        content: \"\\F2CB\"; }\n      :host ::ng-deep .mdi-headphones-box:before {\n        content: \"\\F2CC\"; }\n      :host ::ng-deep .mdi-headphones-settings:before {\n        content: \"\\F2CD\"; }\n      :host ::ng-deep .mdi-headset:before {\n        content: \"\\F2CE\"; }\n      :host ::ng-deep .mdi-headset-dock:before {\n        content: \"\\F2CF\"; }\n      :host ::ng-deep .mdi-headset-off:before {\n        content: \"\\F2D0\"; }\n      :host ::ng-deep .mdi-heart:before {\n        content: \"\\F2D1\"; }\n      :host ::ng-deep .mdi-heart-box:before {\n        content: \"\\F2D2\"; }\n      :host ::ng-deep .mdi-heart-box-outline:before {\n        content: \"\\F2D3\"; }\n      :host ::ng-deep .mdi-heart-broken:before {\n        content: \"\\F2D4\"; }\n      :host ::ng-deep .mdi-heart-half-outline:before {\n        content: \"\\F6DD\"; }\n      :host ::ng-deep .mdi-heart-half-part:before {\n        content: \"\\F6DE\"; }\n      :host ::ng-deep .mdi-heart-half-part-outline:before {\n        content: \"\\F6DF\"; }\n      :host ::ng-deep .mdi-heart-outline:before {\n        content: \"\\F2D5\"; }\n      :host ::ng-deep .mdi-heart-pulse:before {\n        content: \"\\F5F6\"; }\n      :host ::ng-deep .mdi-help:before {\n        content: \"\\F2D6\"; }\n      :host ::ng-deep .mdi-help-circle:before {\n        content: \"\\F2D7\"; }\n      :host ::ng-deep .mdi-help-circle-outline:before {\n        content: \"\\F625\"; }\n      :host ::ng-deep .mdi-hexagon:before {\n        content: \"\\F2D8\"; }\n      :host ::ng-deep .mdi-hexagon-multiple:before {\n        content: \"\\F6E0\"; }\n      :host ::ng-deep .mdi-hexagon-outline:before {\n        content: \"\\F2D9\"; }\n      :host ::ng-deep .mdi-highway:before {\n        content: \"\\F5F7\"; }\n      :host ::ng-deep .mdi-history:before {\n        content: \"\\F2DA\"; }\n      :host ::ng-deep .mdi-hololens:before {\n        content: \"\\F2DB\"; }\n      :host ::ng-deep .mdi-home:before {\n        content: \"\\F2DC\"; }\n      :host ::ng-deep .mdi-home-map-marker:before {\n        content: \"\\F5F8\"; }\n      :host ::ng-deep .mdi-home-modern:before {\n        content: \"\\F2DD\"; }\n      :host ::ng-deep .mdi-home-outline:before {\n        content: \"\\F6A0\"; }\n      :host ::ng-deep .mdi-home-variant:before {\n        content: \"\\F2DE\"; }\n      :host ::ng-deep .mdi-hook:before {\n        content: \"\\F6E1\"; }\n      :host ::ng-deep .mdi-hook-off:before {\n        content: \"\\F6E2\"; }\n      :host ::ng-deep .mdi-hops:before {\n        content: \"\\F2DF\"; }\n      :host ::ng-deep .mdi-hospital:before {\n        content: \"\\F2E0\"; }\n      :host ::ng-deep .mdi-hospital-building:before {\n        content: \"\\F2E1\"; }\n      :host ::ng-deep .mdi-hospital-marker:before {\n        content: \"\\F2E2\"; }\n      :host ::ng-deep .mdi-hotel:before {\n        content: \"\\F2E3\"; }\n      :host ::ng-deep .mdi-houzz:before {\n        content: \"\\F2E4\"; }\n      :host ::ng-deep .mdi-houzz-box:before {\n        content: \"\\F2E5\"; }\n      :host ::ng-deep .mdi-human:before {\n        content: \"\\F2E6\"; }\n      :host ::ng-deep .mdi-human-child:before {\n        content: \"\\F2E7\"; }\n      :host ::ng-deep .mdi-human-female:before {\n        content: \"\\F649\"; }\n      :host ::ng-deep .mdi-human-greeting:before {\n        content: \"\\F64A\"; }\n      :host ::ng-deep .mdi-human-handsdown:before {\n        content: \"\\F64B\"; }\n      :host ::ng-deep .mdi-human-handsup:before {\n        content: \"\\F64C\"; }\n      :host ::ng-deep .mdi-human-male:before {\n        content: \"\\F64D\"; }\n      :host ::ng-deep .mdi-human-male-female:before {\n        content: \"\\F2E8\"; }\n      :host ::ng-deep .mdi-human-pregnant:before {\n        content: \"\\F5CF\"; }\n      :host ::ng-deep .mdi-image:before {\n        content: \"\\F2E9\"; }\n      :host ::ng-deep .mdi-image-album:before {\n        content: \"\\F2EA\"; }\n      :host ::ng-deep .mdi-image-area:before {\n        content: \"\\F2EB\"; }\n      :host ::ng-deep .mdi-image-area-close:before {\n        content: \"\\F2EC\"; }\n      :host ::ng-deep .mdi-image-broken:before {\n        content: \"\\F2ED\"; }\n      :host ::ng-deep .mdi-image-broken-variant:before {\n        content: \"\\F2EE\"; }\n      :host ::ng-deep .mdi-image-filter:before {\n        content: \"\\F2EF\"; }\n      :host ::ng-deep .mdi-image-filter-black-white:before {\n        content: \"\\F2F0\"; }\n      :host ::ng-deep .mdi-image-filter-center-focus:before {\n        content: \"\\F2F1\"; }\n      :host ::ng-deep .mdi-image-filter-center-focus-weak:before {\n        content: \"\\F2F2\"; }\n      :host ::ng-deep .mdi-image-filter-drama:before {\n        content: \"\\F2F3\"; }\n      :host ::ng-deep .mdi-image-filter-frames:before {\n        content: \"\\F2F4\"; }\n      :host ::ng-deep .mdi-image-filter-hdr:before {\n        content: \"\\F2F5\"; }\n      :host ::ng-deep .mdi-image-filter-none:before {\n        content: \"\\F2F6\"; }\n      :host ::ng-deep .mdi-image-filter-tilt-shift:before {\n        content: \"\\F2F7\"; }\n      :host ::ng-deep .mdi-image-filter-vintage:before {\n        content: \"\\F2F8\"; }\n      :host ::ng-deep .mdi-image-multiple:before {\n        content: \"\\F2F9\"; }\n      :host ::ng-deep .mdi-import:before {\n        content: \"\\F2FA\"; }\n      :host ::ng-deep .mdi-inbox:before {\n        content: \"\\F686\"; }\n      :host ::ng-deep .mdi-inbox-arrow-down:before {\n        content: \"\\F2FB\"; }\n      :host ::ng-deep .mdi-inbox-arrow-up:before {\n        content: \"\\F3D1\"; }\n      :host ::ng-deep .mdi-incognito:before {\n        content: \"\\F5F9\"; }\n      :host ::ng-deep .mdi-infinity:before {\n        content: \"\\F6E3\"; }\n      :host ::ng-deep .mdi-information:before {\n        content: \"\\F2FC\"; }\n      :host ::ng-deep .mdi-information-outline:before {\n        content: \"\\F2FD\"; }\n      :host ::ng-deep .mdi-information-variant:before {\n        content: \"\\F64E\"; }\n      :host ::ng-deep .mdi-instagram:before {\n        content: \"\\F2FE\"; }\n      :host ::ng-deep .mdi-instapaper:before {\n        content: \"\\F2FF\"; }\n      :host ::ng-deep .mdi-internet-explorer:before {\n        content: \"\\F300\"; }\n      :host ::ng-deep .mdi-invert-colors:before {\n        content: \"\\F301\"; }\n      :host ::ng-deep .mdi-itunes:before {\n        content: \"\\F676\"; }\n      :host ::ng-deep .mdi-jeepney:before {\n        content: \"\\F302\"; }\n      :host ::ng-deep .mdi-jira:before {\n        content: \"\\F303\"; }\n      :host ::ng-deep .mdi-jsfiddle:before {\n        content: \"\\F304\"; }\n      :host ::ng-deep .mdi-json:before {\n        content: \"\\F626\"; }\n      :host ::ng-deep .mdi-keg:before {\n        content: \"\\F305\"; }\n      :host ::ng-deep .mdi-kettle:before {\n        content: \"\\F5FA\"; }\n      :host ::ng-deep .mdi-key:before {\n        content: \"\\F306\"; }\n      :host ::ng-deep .mdi-key-change:before {\n        content: \"\\F307\"; }\n      :host ::ng-deep .mdi-key-minus:before {\n        content: \"\\F308\"; }\n      :host ::ng-deep .mdi-key-plus:before {\n        content: \"\\F309\"; }\n      :host ::ng-deep .mdi-key-remove:before {\n        content: \"\\F30A\"; }\n      :host ::ng-deep .mdi-key-variant:before {\n        content: \"\\F30B\"; }\n      :host ::ng-deep .mdi-keyboard:before {\n        content: \"\\F30C\"; }\n      :host ::ng-deep .mdi-keyboard-backspace:before {\n        content: \"\\F30D\"; }\n      :host ::ng-deep .mdi-keyboard-caps:before {\n        content: \"\\F30E\"; }\n      :host ::ng-deep .mdi-keyboard-close:before {\n        content: \"\\F30F\"; }\n      :host ::ng-deep .mdi-keyboard-off:before {\n        content: \"\\F310\"; }\n      :host ::ng-deep .mdi-keyboard-return:before {\n        content: \"\\F311\"; }\n      :host ::ng-deep .mdi-keyboard-tab:before {\n        content: \"\\F312\"; }\n      :host ::ng-deep .mdi-keyboard-variant:before {\n        content: \"\\F313\"; }\n      :host ::ng-deep .mdi-kodi:before {\n        content: \"\\F314\"; }\n      :host ::ng-deep .mdi-label:before {\n        content: \"\\F315\"; }\n      :host ::ng-deep .mdi-label-outline:before {\n        content: \"\\F316\"; }\n      :host ::ng-deep .mdi-lambda:before {\n        content: \"\\F627\"; }\n      :host ::ng-deep .mdi-lamp:before {\n        content: \"\\F6B4\"; }\n      :host ::ng-deep .mdi-lan:before {\n        content: \"\\F317\"; }\n      :host ::ng-deep .mdi-lan-connect:before {\n        content: \"\\F318\"; }\n      :host ::ng-deep .mdi-lan-disconnect:before {\n        content: \"\\F319\"; }\n      :host ::ng-deep .mdi-lan-pending:before {\n        content: \"\\F31A\"; }\n      :host ::ng-deep .mdi-language-c:before {\n        content: \"\\F671\"; }\n      :host ::ng-deep .mdi-language-cpp:before {\n        content: \"\\F672\"; }\n      :host ::ng-deep .mdi-language-csharp:before {\n        content: \"\\F31B\"; }\n      :host ::ng-deep .mdi-language-css3:before {\n        content: \"\\F31C\"; }\n      :host ::ng-deep .mdi-language-html5:before {\n        content: \"\\F31D\"; }\n      :host ::ng-deep .mdi-language-javascript:before {\n        content: \"\\F31E\"; }\n      :host ::ng-deep .mdi-language-php:before {\n        content: \"\\F31F\"; }\n      :host ::ng-deep .mdi-language-python:before {\n        content: \"\\F320\"; }\n      :host ::ng-deep .mdi-language-python-text:before {\n        content: \"\\F321\"; }\n      :host ::ng-deep .mdi-language-swift:before {\n        content: \"\\F6E4\"; }\n      :host ::ng-deep .mdi-language-typescript:before {\n        content: \"\\F6E5\"; }\n      :host ::ng-deep .mdi-laptop:before {\n        content: \"\\F322\"; }\n      :host ::ng-deep .mdi-laptop-chromebook:before {\n        content: \"\\F323\"; }\n      :host ::ng-deep .mdi-laptop-mac:before {\n        content: \"\\F324\"; }\n      :host ::ng-deep .mdi-laptop-off:before {\n        content: \"\\F6E6\"; }\n      :host ::ng-deep .mdi-laptop-windows:before {\n        content: \"\\F325\"; }\n      :host ::ng-deep .mdi-lastfm:before {\n        content: \"\\F326\"; }\n      :host ::ng-deep .mdi-launch:before {\n        content: \"\\F327\"; }\n      :host ::ng-deep .mdi-layers:before {\n        content: \"\\F328\"; }\n      :host ::ng-deep .mdi-layers-off:before {\n        content: \"\\F329\"; }\n      :host ::ng-deep .mdi-lead-pencil:before {\n        content: \"\\F64F\"; }\n      :host ::ng-deep .mdi-leaf:before {\n        content: \"\\F32A\"; }\n      :host ::ng-deep .mdi-led-off:before {\n        content: \"\\F32B\"; }\n      :host ::ng-deep .mdi-led-on:before {\n        content: \"\\F32C\"; }\n      :host ::ng-deep .mdi-led-outline:before {\n        content: \"\\F32D\"; }\n      :host ::ng-deep .mdi-led-variant-off:before {\n        content: \"\\F32E\"; }\n      :host ::ng-deep .mdi-led-variant-on:before {\n        content: \"\\F32F\"; }\n      :host ::ng-deep .mdi-led-variant-outline:before {\n        content: \"\\F330\"; }\n      :host ::ng-deep .mdi-library:before {\n        content: \"\\F331\"; }\n      :host ::ng-deep .mdi-library-books:before {\n        content: \"\\F332\"; }\n      :host ::ng-deep .mdi-library-music:before {\n        content: \"\\F333\"; }\n      :host ::ng-deep .mdi-library-plus:before {\n        content: \"\\F334\"; }\n      :host ::ng-deep .mdi-lightbulb:before {\n        content: \"\\F335\"; }\n      :host ::ng-deep .mdi-lightbulb-on:before {\n        content: \"\\F6E7\"; }\n      :host ::ng-deep .mdi-lightbulb-on-outline:before {\n        content: \"\\F6E8\"; }\n      :host ::ng-deep .mdi-lightbulb-outline:before {\n        content: \"\\F336\"; }\n      :host ::ng-deep .mdi-link:before {\n        content: \"\\F337\"; }\n      :host ::ng-deep .mdi-link-off:before {\n        content: \"\\F338\"; }\n      :host ::ng-deep .mdi-link-variant:before {\n        content: \"\\F339\"; }\n      :host ::ng-deep .mdi-link-variant-off:before {\n        content: \"\\F33A\"; }\n      :host ::ng-deep .mdi-linkedin:before {\n        content: \"\\F33B\"; }\n      :host ::ng-deep .mdi-linkedin-box:before {\n        content: \"\\F33C\"; }\n      :host ::ng-deep .mdi-linux:before {\n        content: \"\\F33D\"; }\n      :host ::ng-deep .mdi-lock:before {\n        content: \"\\F33E\"; }\n      :host ::ng-deep .mdi-lock-open:before {\n        content: \"\\F33F\"; }\n      :host ::ng-deep .mdi-lock-open-outline:before {\n        content: \"\\F340\"; }\n      :host ::ng-deep .mdi-lock-outline:before {\n        content: \"\\F341\"; }\n      :host ::ng-deep .mdi-lock-pattern:before {\n        content: \"\\F6E9\"; }\n      :host ::ng-deep .mdi-lock-plus:before {\n        content: \"\\F5FB\"; }\n      :host ::ng-deep .mdi-login:before {\n        content: \"\\F342\"; }\n      :host ::ng-deep .mdi-login-variant:before {\n        content: \"\\F5FC\"; }\n      :host ::ng-deep .mdi-logout:before {\n        content: \"\\F343\"; }\n      :host ::ng-deep .mdi-logout-variant:before {\n        content: \"\\F5FD\"; }\n      :host ::ng-deep .mdi-looks:before {\n        content: \"\\F344\"; }\n      :host ::ng-deep .mdi-loop:before {\n        content: \"\\F6EA\"; }\n      :host ::ng-deep .mdi-loupe:before {\n        content: \"\\F345\"; }\n      :host ::ng-deep .mdi-lumx:before {\n        content: \"\\F346\"; }\n      :host ::ng-deep .mdi-magnet:before {\n        content: \"\\F347\"; }\n      :host ::ng-deep .mdi-magnet-on:before {\n        content: \"\\F348\"; }\n      :host ::ng-deep .mdi-magnify:before {\n        content: \"\\F349\"; }\n      :host ::ng-deep .mdi-magnify-minus:before {\n        content: \"\\F34A\"; }\n      :host ::ng-deep .mdi-magnify-minus-outline:before {\n        content: \"\\F6EB\"; }\n      :host ::ng-deep .mdi-magnify-plus:before {\n        content: \"\\F34B\"; }\n      :host ::ng-deep .mdi-magnify-plus-outline:before {\n        content: \"\\F6EC\"; }\n      :host ::ng-deep .mdi-mail-ru:before {\n        content: \"\\F34C\"; }\n      :host ::ng-deep .mdi-mailbox:before {\n        content: \"\\F6ED\"; }\n      :host ::ng-deep .mdi-map:before {\n        content: \"\\F34D\"; }\n      :host ::ng-deep .mdi-map-marker:before {\n        content: \"\\F34E\"; }\n      :host ::ng-deep .mdi-map-marker-circle:before {\n        content: \"\\F34F\"; }\n      :host ::ng-deep .mdi-map-marker-minus:before {\n        content: \"\\F650\"; }\n      :host ::ng-deep .mdi-map-marker-multiple:before {\n        content: \"\\F350\"; }\n      :host ::ng-deep .mdi-map-marker-off:before {\n        content: \"\\F351\"; }\n      :host ::ng-deep .mdi-map-marker-plus:before {\n        content: \"\\F651\"; }\n      :host ::ng-deep .mdi-map-marker-radius:before {\n        content: \"\\F352\"; }\n      :host ::ng-deep .mdi-margin:before {\n        content: \"\\F353\"; }\n      :host ::ng-deep .mdi-markdown:before {\n        content: \"\\F354\"; }\n      :host ::ng-deep .mdi-marker:before {\n        content: \"\\F652\"; }\n      :host ::ng-deep .mdi-marker-check:before {\n        content: \"\\F355\"; }\n      :host ::ng-deep .mdi-martini:before {\n        content: \"\\F356\"; }\n      :host ::ng-deep .mdi-material-ui:before {\n        content: \"\\F357\"; }\n      :host ::ng-deep .mdi-math-compass:before {\n        content: \"\\F358\"; }\n      :host ::ng-deep .mdi-matrix:before {\n        content: \"\\F628\"; }\n      :host ::ng-deep .mdi-maxcdn:before {\n        content: \"\\F359\"; }\n      :host ::ng-deep .mdi-medical-bag:before {\n        content: \"\\F6EE\"; }\n      :host ::ng-deep .mdi-medium:before {\n        content: \"\\F35A\"; }\n      :host ::ng-deep .mdi-memory:before {\n        content: \"\\F35B\"; }\n      :host ::ng-deep .mdi-menu:before {\n        content: \"\\F35C\"; }\n      :host ::ng-deep .mdi-menu-down:before {\n        content: \"\\F35D\"; }\n      :host ::ng-deep .mdi-menu-down-outline:before {\n        content: \"\\F6B5\"; }\n      :host ::ng-deep .mdi-menu-left:before {\n        content: \"\\F35E\"; }\n      :host ::ng-deep .mdi-menu-right:before {\n        content: \"\\F35F\"; }\n      :host ::ng-deep .mdi-menu-up:before {\n        content: \"\\F360\"; }\n      :host ::ng-deep .mdi-menu-up-outline:before {\n        content: \"\\F6B6\"; }\n      :host ::ng-deep .mdi-message:before {\n        content: \"\\F361\"; }\n      :host ::ng-deep .mdi-message-alert:before {\n        content: \"\\F362\"; }\n      :host ::ng-deep .mdi-message-bulleted:before {\n        content: \"\\F6A1\"; }\n      :host ::ng-deep .mdi-message-bulleted-off:before {\n        content: \"\\F6A2\"; }\n      :host ::ng-deep .mdi-message-draw:before {\n        content: \"\\F363\"; }\n      :host ::ng-deep .mdi-message-image:before {\n        content: \"\\F364\"; }\n      :host ::ng-deep .mdi-message-outline:before {\n        content: \"\\F365\"; }\n      :host ::ng-deep .mdi-message-plus:before {\n        content: \"\\F653\"; }\n      :host ::ng-deep .mdi-message-processing:before {\n        content: \"\\F366\"; }\n      :host ::ng-deep .mdi-message-reply:before {\n        content: \"\\F367\"; }\n      :host ::ng-deep .mdi-message-reply-text:before {\n        content: \"\\F368\"; }\n      :host ::ng-deep .mdi-message-settings:before {\n        content: \"\\F6EF\"; }\n      :host ::ng-deep .mdi-message-settings-variant:before {\n        content: \"\\F6F0\"; }\n      :host ::ng-deep .mdi-message-text:before {\n        content: \"\\F369\"; }\n      :host ::ng-deep .mdi-message-text-outline:before {\n        content: \"\\F36A\"; }\n      :host ::ng-deep .mdi-message-video:before {\n        content: \"\\F36B\"; }\n      :host ::ng-deep .mdi-meteor:before {\n        content: \"\\F629\"; }\n      :host ::ng-deep .mdi-microphone:before {\n        content: \"\\F36C\"; }\n      :host ::ng-deep .mdi-microphone-off:before {\n        content: \"\\F36D\"; }\n      :host ::ng-deep .mdi-microphone-outline:before {\n        content: \"\\F36E\"; }\n      :host ::ng-deep .mdi-microphone-settings:before {\n        content: \"\\F36F\"; }\n      :host ::ng-deep .mdi-microphone-variant:before {\n        content: \"\\F370\"; }\n      :host ::ng-deep .mdi-microphone-variant-off:before {\n        content: \"\\F371\"; }\n      :host ::ng-deep .mdi-microscope:before {\n        content: \"\\F654\"; }\n      :host ::ng-deep .mdi-microsoft:before {\n        content: \"\\F372\"; }\n      :host ::ng-deep .mdi-minecraft:before {\n        content: \"\\F373\"; }\n      :host ::ng-deep .mdi-minus:before {\n        content: \"\\F374\"; }\n      :host ::ng-deep .mdi-minus-box:before {\n        content: \"\\F375\"; }\n      :host ::ng-deep .mdi-minus-box-outline:before {\n        content: \"\\F6F1\"; }\n      :host ::ng-deep .mdi-minus-circle:before {\n        content: \"\\F376\"; }\n      :host ::ng-deep .mdi-minus-circle-outline:before {\n        content: \"\\F377\"; }\n      :host ::ng-deep .mdi-minus-network:before {\n        content: \"\\F378\"; }\n      :host ::ng-deep .mdi-mixcloud:before {\n        content: \"\\F62A\"; }\n      :host ::ng-deep .mdi-monitor:before {\n        content: \"\\F379\"; }\n      :host ::ng-deep .mdi-monitor-multiple:before {\n        content: \"\\F37A\"; }\n      :host ::ng-deep .mdi-more:before {\n        content: \"\\F37B\"; }\n      :host ::ng-deep .mdi-motorbike:before {\n        content: \"\\F37C\"; }\n      :host ::ng-deep .mdi-mouse:before {\n        content: \"\\F37D\"; }\n      :host ::ng-deep .mdi-mouse-off:before {\n        content: \"\\F37E\"; }\n      :host ::ng-deep .mdi-mouse-variant:before {\n        content: \"\\F37F\"; }\n      :host ::ng-deep .mdi-mouse-variant-off:before {\n        content: \"\\F380\"; }\n      :host ::ng-deep .mdi-move-resize:before {\n        content: \"\\F655\"; }\n      :host ::ng-deep .mdi-move-resize-variant:before {\n        content: \"\\F656\"; }\n      :host ::ng-deep .mdi-movie:before {\n        content: \"\\F381\"; }\n      :host ::ng-deep .mdi-multiplication:before {\n        content: \"\\F382\"; }\n      :host ::ng-deep .mdi-multiplication-box:before {\n        content: \"\\F383\"; }\n      :host ::ng-deep .mdi-music-box:before {\n        content: \"\\F384\"; }\n      :host ::ng-deep .mdi-music-box-outline:before {\n        content: \"\\F385\"; }\n      :host ::ng-deep .mdi-music-circle:before {\n        content: \"\\F386\"; }\n      :host ::ng-deep .mdi-music-note:before {\n        content: \"\\F387\"; }\n      :host ::ng-deep .mdi-music-note-bluetooth:before {\n        content: \"\\F5FE\"; }\n      :host ::ng-deep .mdi-music-note-bluetooth-off:before {\n        content: \"\\F5FF\"; }\n      :host ::ng-deep .mdi-music-note-eighth:before {\n        content: \"\\F388\"; }\n      :host ::ng-deep .mdi-music-note-half:before {\n        content: \"\\F389\"; }\n      :host ::ng-deep .mdi-music-note-off:before {\n        content: \"\\F38A\"; }\n      :host ::ng-deep .mdi-music-note-quarter:before {\n        content: \"\\F38B\"; }\n      :host ::ng-deep .mdi-music-note-sixteenth:before {\n        content: \"\\F38C\"; }\n      :host ::ng-deep .mdi-music-note-whole:before {\n        content: \"\\F38D\"; }\n      :host ::ng-deep .mdi-nature:before {\n        content: \"\\F38E\"; }\n      :host ::ng-deep .mdi-nature-people:before {\n        content: \"\\F38F\"; }\n      :host ::ng-deep .mdi-navigation:before {\n        content: \"\\F390\"; }\n      :host ::ng-deep .mdi-near-me:before {\n        content: \"\\F5CD\"; }\n      :host ::ng-deep .mdi-needle:before {\n        content: \"\\F391\"; }\n      :host ::ng-deep .mdi-nest-protect:before {\n        content: \"\\F392\"; }\n      :host ::ng-deep .mdi-nest-thermostat:before {\n        content: \"\\F393\"; }\n      :host ::ng-deep .mdi-network:before {\n        content: \"\\F6F2\"; }\n      :host ::ng-deep .mdi-network-download:before {\n        content: \"\\F6F3\"; }\n      :host ::ng-deep .mdi-network-question:before {\n        content: \"\\F6F4\"; }\n      :host ::ng-deep .mdi-network-upload:before {\n        content: \"\\F6F5\"; }\n      :host ::ng-deep .mdi-new-box:before {\n        content: \"\\F394\"; }\n      :host ::ng-deep .mdi-newspaper:before {\n        content: \"\\F395\"; }\n      :host ::ng-deep .mdi-nfc:before {\n        content: \"\\F396\"; }\n      :host ::ng-deep .mdi-nfc-tap:before {\n        content: \"\\F397\"; }\n      :host ::ng-deep .mdi-nfc-variant:before {\n        content: \"\\F398\"; }\n      :host ::ng-deep .mdi-nodejs:before {\n        content: \"\\F399\"; }\n      :host ::ng-deep .mdi-note:before {\n        content: \"\\F39A\"; }\n      :host ::ng-deep .mdi-note-multiple:before {\n        content: \"\\F6B7\"; }\n      :host ::ng-deep .mdi-note-multiple-outline:before {\n        content: \"\\F6B8\"; }\n      :host ::ng-deep .mdi-note-outline:before {\n        content: \"\\F39B\"; }\n      :host ::ng-deep .mdi-note-plus:before {\n        content: \"\\F39C\"; }\n      :host ::ng-deep .mdi-note-plus-outline:before {\n        content: \"\\F39D\"; }\n      :host ::ng-deep .mdi-note-text:before {\n        content: \"\\F39E\"; }\n      :host ::ng-deep .mdi-notification-clear-all:before {\n        content: \"\\F39F\"; }\n      :host ::ng-deep .mdi-npm:before {\n        content: \"\\F6F6\"; }\n      :host ::ng-deep .mdi-nuke:before {\n        content: \"\\F6A3\"; }\n      :host ::ng-deep .mdi-numeric:before {\n        content: \"\\F3A0\"; }\n      :host ::ng-deep .mdi-numeric-0-box:before {\n        content: \"\\F3A1\"; }\n      :host ::ng-deep .mdi-numeric-0-box-multiple-outline:before {\n        content: \"\\F3A2\"; }\n      :host ::ng-deep .mdi-numeric-0-box-outline:before {\n        content: \"\\F3A3\"; }\n      :host ::ng-deep .mdi-numeric-1-box:before {\n        content: \"\\F3A4\"; }\n      :host ::ng-deep .mdi-numeric-1-box-multiple-outline:before {\n        content: \"\\F3A5\"; }\n      :host ::ng-deep .mdi-numeric-1-box-outline:before {\n        content: \"\\F3A6\"; }\n      :host ::ng-deep .mdi-numeric-2-box:before {\n        content: \"\\F3A7\"; }\n      :host ::ng-deep .mdi-numeric-2-box-multiple-outline:before {\n        content: \"\\F3A8\"; }\n      :host ::ng-deep .mdi-numeric-2-box-outline:before {\n        content: \"\\F3A9\"; }\n      :host ::ng-deep .mdi-numeric-3-box:before {\n        content: \"\\F3AA\"; }\n      :host ::ng-deep .mdi-numeric-3-box-multiple-outline:before {\n        content: \"\\F3AB\"; }\n      :host ::ng-deep .mdi-numeric-3-box-outline:before {\n        content: \"\\F3AC\"; }\n      :host ::ng-deep .mdi-numeric-4-box:before {\n        content: \"\\F3AD\"; }\n      :host ::ng-deep .mdi-numeric-4-box-multiple-outline:before {\n        content: \"\\F3AE\"; }\n      :host ::ng-deep .mdi-numeric-4-box-outline:before {\n        content: \"\\F3AF\"; }\n      :host ::ng-deep .mdi-numeric-5-box:before {\n        content: \"\\F3B0\"; }\n      :host ::ng-deep .mdi-numeric-5-box-multiple-outline:before {\n        content: \"\\F3B1\"; }\n      :host ::ng-deep .mdi-numeric-5-box-outline:before {\n        content: \"\\F3B2\"; }\n      :host ::ng-deep .mdi-numeric-6-box:before {\n        content: \"\\F3B3\"; }\n      :host ::ng-deep .mdi-numeric-6-box-multiple-outline:before {\n        content: \"\\F3B4\"; }\n      :host ::ng-deep .mdi-numeric-6-box-outline:before {\n        content: \"\\F3B5\"; }\n      :host ::ng-deep .mdi-numeric-7-box:before {\n        content: \"\\F3B6\"; }\n      :host ::ng-deep .mdi-numeric-7-box-multiple-outline:before {\n        content: \"\\F3B7\"; }\n      :host ::ng-deep .mdi-numeric-7-box-outline:before {\n        content: \"\\F3B8\"; }\n      :host ::ng-deep .mdi-numeric-8-box:before {\n        content: \"\\F3B9\"; }\n      :host ::ng-deep .mdi-numeric-8-box-multiple-outline:before {\n        content: \"\\F3BA\"; }\n      :host ::ng-deep .mdi-numeric-8-box-outline:before {\n        content: \"\\F3BB\"; }\n      :host ::ng-deep .mdi-numeric-9-box:before {\n        content: \"\\F3BC\"; }\n      :host ::ng-deep .mdi-numeric-9-box-multiple-outline:before {\n        content: \"\\F3BD\"; }\n      :host ::ng-deep .mdi-numeric-9-box-outline:before {\n        content: \"\\F3BE\"; }\n      :host ::ng-deep .mdi-numeric-9-plus-box:before {\n        content: \"\\F3BF\"; }\n      :host ::ng-deep .mdi-numeric-9-plus-box-multiple-outline:before {\n        content: \"\\F3C0\"; }\n      :host ::ng-deep .mdi-numeric-9-plus-box-outline:before {\n        content: \"\\F3C1\"; }\n      :host ::ng-deep .mdi-nut:before {\n        content: \"\\F6F7\"; }\n      :host ::ng-deep .mdi-nutrition:before {\n        content: \"\\F3C2\"; }\n      :host ::ng-deep .mdi-oar:before {\n        content: \"\\F67B\"; }\n      :host ::ng-deep .mdi-octagon:before {\n        content: \"\\F3C3\"; }\n      :host ::ng-deep .mdi-octagon-outline:before {\n        content: \"\\F3C4\"; }\n      :host ::ng-deep .mdi-octagram:before {\n        content: \"\\F6F8\"; }\n      :host ::ng-deep .mdi-odnoklassniki:before {\n        content: \"\\F3C5\"; }\n      :host ::ng-deep .mdi-office:before {\n        content: \"\\F3C6\"; }\n      :host ::ng-deep .mdi-oil:before {\n        content: \"\\F3C7\"; }\n      :host ::ng-deep .mdi-oil-temperature:before {\n        content: \"\\F3C8\"; }\n      :host ::ng-deep .mdi-omega:before {\n        content: \"\\F3C9\"; }\n      :host ::ng-deep .mdi-onedrive:before {\n        content: \"\\F3CA\"; }\n      :host ::ng-deep .mdi-opacity:before {\n        content: \"\\F5CC\"; }\n      :host ::ng-deep .mdi-open-in-app:before {\n        content: \"\\F3CB\"; }\n      :host ::ng-deep .mdi-open-in-new:before {\n        content: \"\\F3CC\"; }\n      :host ::ng-deep .mdi-openid:before {\n        content: \"\\F3CD\"; }\n      :host ::ng-deep .mdi-opera:before {\n        content: \"\\F3CE\"; }\n      :host ::ng-deep .mdi-ornament:before {\n        content: \"\\F3CF\"; }\n      :host ::ng-deep .mdi-ornament-variant:before {\n        content: \"\\F3D0\"; }\n      :host ::ng-deep .mdi-owl:before {\n        content: \"\\F3D2\"; }\n      :host ::ng-deep .mdi-package:before {\n        content: \"\\F3D3\"; }\n      :host ::ng-deep .mdi-package-down:before {\n        content: \"\\F3D4\"; }\n      :host ::ng-deep .mdi-package-up:before {\n        content: \"\\F3D5\"; }\n      :host ::ng-deep .mdi-package-variant:before {\n        content: \"\\F3D6\"; }\n      :host ::ng-deep .mdi-package-variant-closed:before {\n        content: \"\\F3D7\"; }\n      :host ::ng-deep .mdi-page-first:before {\n        content: \"\\F600\"; }\n      :host ::ng-deep .mdi-page-last:before {\n        content: \"\\F601\"; }\n      :host ::ng-deep .mdi-page-layout-body:before {\n        content: \"\\F6F9\"; }\n      :host ::ng-deep .mdi-page-layout-footer:before {\n        content: \"\\F6FA\"; }\n      :host ::ng-deep .mdi-page-layout-header:before {\n        content: \"\\F6FB\"; }\n      :host ::ng-deep .mdi-page-layout-sidebar-left:before {\n        content: \"\\F6FC\"; }\n      :host ::ng-deep .mdi-page-layout-sidebar-right:before {\n        content: \"\\F6FD\"; }\n      :host ::ng-deep .mdi-palette:before {\n        content: \"\\F3D8\"; }\n      :host ::ng-deep .mdi-palette-advanced:before {\n        content: \"\\F3D9\"; }\n      :host ::ng-deep .mdi-panda:before {\n        content: \"\\F3DA\"; }\n      :host ::ng-deep .mdi-pandora:before {\n        content: \"\\F3DB\"; }\n      :host ::ng-deep .mdi-panorama:before {\n        content: \"\\F3DC\"; }\n      :host ::ng-deep .mdi-panorama-fisheye:before {\n        content: \"\\F3DD\"; }\n      :host ::ng-deep .mdi-panorama-horizontal:before {\n        content: \"\\F3DE\"; }\n      :host ::ng-deep .mdi-panorama-vertical:before {\n        content: \"\\F3DF\"; }\n      :host ::ng-deep .mdi-panorama-wide-angle:before {\n        content: \"\\F3E0\"; }\n      :host ::ng-deep .mdi-paper-cut-vertical:before {\n        content: \"\\F3E1\"; }\n      :host ::ng-deep .mdi-paperclip:before {\n        content: \"\\F3E2\"; }\n      :host ::ng-deep .mdi-parking:before {\n        content: \"\\F3E3\"; }\n      :host ::ng-deep .mdi-pause:before {\n        content: \"\\F3E4\"; }\n      :host ::ng-deep .mdi-pause-circle:before {\n        content: \"\\F3E5\"; }\n      :host ::ng-deep .mdi-pause-circle-outline:before {\n        content: \"\\F3E6\"; }\n      :host ::ng-deep .mdi-pause-octagon:before {\n        content: \"\\F3E7\"; }\n      :host ::ng-deep .mdi-pause-octagon-outline:before {\n        content: \"\\F3E8\"; }\n      :host ::ng-deep .mdi-paw:before {\n        content: \"\\F3E9\"; }\n      :host ::ng-deep .mdi-paw-off:before {\n        content: \"\\F657\"; }\n      :host ::ng-deep .mdi-pen:before {\n        content: \"\\F3EA\"; }\n      :host ::ng-deep .mdi-pencil:before {\n        content: \"\\F3EB\"; }\n      :host ::ng-deep .mdi-pencil-box:before {\n        content: \"\\F3EC\"; }\n      :host ::ng-deep .mdi-pencil-box-outline:before {\n        content: \"\\F3ED\"; }\n      :host ::ng-deep .mdi-pencil-circle:before {\n        content: \"\\F6FE\"; }\n      :host ::ng-deep .mdi-pencil-lock:before {\n        content: \"\\F3EE\"; }\n      :host ::ng-deep .mdi-pencil-off:before {\n        content: \"\\F3EF\"; }\n      :host ::ng-deep .mdi-pentagon:before {\n        content: \"\\F6FF\"; }\n      :host ::ng-deep .mdi-pentagon-outline:before {\n        content: \"\\F700\"; }\n      :host ::ng-deep .mdi-percent:before {\n        content: \"\\F3F0\"; }\n      :host ::ng-deep .mdi-pharmacy:before {\n        content: \"\\F3F1\"; }\n      :host ::ng-deep .mdi-phone:before {\n        content: \"\\F3F2\"; }\n      :host ::ng-deep .mdi-phone-bluetooth:before {\n        content: \"\\F3F3\"; }\n      :host ::ng-deep .mdi-phone-classic:before {\n        content: \"\\F602\"; }\n      :host ::ng-deep .mdi-phone-forward:before {\n        content: \"\\F3F4\"; }\n      :host ::ng-deep .mdi-phone-hangup:before {\n        content: \"\\F3F5\"; }\n      :host ::ng-deep .mdi-phone-in-talk:before {\n        content: \"\\F3F6\"; }\n      :host ::ng-deep .mdi-phone-incoming:before {\n        content: \"\\F3F7\"; }\n      :host ::ng-deep .mdi-phone-locked:before {\n        content: \"\\F3F8\"; }\n      :host ::ng-deep .mdi-phone-log:before {\n        content: \"\\F3F9\"; }\n      :host ::ng-deep .mdi-phone-minus:before {\n        content: \"\\F658\"; }\n      :host ::ng-deep .mdi-phone-missed:before {\n        content: \"\\F3FA\"; }\n      :host ::ng-deep .mdi-phone-outgoing:before {\n        content: \"\\F3FB\"; }\n      :host ::ng-deep .mdi-phone-paused:before {\n        content: \"\\F3FC\"; }\n      :host ::ng-deep .mdi-phone-plus:before {\n        content: \"\\F659\"; }\n      :host ::ng-deep .mdi-phone-settings:before {\n        content: \"\\F3FD\"; }\n      :host ::ng-deep .mdi-phone-voip:before {\n        content: \"\\F3FE\"; }\n      :host ::ng-deep .mdi-pi:before {\n        content: \"\\F3FF\"; }\n      :host ::ng-deep .mdi-pi-box:before {\n        content: \"\\F400\"; }\n      :host ::ng-deep .mdi-piano:before {\n        content: \"\\F67C\"; }\n      :host ::ng-deep .mdi-pig:before {\n        content: \"\\F401\"; }\n      :host ::ng-deep .mdi-pill:before {\n        content: \"\\F402\"; }\n      :host ::ng-deep .mdi-pillar:before {\n        content: \"\\F701\"; }\n      :host ::ng-deep .mdi-pin:before {\n        content: \"\\F403\"; }\n      :host ::ng-deep .mdi-pin-off:before {\n        content: \"\\F404\"; }\n      :host ::ng-deep .mdi-pine-tree:before {\n        content: \"\\F405\"; }\n      :host ::ng-deep .mdi-pine-tree-box:before {\n        content: \"\\F406\"; }\n      :host ::ng-deep .mdi-pinterest:before {\n        content: \"\\F407\"; }\n      :host ::ng-deep .mdi-pinterest-box:before {\n        content: \"\\F408\"; }\n      :host ::ng-deep .mdi-pistol:before {\n        content: \"\\F702\"; }\n      :host ::ng-deep .mdi-pizza:before {\n        content: \"\\F409\"; }\n      :host ::ng-deep .mdi-plane-shield:before {\n        content: \"\\F6BA\"; }\n      :host ::ng-deep .mdi-play:before {\n        content: \"\\F40A\"; }\n      :host ::ng-deep .mdi-play-box-outline:before {\n        content: \"\\F40B\"; }\n      :host ::ng-deep .mdi-play-circle:before {\n        content: \"\\F40C\"; }\n      :host ::ng-deep .mdi-play-circle-outline:before {\n        content: \"\\F40D\"; }\n      :host ::ng-deep .mdi-play-pause:before {\n        content: \"\\F40E\"; }\n      :host ::ng-deep .mdi-play-protected-content:before {\n        content: \"\\F40F\"; }\n      :host ::ng-deep .mdi-playlist-check:before {\n        content: \"\\F5C7\"; }\n      :host ::ng-deep .mdi-playlist-minus:before {\n        content: \"\\F410\"; }\n      :host ::ng-deep .mdi-playlist-play:before {\n        content: \"\\F411\"; }\n      :host ::ng-deep .mdi-playlist-plus:before {\n        content: \"\\F412\"; }\n      :host ::ng-deep .mdi-playlist-remove:before {\n        content: \"\\F413\"; }\n      :host ::ng-deep .mdi-playstation:before {\n        content: \"\\F414\"; }\n      :host ::ng-deep .mdi-plex:before {\n        content: \"\\F6B9\"; }\n      :host ::ng-deep .mdi-plus:before {\n        content: \"\\F415\"; }\n      :host ::ng-deep .mdi-plus-box:before {\n        content: \"\\F416\"; }\n      :host ::ng-deep .mdi-plus-box-outline:before {\n        content: \"\\F703\"; }\n      :host ::ng-deep .mdi-plus-circle:before {\n        content: \"\\F417\"; }\n      :host ::ng-deep .mdi-plus-circle-multiple-outline:before {\n        content: \"\\F418\"; }\n      :host ::ng-deep .mdi-plus-circle-outline:before {\n        content: \"\\F419\"; }\n      :host ::ng-deep .mdi-plus-network:before {\n        content: \"\\F41A\"; }\n      :host ::ng-deep .mdi-plus-one:before {\n        content: \"\\F41B\"; }\n      :host ::ng-deep .mdi-plus-outline:before {\n        content: \"\\F704\"; }\n      :host ::ng-deep .mdi-pocket:before {\n        content: \"\\F41C\"; }\n      :host ::ng-deep .mdi-pokeball:before {\n        content: \"\\F41D\"; }\n      :host ::ng-deep .mdi-polaroid:before {\n        content: \"\\F41E\"; }\n      :host ::ng-deep .mdi-poll:before {\n        content: \"\\F41F\"; }\n      :host ::ng-deep .mdi-poll-box:before {\n        content: \"\\F420\"; }\n      :host ::ng-deep .mdi-polymer:before {\n        content: \"\\F421\"; }\n      :host ::ng-deep .mdi-pool:before {\n        content: \"\\F606\"; }\n      :host ::ng-deep .mdi-popcorn:before {\n        content: \"\\F422\"; }\n      :host ::ng-deep .mdi-pot:before {\n        content: \"\\F65A\"; }\n      :host ::ng-deep .mdi-pot-mix:before {\n        content: \"\\F65B\"; }\n      :host ::ng-deep .mdi-pound:before {\n        content: \"\\F423\"; }\n      :host ::ng-deep .mdi-pound-box:before {\n        content: \"\\F424\"; }\n      :host ::ng-deep .mdi-power:before {\n        content: \"\\F425\"; }\n      :host ::ng-deep .mdi-power-plug:before {\n        content: \"\\F6A4\"; }\n      :host ::ng-deep .mdi-power-plug-off:before {\n        content: \"\\F6A5\"; }\n      :host ::ng-deep .mdi-power-settings:before {\n        content: \"\\F426\"; }\n      :host ::ng-deep .mdi-power-socket:before {\n        content: \"\\F427\"; }\n      :host ::ng-deep .mdi-prescription:before {\n        content: \"\\F705\"; }\n      :host ::ng-deep .mdi-presentation:before {\n        content: \"\\F428\"; }\n      :host ::ng-deep .mdi-presentation-play:before {\n        content: \"\\F429\"; }\n      :host ::ng-deep .mdi-printer:before {\n        content: \"\\F42A\"; }\n      :host ::ng-deep .mdi-printer-3d:before {\n        content: \"\\F42B\"; }\n      :host ::ng-deep .mdi-printer-alert:before {\n        content: \"\\F42C\"; }\n      :host ::ng-deep .mdi-printer-settings:before {\n        content: \"\\F706\"; }\n      :host ::ng-deep .mdi-priority-high:before {\n        content: \"\\F603\"; }\n      :host ::ng-deep .mdi-priority-low:before {\n        content: \"\\F604\"; }\n      :host ::ng-deep .mdi-professional-hexagon:before {\n        content: \"\\F42D\"; }\n      :host ::ng-deep .mdi-projector:before {\n        content: \"\\F42E\"; }\n      :host ::ng-deep .mdi-projector-screen:before {\n        content: \"\\F42F\"; }\n      :host ::ng-deep .mdi-publish:before {\n        content: \"\\F6A6\"; }\n      :host ::ng-deep .mdi-pulse:before {\n        content: \"\\F430\"; }\n      :host ::ng-deep .mdi-puzzle:before {\n        content: \"\\F431\"; }\n      :host ::ng-deep .mdi-qqchat:before {\n        content: \"\\F605\"; }\n      :host ::ng-deep .mdi-qrcode:before {\n        content: \"\\F432\"; }\n      :host ::ng-deep .mdi-qrcode-scan:before {\n        content: \"\\F433\"; }\n      :host ::ng-deep .mdi-quadcopter:before {\n        content: \"\\F434\"; }\n      :host ::ng-deep .mdi-quality-high:before {\n        content: \"\\F435\"; }\n      :host ::ng-deep .mdi-quicktime:before {\n        content: \"\\F436\"; }\n      :host ::ng-deep .mdi-radar:before {\n        content: \"\\F437\"; }\n      :host ::ng-deep .mdi-radiator:before {\n        content: \"\\F438\"; }\n      :host ::ng-deep .mdi-radio:before {\n        content: \"\\F439\"; }\n      :host ::ng-deep .mdi-radio-handheld:before {\n        content: \"\\F43A\"; }\n      :host ::ng-deep .mdi-radio-tower:before {\n        content: \"\\F43B\"; }\n      :host ::ng-deep .mdi-radioactive:before {\n        content: \"\\F43C\"; }\n      :host ::ng-deep .mdi-radiobox-blank:before {\n        content: \"\\F43D\"; }\n      :host ::ng-deep .mdi-radiobox-marked:before {\n        content: \"\\F43E\"; }\n      :host ::ng-deep .mdi-raspberrypi:before {\n        content: \"\\F43F\"; }\n      :host ::ng-deep .mdi-ray-end:before {\n        content: \"\\F440\"; }\n      :host ::ng-deep .mdi-ray-end-arrow:before {\n        content: \"\\F441\"; }\n      :host ::ng-deep .mdi-ray-start:before {\n        content: \"\\F442\"; }\n      :host ::ng-deep .mdi-ray-start-arrow:before {\n        content: \"\\F443\"; }\n      :host ::ng-deep .mdi-ray-start-end:before {\n        content: \"\\F444\"; }\n      :host ::ng-deep .mdi-ray-vertex:before {\n        content: \"\\F445\"; }\n      :host ::ng-deep .mdi-rdio:before {\n        content: \"\\F446\"; }\n      :host ::ng-deep .mdi-react:before {\n        content: \"\\F707\"; }\n      :host ::ng-deep .mdi-read:before {\n        content: \"\\F447\"; }\n      :host ::ng-deep .mdi-readability:before {\n        content: \"\\F448\"; }\n      :host ::ng-deep .mdi-receipt:before {\n        content: \"\\F449\"; }\n      :host ::ng-deep .mdi-record:before {\n        content: \"\\F44A\"; }\n      :host ::ng-deep .mdi-record-rec:before {\n        content: \"\\F44B\"; }\n      :host ::ng-deep .mdi-recycle:before {\n        content: \"\\F44C\"; }\n      :host ::ng-deep .mdi-reddit:before {\n        content: \"\\F44D\"; }\n      :host ::ng-deep .mdi-redo:before {\n        content: \"\\F44E\"; }\n      :host ::ng-deep .mdi-redo-variant:before {\n        content: \"\\F44F\"; }\n      :host ::ng-deep .mdi-refresh:before {\n        content: \"\\F450\"; }\n      :host ::ng-deep .mdi-regex:before {\n        content: \"\\F451\"; }\n      :host ::ng-deep .mdi-relative-scale:before {\n        content: \"\\F452\"; }\n      :host ::ng-deep .mdi-reload:before {\n        content: \"\\F453\"; }\n      :host ::ng-deep .mdi-remote:before {\n        content: \"\\F454\"; }\n      :host ::ng-deep .mdi-rename-box:before {\n        content: \"\\F455\"; }\n      :host ::ng-deep .mdi-reorder-horizontal:before {\n        content: \"\\F687\"; }\n      :host ::ng-deep .mdi-reorder-vertical:before {\n        content: \"\\F688\"; }\n      :host ::ng-deep .mdi-repeat:before {\n        content: \"\\F456\"; }\n      :host ::ng-deep .mdi-repeat-off:before {\n        content: \"\\F457\"; }\n      :host ::ng-deep .mdi-repeat-once:before {\n        content: \"\\F458\"; }\n      :host ::ng-deep .mdi-replay:before {\n        content: \"\\F459\"; }\n      :host ::ng-deep .mdi-reply:before {\n        content: \"\\F45A\"; }\n      :host ::ng-deep .mdi-reply-all:before {\n        content: \"\\F45B\"; }\n      :host ::ng-deep .mdi-reproduction:before {\n        content: \"\\F45C\"; }\n      :host ::ng-deep .mdi-resize-bottom-right:before {\n        content: \"\\F45D\"; }\n      :host ::ng-deep .mdi-responsive:before {\n        content: \"\\F45E\"; }\n      :host ::ng-deep .mdi-restart:before {\n        content: \"\\F708\"; }\n      :host ::ng-deep .mdi-restore:before {\n        content: \"\\F6A7\"; }\n      :host ::ng-deep .mdi-rewind:before {\n        content: \"\\F45F\"; }\n      :host ::ng-deep .mdi-rewind-outline:before {\n        content: \"\\F709\"; }\n      :host ::ng-deep .mdi-rhombus:before {\n        content: \"\\F70A\"; }\n      :host ::ng-deep .mdi-rhombus-outline:before {\n        content: \"\\F70B\"; }\n      :host ::ng-deep .mdi-ribbon:before {\n        content: \"\\F460\"; }\n      :host ::ng-deep .mdi-road:before {\n        content: \"\\F461\"; }\n      :host ::ng-deep .mdi-road-variant:before {\n        content: \"\\F462\"; }\n      :host ::ng-deep .mdi-robot:before {\n        content: \"\\F6A8\"; }\n      :host ::ng-deep .mdi-rocket:before {\n        content: \"\\F463\"; }\n      :host ::ng-deep .mdi-roomba:before {\n        content: \"\\F70C\"; }\n      :host ::ng-deep .mdi-rotate-3d:before {\n        content: \"\\F464\"; }\n      :host ::ng-deep .mdi-rotate-left:before {\n        content: \"\\F465\"; }\n      :host ::ng-deep .mdi-rotate-left-variant:before {\n        content: \"\\F466\"; }\n      :host ::ng-deep .mdi-rotate-right:before {\n        content: \"\\F467\"; }\n      :host ::ng-deep .mdi-rotate-right-variant:before {\n        content: \"\\F468\"; }\n      :host ::ng-deep .mdi-rounded-corner:before {\n        content: \"\\F607\"; }\n      :host ::ng-deep .mdi-router-wireless:before {\n        content: \"\\F469\"; }\n      :host ::ng-deep .mdi-routes:before {\n        content: \"\\F46A\"; }\n      :host ::ng-deep .mdi-rowing:before {\n        content: \"\\F608\"; }\n      :host ::ng-deep .mdi-rss:before {\n        content: \"\\F46B\"; }\n      :host ::ng-deep .mdi-rss-box:before {\n        content: \"\\F46C\"; }\n      :host ::ng-deep .mdi-ruler:before {\n        content: \"\\F46D\"; }\n      :host ::ng-deep .mdi-run:before {\n        content: \"\\F70D\"; }\n      :host ::ng-deep .mdi-run-fast:before {\n        content: \"\\F46E\"; }\n      :host ::ng-deep .mdi-sale:before {\n        content: \"\\F46F\"; }\n      :host ::ng-deep .mdi-satellite:before {\n        content: \"\\F470\"; }\n      :host ::ng-deep .mdi-satellite-variant:before {\n        content: \"\\F471\"; }\n      :host ::ng-deep .mdi-saxophone:before {\n        content: \"\\F609\"; }\n      :host ::ng-deep .mdi-scale:before {\n        content: \"\\F472\"; }\n      :host ::ng-deep .mdi-scale-balance:before {\n        content: \"\\F5D1\"; }\n      :host ::ng-deep .mdi-scale-bathroom:before {\n        content: \"\\F473\"; }\n      :host ::ng-deep .mdi-scanner:before {\n        content: \"\\F6AA\"; }\n      :host ::ng-deep .mdi-school:before {\n        content: \"\\F474\"; }\n      :host ::ng-deep .mdi-screen-rotation:before {\n        content: \"\\F475\"; }\n      :host ::ng-deep .mdi-screen-rotation-lock:before {\n        content: \"\\F476\"; }\n      :host ::ng-deep .mdi-screwdriver:before {\n        content: \"\\F477\"; }\n      :host ::ng-deep .mdi-script:before {\n        content: \"\\F478\"; }\n      :host ::ng-deep .mdi-sd:before {\n        content: \"\\F479\"; }\n      :host ::ng-deep .mdi-seal:before {\n        content: \"\\F47A\"; }\n      :host ::ng-deep .mdi-search-web:before {\n        content: \"\\F70E\"; }\n      :host ::ng-deep .mdi-seat-flat:before {\n        content: \"\\F47B\"; }\n      :host ::ng-deep .mdi-seat-flat-angled:before {\n        content: \"\\F47C\"; }\n      :host ::ng-deep .mdi-seat-individual-suite:before {\n        content: \"\\F47D\"; }\n      :host ::ng-deep .mdi-seat-legroom-extra:before {\n        content: \"\\F47E\"; }\n      :host ::ng-deep .mdi-seat-legroom-normal:before {\n        content: \"\\F47F\"; }\n      :host ::ng-deep .mdi-seat-legroom-reduced:before {\n        content: \"\\F480\"; }\n      :host ::ng-deep .mdi-seat-recline-extra:before {\n        content: \"\\F481\"; }\n      :host ::ng-deep .mdi-seat-recline-normal:before {\n        content: \"\\F482\"; }\n      :host ::ng-deep .mdi-security:before {\n        content: \"\\F483\"; }\n      :host ::ng-deep .mdi-security-home:before {\n        content: \"\\F689\"; }\n      :host ::ng-deep .mdi-security-network:before {\n        content: \"\\F484\"; }\n      :host ::ng-deep .mdi-select:before {\n        content: \"\\F485\"; }\n      :host ::ng-deep .mdi-select-all:before {\n        content: \"\\F486\"; }\n      :host ::ng-deep .mdi-select-inverse:before {\n        content: \"\\F487\"; }\n      :host ::ng-deep .mdi-select-off:before {\n        content: \"\\F488\"; }\n      :host ::ng-deep .mdi-selection:before {\n        content: \"\\F489\"; }\n      :host ::ng-deep .mdi-send:before {\n        content: \"\\F48A\"; }\n      :host ::ng-deep .mdi-serial-port:before {\n        content: \"\\F65C\"; }\n      :host ::ng-deep .mdi-server:before {\n        content: \"\\F48B\"; }\n      :host ::ng-deep .mdi-server-minus:before {\n        content: \"\\F48C\"; }\n      :host ::ng-deep .mdi-server-network:before {\n        content: \"\\F48D\"; }\n      :host ::ng-deep .mdi-server-network-off:before {\n        content: \"\\F48E\"; }\n      :host ::ng-deep .mdi-server-off:before {\n        content: \"\\F48F\"; }\n      :host ::ng-deep .mdi-server-plus:before {\n        content: \"\\F490\"; }\n      :host ::ng-deep .mdi-server-remove:before {\n        content: \"\\F491\"; }\n      :host ::ng-deep .mdi-server-security:before {\n        content: \"\\F492\"; }\n      :host ::ng-deep .mdi-settings:before {\n        content: \"\\F493\"; }\n      :host ::ng-deep .mdi-settings-box:before {\n        content: \"\\F494\"; }\n      :host ::ng-deep .mdi-shape-circle-plus:before {\n        content: \"\\F65D\"; }\n      :host ::ng-deep .mdi-shape-plus:before {\n        content: \"\\F495\"; }\n      :host ::ng-deep .mdi-shape-polygon-plus:before {\n        content: \"\\F65E\"; }\n      :host ::ng-deep .mdi-shape-rectangle-plus:before {\n        content: \"\\F65F\"; }\n      :host ::ng-deep .mdi-shape-square-plus:before {\n        content: \"\\F660\"; }\n      :host ::ng-deep .mdi-share:before {\n        content: \"\\F496\"; }\n      :host ::ng-deep .mdi-share-variant:before {\n        content: \"\\F497\"; }\n      :host ::ng-deep .mdi-shield:before {\n        content: \"\\F498\"; }\n      :host ::ng-deep .mdi-shield-outline:before {\n        content: \"\\F499\"; }\n      :host ::ng-deep .mdi-shopping:before {\n        content: \"\\F49A\"; }\n      :host ::ng-deep .mdi-shopping-music:before {\n        content: \"\\F49B\"; }\n      :host ::ng-deep .mdi-shovel:before {\n        content: \"\\F70F\"; }\n      :host ::ng-deep .mdi-shovel-off:before {\n        content: \"\\F710\"; }\n      :host ::ng-deep .mdi-shredder:before {\n        content: \"\\F49C\"; }\n      :host ::ng-deep .mdi-shuffle:before {\n        content: \"\\F49D\"; }\n      :host ::ng-deep .mdi-shuffle-disabled:before {\n        content: \"\\F49E\"; }\n      :host ::ng-deep .mdi-shuffle-variant:before {\n        content: \"\\F49F\"; }\n      :host ::ng-deep .mdi-sigma:before {\n        content: \"\\F4A0\"; }\n      :host ::ng-deep .mdi-sigma-lower:before {\n        content: \"\\F62B\"; }\n      :host ::ng-deep .mdi-sign-caution:before {\n        content: \"\\F4A1\"; }\n      :host ::ng-deep .mdi-signal:before {\n        content: \"\\F4A2\"; }\n      :host ::ng-deep .mdi-signal-2g:before {\n        content: \"\\F711\"; }\n      :host ::ng-deep .mdi-signal-3g:before {\n        content: \"\\F712\"; }\n      :host ::ng-deep .mdi-signal-4g:before {\n        content: \"\\F713\"; }\n      :host ::ng-deep .mdi-signal-hspa:before {\n        content: \"\\F714\"; }\n      :host ::ng-deep .mdi-signal-hspa-plus:before {\n        content: \"\\F715\"; }\n      :host ::ng-deep .mdi-signal-variant:before {\n        content: \"\\F60A\"; }\n      :host ::ng-deep .mdi-silverware:before {\n        content: \"\\F4A3\"; }\n      :host ::ng-deep .mdi-silverware-fork:before {\n        content: \"\\F4A4\"; }\n      :host ::ng-deep .mdi-silverware-spoon:before {\n        content: \"\\F4A5\"; }\n      :host ::ng-deep .mdi-silverware-variant:before {\n        content: \"\\F4A6\"; }\n      :host ::ng-deep .mdi-sim:before {\n        content: \"\\F4A7\"; }\n      :host ::ng-deep .mdi-sim-alert:before {\n        content: \"\\F4A8\"; }\n      :host ::ng-deep .mdi-sim-off:before {\n        content: \"\\F4A9\"; }\n      :host ::ng-deep .mdi-sitemap:before {\n        content: \"\\F4AA\"; }\n      :host ::ng-deep .mdi-skip-backward:before {\n        content: \"\\F4AB\"; }\n      :host ::ng-deep .mdi-skip-forward:before {\n        content: \"\\F4AC\"; }\n      :host ::ng-deep .mdi-skip-next:before {\n        content: \"\\F4AD\"; }\n      :host ::ng-deep .mdi-skip-next-circle:before {\n        content: \"\\F661\"; }\n      :host ::ng-deep .mdi-skip-next-circle-outline:before {\n        content: \"\\F662\"; }\n      :host ::ng-deep .mdi-skip-previous:before {\n        content: \"\\F4AE\"; }\n      :host ::ng-deep .mdi-skip-previous-circle:before {\n        content: \"\\F663\"; }\n      :host ::ng-deep .mdi-skip-previous-circle-outline:before {\n        content: \"\\F664\"; }\n      :host ::ng-deep .mdi-skull:before {\n        content: \"\\F68B\"; }\n      :host ::ng-deep .mdi-skype:before {\n        content: \"\\F4AF\"; }\n      :host ::ng-deep .mdi-skype-business:before {\n        content: \"\\F4B0\"; }\n      :host ::ng-deep .mdi-slack:before {\n        content: \"\\F4B1\"; }\n      :host ::ng-deep .mdi-sleep:before {\n        content: \"\\F4B2\"; }\n      :host ::ng-deep .mdi-sleep-off:before {\n        content: \"\\F4B3\"; }\n      :host ::ng-deep .mdi-smoking:before {\n        content: \"\\F4B4\"; }\n      :host ::ng-deep .mdi-smoking-off:before {\n        content: \"\\F4B5\"; }\n      :host ::ng-deep .mdi-snapchat:before {\n        content: \"\\F4B6\"; }\n      :host ::ng-deep .mdi-snowflake:before {\n        content: \"\\F716\"; }\n      :host ::ng-deep .mdi-snowman:before {\n        content: \"\\F4B7\"; }\n      :host ::ng-deep .mdi-soccer:before {\n        content: \"\\F4B8\"; }\n      :host ::ng-deep .mdi-sofa:before {\n        content: \"\\F4B9\"; }\n      :host ::ng-deep .mdi-solid:before {\n        content: \"\\F68C\"; }\n      :host ::ng-deep .mdi-sort:before {\n        content: \"\\F4BA\"; }\n      :host ::ng-deep .mdi-sort-alphabetical:before {\n        content: \"\\F4BB\"; }\n      :host ::ng-deep .mdi-sort-ascending:before {\n        content: \"\\F4BC\"; }\n      :host ::ng-deep .mdi-sort-descending:before {\n        content: \"\\F4BD\"; }\n      :host ::ng-deep .mdi-sort-numeric:before {\n        content: \"\\F4BE\"; }\n      :host ::ng-deep .mdi-sort-variant:before {\n        content: \"\\F4BF\"; }\n      :host ::ng-deep .mdi-soundcloud:before {\n        content: \"\\F4C0\"; }\n      :host ::ng-deep .mdi-source-branch:before {\n        content: \"\\F62C\"; }\n      :host ::ng-deep .mdi-source-commit:before {\n        content: \"\\F717\"; }\n      :host ::ng-deep .mdi-source-commit-end:before {\n        content: \"\\F718\"; }\n      :host ::ng-deep .mdi-source-commit-end-local:before {\n        content: \"\\F719\"; }\n      :host ::ng-deep .mdi-source-commit-local:before {\n        content: \"\\F71A\"; }\n      :host ::ng-deep .mdi-source-commit-next-local:before {\n        content: \"\\F71B\"; }\n      :host ::ng-deep .mdi-source-commit-start:before {\n        content: \"\\F71C\"; }\n      :host ::ng-deep .mdi-source-commit-start-next-local:before {\n        content: \"\\F71D\"; }\n      :host ::ng-deep .mdi-source-fork:before {\n        content: \"\\F4C1\"; }\n      :host ::ng-deep .mdi-source-merge:before {\n        content: \"\\F62D\"; }\n      :host ::ng-deep .mdi-source-pull:before {\n        content: \"\\F4C2\"; }\n      :host ::ng-deep .mdi-speaker:before {\n        content: \"\\F4C3\"; }\n      :host ::ng-deep .mdi-speaker-off:before {\n        content: \"\\F4C4\"; }\n      :host ::ng-deep .mdi-speaker-wireless:before {\n        content: \"\\F71E\"; }\n      :host ::ng-deep .mdi-speedometer:before {\n        content: \"\\F4C5\"; }\n      :host ::ng-deep .mdi-spellcheck:before {\n        content: \"\\F4C6\"; }\n      :host ::ng-deep .mdi-spotify:before {\n        content: \"\\F4C7\"; }\n      :host ::ng-deep .mdi-spotlight:before {\n        content: \"\\F4C8\"; }\n      :host ::ng-deep .mdi-spotlight-beam:before {\n        content: \"\\F4C9\"; }\n      :host ::ng-deep .mdi-spray:before {\n        content: \"\\F665\"; }\n      :host ::ng-deep .mdi-square-inc:before {\n        content: \"\\F4CA\"; }\n      :host ::ng-deep .mdi-square-inc-cash:before {\n        content: \"\\F4CB\"; }\n      :host ::ng-deep .mdi-stackexchange:before {\n        content: \"\\F60B\"; }\n      :host ::ng-deep .mdi-stackoverflow:before {\n        content: \"\\F4CC\"; }\n      :host ::ng-deep .mdi-stadium:before {\n        content: \"\\F71F\"; }\n      :host ::ng-deep .mdi-stairs:before {\n        content: \"\\F4CD\"; }\n      :host ::ng-deep .mdi-star:before {\n        content: \"\\F4CE\"; }\n      :host ::ng-deep .mdi-star-circle:before {\n        content: \"\\F4CF\"; }\n      :host ::ng-deep .mdi-star-half:before {\n        content: \"\\F4D0\"; }\n      :host ::ng-deep .mdi-star-off:before {\n        content: \"\\F4D1\"; }\n      :host ::ng-deep .mdi-star-outline:before {\n        content: \"\\F4D2\"; }\n      :host ::ng-deep .mdi-steam:before {\n        content: \"\\F4D3\"; }\n      :host ::ng-deep .mdi-steering:before {\n        content: \"\\F4D4\"; }\n      :host ::ng-deep .mdi-step-backward:before {\n        content: \"\\F4D5\"; }\n      :host ::ng-deep .mdi-step-backward-2:before {\n        content: \"\\F4D6\"; }\n      :host ::ng-deep .mdi-step-forward:before {\n        content: \"\\F4D7\"; }\n      :host ::ng-deep .mdi-step-forward-2:before {\n        content: \"\\F4D8\"; }\n      :host ::ng-deep .mdi-stethoscope:before {\n        content: \"\\F4D9\"; }\n      :host ::ng-deep .mdi-sticker:before {\n        content: \"\\F5D0\"; }\n      :host ::ng-deep .mdi-stocking:before {\n        content: \"\\F4DA\"; }\n      :host ::ng-deep .mdi-stop:before {\n        content: \"\\F4DB\"; }\n      :host ::ng-deep .mdi-stop-circle:before {\n        content: \"\\F666\"; }\n      :host ::ng-deep .mdi-stop-circle-outline:before {\n        content: \"\\F667\"; }\n      :host ::ng-deep .mdi-store:before {\n        content: \"\\F4DC\"; }\n      :host ::ng-deep .mdi-store-24-hour:before {\n        content: \"\\F4DD\"; }\n      :host ::ng-deep .mdi-stove:before {\n        content: \"\\F4DE\"; }\n      :host ::ng-deep .mdi-subdirectory-arrow-left:before {\n        content: \"\\F60C\"; }\n      :host ::ng-deep .mdi-subdirectory-arrow-right:before {\n        content: \"\\F60D\"; }\n      :host ::ng-deep .mdi-subway:before {\n        content: \"\\F6AB\"; }\n      :host ::ng-deep .mdi-subway-variant:before {\n        content: \"\\F4DF\"; }\n      :host ::ng-deep .mdi-sunglasses:before {\n        content: \"\\F4E0\"; }\n      :host ::ng-deep .mdi-surround-sound:before {\n        content: \"\\F5C5\"; }\n      :host ::ng-deep .mdi-svg:before {\n        content: \"\\F720\"; }\n      :host ::ng-deep .mdi-swap-horizontal:before {\n        content: \"\\F4E1\"; }\n      :host ::ng-deep .mdi-swap-vertical:before {\n        content: \"\\F4E2\"; }\n      :host ::ng-deep .mdi-swim:before {\n        content: \"\\F4E3\"; }\n      :host ::ng-deep .mdi-switch:before {\n        content: \"\\F4E4\"; }\n      :host ::ng-deep .mdi-sword:before {\n        content: \"\\F4E5\"; }\n      :host ::ng-deep .mdi-sync:before {\n        content: \"\\F4E6\"; }\n      :host ::ng-deep .mdi-sync-alert:before {\n        content: \"\\F4E7\"; }\n      :host ::ng-deep .mdi-sync-off:before {\n        content: \"\\F4E8\"; }\n      :host ::ng-deep .mdi-tab:before {\n        content: \"\\F4E9\"; }\n      :host ::ng-deep .mdi-tab-unselected:before {\n        content: \"\\F4EA\"; }\n      :host ::ng-deep .mdi-table:before {\n        content: \"\\F4EB\"; }\n      :host ::ng-deep .mdi-table-column-plus-after:before {\n        content: \"\\F4EC\"; }\n      :host ::ng-deep .mdi-table-column-plus-before:before {\n        content: \"\\F4ED\"; }\n      :host ::ng-deep .mdi-table-column-remove:before {\n        content: \"\\F4EE\"; }\n      :host ::ng-deep .mdi-table-column-width:before {\n        content: \"\\F4EF\"; }\n      :host ::ng-deep .mdi-table-edit:before {\n        content: \"\\F4F0\"; }\n      :host ::ng-deep .mdi-table-large:before {\n        content: \"\\F4F1\"; }\n      :host ::ng-deep .mdi-table-row-height:before {\n        content: \"\\F4F2\"; }\n      :host ::ng-deep .mdi-table-row-plus-after:before {\n        content: \"\\F4F3\"; }\n      :host ::ng-deep .mdi-table-row-plus-before:before {\n        content: \"\\F4F4\"; }\n      :host ::ng-deep .mdi-table-row-remove:before {\n        content: \"\\F4F5\"; }\n      :host ::ng-deep .mdi-tablet:before {\n        content: \"\\F4F6\"; }\n      :host ::ng-deep .mdi-tablet-android:before {\n        content: \"\\F4F7\"; }\n      :host ::ng-deep .mdi-tablet-ipad:before {\n        content: \"\\F4F8\"; }\n      :host ::ng-deep .mdi-tag:before {\n        content: \"\\F4F9\"; }\n      :host ::ng-deep .mdi-tag-faces:before {\n        content: \"\\F4FA\"; }\n      :host ::ng-deep .mdi-tag-heart:before {\n        content: \"\\F68A\"; }\n      :host ::ng-deep .mdi-tag-multiple:before {\n        content: \"\\F4FB\"; }\n      :host ::ng-deep .mdi-tag-outline:before {\n        content: \"\\F4FC\"; }\n      :host ::ng-deep .mdi-tag-plus:before {\n        content: \"\\F721\"; }\n      :host ::ng-deep .mdi-tag-remove:before {\n        content: \"\\F722\"; }\n      :host ::ng-deep .mdi-tag-text-outline:before {\n        content: \"\\F4FD\"; }\n      :host ::ng-deep .mdi-target:before {\n        content: \"\\F4FE\"; }\n      :host ::ng-deep .mdi-taxi:before {\n        content: \"\\F4FF\"; }\n      :host ::ng-deep .mdi-teamviewer:before {\n        content: \"\\F500\"; }\n      :host ::ng-deep .mdi-telegram:before {\n        content: \"\\F501\"; }\n      :host ::ng-deep .mdi-television:before {\n        content: \"\\F502\"; }\n      :host ::ng-deep .mdi-television-guide:before {\n        content: \"\\F503\"; }\n      :host ::ng-deep .mdi-temperature-celsius:before {\n        content: \"\\F504\"; }\n      :host ::ng-deep .mdi-temperature-fahrenheit:before {\n        content: \"\\F505\"; }\n      :host ::ng-deep .mdi-temperature-kelvin:before {\n        content: \"\\F506\"; }\n      :host ::ng-deep .mdi-tennis:before {\n        content: \"\\F507\"; }\n      :host ::ng-deep .mdi-tent:before {\n        content: \"\\F508\"; }\n      :host ::ng-deep .mdi-terrain:before {\n        content: \"\\F509\"; }\n      :host ::ng-deep .mdi-test-tube:before {\n        content: \"\\F668\"; }\n      :host ::ng-deep .mdi-text-shadow:before {\n        content: \"\\F669\"; }\n      :host ::ng-deep .mdi-text-to-speech:before {\n        content: \"\\F50A\"; }\n      :host ::ng-deep .mdi-text-to-speech-off:before {\n        content: \"\\F50B\"; }\n      :host ::ng-deep .mdi-textbox:before {\n        content: \"\\F60E\"; }\n      :host ::ng-deep .mdi-texture:before {\n        content: \"\\F50C\"; }\n      :host ::ng-deep .mdi-theater:before {\n        content: \"\\F50D\"; }\n      :host ::ng-deep .mdi-theme-light-dark:before {\n        content: \"\\F50E\"; }\n      :host ::ng-deep .mdi-thermometer:before {\n        content: \"\\F50F\"; }\n      :host ::ng-deep .mdi-thermometer-lines:before {\n        content: \"\\F510\"; }\n      :host ::ng-deep .mdi-thumb-down:before {\n        content: \"\\F511\"; }\n      :host ::ng-deep .mdi-thumb-down-outline:before {\n        content: \"\\F512\"; }\n      :host ::ng-deep .mdi-thumb-up:before {\n        content: \"\\F513\"; }\n      :host ::ng-deep .mdi-thumb-up-outline:before {\n        content: \"\\F514\"; }\n      :host ::ng-deep .mdi-thumbs-up-down:before {\n        content: \"\\F515\"; }\n      :host ::ng-deep .mdi-ticket:before {\n        content: \"\\F516\"; }\n      :host ::ng-deep .mdi-ticket-account:before {\n        content: \"\\F517\"; }\n      :host ::ng-deep .mdi-ticket-confirmation:before {\n        content: \"\\F518\"; }\n      :host ::ng-deep .mdi-ticket-percent:before {\n        content: \"\\F723\"; }\n      :host ::ng-deep .mdi-tie:before {\n        content: \"\\F519\"; }\n      :host ::ng-deep .mdi-tilde:before {\n        content: \"\\F724\"; }\n      :host ::ng-deep .mdi-timelapse:before {\n        content: \"\\F51A\"; }\n      :host ::ng-deep .mdi-timer:before {\n        content: \"\\F51B\"; }\n      :host ::ng-deep .mdi-timer-10:before {\n        content: \"\\F51C\"; }\n      :host ::ng-deep .mdi-timer-3:before {\n        content: \"\\F51D\"; }\n      :host ::ng-deep .mdi-timer-off:before {\n        content: \"\\F51E\"; }\n      :host ::ng-deep .mdi-timer-sand:before {\n        content: \"\\F51F\"; }\n      :host ::ng-deep .mdi-timer-sand-empty:before {\n        content: \"\\F6AC\"; }\n      :host ::ng-deep .mdi-timetable:before {\n        content: \"\\F520\"; }\n      :host ::ng-deep .mdi-toggle-switch:before {\n        content: \"\\F521\"; }\n      :host ::ng-deep .mdi-toggle-switch-off:before {\n        content: \"\\F522\"; }\n      :host ::ng-deep .mdi-tooltip:before {\n        content: \"\\F523\"; }\n      :host ::ng-deep .mdi-tooltip-edit:before {\n        content: \"\\F524\"; }\n      :host ::ng-deep .mdi-tooltip-image:before {\n        content: \"\\F525\"; }\n      :host ::ng-deep .mdi-tooltip-outline:before {\n        content: \"\\F526\"; }\n      :host ::ng-deep .mdi-tooltip-outline-plus:before {\n        content: \"\\F527\"; }\n      :host ::ng-deep .mdi-tooltip-text:before {\n        content: \"\\F528\"; }\n      :host ::ng-deep .mdi-tooth:before {\n        content: \"\\F529\"; }\n      :host ::ng-deep .mdi-tor:before {\n        content: \"\\F52A\"; }\n      :host ::ng-deep .mdi-tower-beach:before {\n        content: \"\\F680\"; }\n      :host ::ng-deep .mdi-tower-fire:before {\n        content: \"\\F681\"; }\n      :host ::ng-deep .mdi-traffic-light:before {\n        content: \"\\F52B\"; }\n      :host ::ng-deep .mdi-train:before {\n        content: \"\\F52C\"; }\n      :host ::ng-deep .mdi-tram:before {\n        content: \"\\F52D\"; }\n      :host ::ng-deep .mdi-transcribe:before {\n        content: \"\\F52E\"; }\n      :host ::ng-deep .mdi-transcribe-close:before {\n        content: \"\\F52F\"; }\n      :host ::ng-deep .mdi-transfer:before {\n        content: \"\\F530\"; }\n      :host ::ng-deep .mdi-transit-transfer:before {\n        content: \"\\F6AD\"; }\n      :host ::ng-deep .mdi-translate:before {\n        content: \"\\F5CA\"; }\n      :host ::ng-deep .mdi-treasure-chest:before {\n        content: \"\\F725\"; }\n      :host ::ng-deep .mdi-tree:before {\n        content: \"\\F531\"; }\n      :host ::ng-deep .mdi-trello:before {\n        content: \"\\F532\"; }\n      :host ::ng-deep .mdi-trending-down:before {\n        content: \"\\F533\"; }\n      :host ::ng-deep .mdi-trending-neutral:before {\n        content: \"\\F534\"; }\n      :host ::ng-deep .mdi-trending-up:before {\n        content: \"\\F535\"; }\n      :host ::ng-deep .mdi-triangle:before {\n        content: \"\\F536\"; }\n      :host ::ng-deep .mdi-triangle-outline:before {\n        content: \"\\F537\"; }\n      :host ::ng-deep .mdi-trophy:before {\n        content: \"\\F538\"; }\n      :host ::ng-deep .mdi-trophy-award:before {\n        content: \"\\F539\"; }\n      :host ::ng-deep .mdi-trophy-outline:before {\n        content: \"\\F53A\"; }\n      :host ::ng-deep .mdi-trophy-variant:before {\n        content: \"\\F53B\"; }\n      :host ::ng-deep .mdi-trophy-variant-outline:before {\n        content: \"\\F53C\"; }\n      :host ::ng-deep .mdi-truck:before {\n        content: \"\\F53D\"; }\n      :host ::ng-deep .mdi-truck-delivery:before {\n        content: \"\\F53E\"; }\n      :host ::ng-deep .mdi-truck-trailer:before {\n        content: \"\\F726\"; }\n      :host ::ng-deep .mdi-tshirt-crew:before {\n        content: \"\\F53F\"; }\n      :host ::ng-deep .mdi-tshirt-v:before {\n        content: \"\\F540\"; }\n      :host ::ng-deep .mdi-tumblr:before {\n        content: \"\\F541\"; }\n      :host ::ng-deep .mdi-tumblr-reblog:before {\n        content: \"\\F542\"; }\n      :host ::ng-deep .mdi-tune:before {\n        content: \"\\F62E\"; }\n      :host ::ng-deep .mdi-tune-vertical:before {\n        content: \"\\F66A\"; }\n      :host ::ng-deep .mdi-twitch:before {\n        content: \"\\F543\"; }\n      :host ::ng-deep .mdi-twitter:before {\n        content: \"\\F544\"; }\n      :host ::ng-deep .mdi-twitter-box:before {\n        content: \"\\F545\"; }\n      :host ::ng-deep .mdi-twitter-circle:before {\n        content: \"\\F546\"; }\n      :host ::ng-deep .mdi-twitter-retweet:before {\n        content: \"\\F547\"; }\n      :host ::ng-deep .mdi-ubuntu:before {\n        content: \"\\F548\"; }\n      :host ::ng-deep .mdi-umbraco:before {\n        content: \"\\F549\"; }\n      :host ::ng-deep .mdi-umbrella:before {\n        content: \"\\F54A\"; }\n      :host ::ng-deep .mdi-umbrella-outline:before {\n        content: \"\\F54B\"; }\n      :host ::ng-deep .mdi-undo:before {\n        content: \"\\F54C\"; }\n      :host ::ng-deep .mdi-undo-variant:before {\n        content: \"\\F54D\"; }\n      :host ::ng-deep .mdi-unfold-less:before {\n        content: \"\\F54E\"; }\n      :host ::ng-deep .mdi-unfold-more:before {\n        content: \"\\F54F\"; }\n      :host ::ng-deep .mdi-ungroup:before {\n        content: \"\\F550\"; }\n      :host ::ng-deep .mdi-unity:before {\n        content: \"\\F6AE\"; }\n      :host ::ng-deep .mdi-untappd:before {\n        content: \"\\F551\"; }\n      :host ::ng-deep .mdi-update:before {\n        content: \"\\F6AF\"; }\n      :host ::ng-deep .mdi-upload:before {\n        content: \"\\F552\"; }\n      :host ::ng-deep .mdi-usb:before {\n        content: \"\\F553\"; }\n      :host ::ng-deep .mdi-vector-arrange-above:before {\n        content: \"\\F554\"; }\n      :host ::ng-deep .mdi-vector-arrange-below:before {\n        content: \"\\F555\"; }\n      :host ::ng-deep .mdi-vector-circle:before {\n        content: \"\\F556\"; }\n      :host ::ng-deep .mdi-vector-circle-variant:before {\n        content: \"\\F557\"; }\n      :host ::ng-deep .mdi-vector-combine:before {\n        content: \"\\F558\"; }\n      :host ::ng-deep .mdi-vector-curve:before {\n        content: \"\\F559\"; }\n      :host ::ng-deep .mdi-vector-difference:before {\n        content: \"\\F55A\"; }\n      :host ::ng-deep .mdi-vector-difference-ab:before {\n        content: \"\\F55B\"; }\n      :host ::ng-deep .mdi-vector-difference-ba:before {\n        content: \"\\F55C\"; }\n      :host ::ng-deep .mdi-vector-intersection:before {\n        content: \"\\F55D\"; }\n      :host ::ng-deep .mdi-vector-line:before {\n        content: \"\\F55E\"; }\n      :host ::ng-deep .mdi-vector-point:before {\n        content: \"\\F55F\"; }\n      :host ::ng-deep .mdi-vector-polygon:before {\n        content: \"\\F560\"; }\n      :host ::ng-deep .mdi-vector-polyline:before {\n        content: \"\\F561\"; }\n      :host ::ng-deep .mdi-vector-rectangle:before {\n        content: \"\\F5C6\"; }\n      :host ::ng-deep .mdi-vector-selection:before {\n        content: \"\\F562\"; }\n      :host ::ng-deep .mdi-vector-square:before {\n        content: \"\\F001\"; }\n      :host ::ng-deep .mdi-vector-triangle:before {\n        content: \"\\F563\"; }\n      :host ::ng-deep .mdi-vector-union:before {\n        content: \"\\F564\"; }\n      :host ::ng-deep .mdi-verified:before {\n        content: \"\\F565\"; }\n      :host ::ng-deep .mdi-vibrate:before {\n        content: \"\\F566\"; }\n      :host ::ng-deep .mdi-video:before {\n        content: \"\\F567\"; }\n      :host ::ng-deep .mdi-video-off:before {\n        content: \"\\F568\"; }\n      :host ::ng-deep .mdi-video-switch:before {\n        content: \"\\F569\"; }\n      :host ::ng-deep .mdi-view-agenda:before {\n        content: \"\\F56A\"; }\n      :host ::ng-deep .mdi-view-array:before {\n        content: \"\\F56B\"; }\n      :host ::ng-deep .mdi-view-carousel:before {\n        content: \"\\F56C\"; }\n      :host ::ng-deep .mdi-view-column:before {\n        content: \"\\F56D\"; }\n      :host ::ng-deep .mdi-view-dashboard:before {\n        content: \"\\F56E\"; }\n      :host ::ng-deep .mdi-view-day:before {\n        content: \"\\F56F\"; }\n      :host ::ng-deep .mdi-view-grid:before {\n        content: \"\\F570\"; }\n      :host ::ng-deep .mdi-view-headline:before {\n        content: \"\\F571\"; }\n      :host ::ng-deep .mdi-view-list:before {\n        content: \"\\F572\"; }\n      :host ::ng-deep .mdi-view-module:before {\n        content: \"\\F573\"; }\n      :host ::ng-deep .mdi-view-parallel:before {\n        content: \"\\F727\"; }\n      :host ::ng-deep .mdi-view-quilt:before {\n        content: \"\\F574\"; }\n      :host ::ng-deep .mdi-view-sequential:before {\n        content: \"\\F728\"; }\n      :host ::ng-deep .mdi-view-stream:before {\n        content: \"\\F575\"; }\n      :host ::ng-deep .mdi-view-week:before {\n        content: \"\\F576\"; }\n      :host ::ng-deep .mdi-vimeo:before {\n        content: \"\\F577\"; }\n      :host ::ng-deep .mdi-vine:before {\n        content: \"\\F578\"; }\n      :host ::ng-deep .mdi-violin:before {\n        content: \"\\F60F\"; }\n      :host ::ng-deep .mdi-visualstudio:before {\n        content: \"\\F610\"; }\n      :host ::ng-deep .mdi-vk:before {\n        content: \"\\F579\"; }\n      :host ::ng-deep .mdi-vk-box:before {\n        content: \"\\F57A\"; }\n      :host ::ng-deep .mdi-vk-circle:before {\n        content: \"\\F57B\"; }\n      :host ::ng-deep .mdi-vlc:before {\n        content: \"\\F57C\"; }\n      :host ::ng-deep .mdi-voice:before {\n        content: \"\\F5CB\"; }\n      :host ::ng-deep .mdi-voicemail:before {\n        content: \"\\F57D\"; }\n      :host ::ng-deep .mdi-volume-high:before {\n        content: \"\\F57E\"; }\n      :host ::ng-deep .mdi-volume-low:before {\n        content: \"\\F57F\"; }\n      :host ::ng-deep .mdi-volume-medium:before {\n        content: \"\\F580\"; }\n      :host ::ng-deep .mdi-volume-off:before {\n        content: \"\\F581\"; }\n      :host ::ng-deep .mdi-vpn:before {\n        content: \"\\F582\"; }\n      :host ::ng-deep .mdi-walk:before {\n        content: \"\\F583\"; }\n      :host ::ng-deep .mdi-wallet:before {\n        content: \"\\F584\"; }\n      :host ::ng-deep .mdi-wallet-giftcard:before {\n        content: \"\\F585\"; }\n      :host ::ng-deep .mdi-wallet-membership:before {\n        content: \"\\F586\"; }\n      :host ::ng-deep .mdi-wallet-travel:before {\n        content: \"\\F587\"; }\n      :host ::ng-deep .mdi-wan:before {\n        content: \"\\F588\"; }\n      :host ::ng-deep .mdi-washing-machine:before {\n        content: \"\\F729\"; }\n      :host ::ng-deep .mdi-watch:before {\n        content: \"\\F589\"; }\n      :host ::ng-deep .mdi-watch-export:before {\n        content: \"\\F58A\"; }\n      :host ::ng-deep .mdi-watch-import:before {\n        content: \"\\F58B\"; }\n      :host ::ng-deep .mdi-watch-vibrate:before {\n        content: \"\\F6B0\"; }\n      :host ::ng-deep .mdi-water:before {\n        content: \"\\F58C\"; }\n      :host ::ng-deep .mdi-water-off:before {\n        content: \"\\F58D\"; }\n      :host ::ng-deep .mdi-water-percent:before {\n        content: \"\\F58E\"; }\n      :host ::ng-deep .mdi-water-pump:before {\n        content: \"\\F58F\"; }\n      :host ::ng-deep .mdi-watermark:before {\n        content: \"\\F612\"; }\n      :host ::ng-deep .mdi-weather-cloudy:before {\n        content: \"\\F590\"; }\n      :host ::ng-deep .mdi-weather-fog:before {\n        content: \"\\F591\"; }\n      :host ::ng-deep .mdi-weather-hail:before {\n        content: \"\\F592\"; }\n      :host ::ng-deep .mdi-weather-lightning:before {\n        content: \"\\F593\"; }\n      :host ::ng-deep .mdi-weather-lightning-rainy:before {\n        content: \"\\F67D\"; }\n      :host ::ng-deep .mdi-weather-night:before {\n        content: \"\\F594\"; }\n      :host ::ng-deep .mdi-weather-partlycloudy:before {\n        content: \"\\F595\"; }\n      :host ::ng-deep .mdi-weather-pouring:before {\n        content: \"\\F596\"; }\n      :host ::ng-deep .mdi-weather-rainy:before {\n        content: \"\\F597\"; }\n      :host ::ng-deep .mdi-weather-snowy:before {\n        content: \"\\F598\"; }\n      :host ::ng-deep .mdi-weather-snowy-rainy:before {\n        content: \"\\F67E\"; }\n      :host ::ng-deep .mdi-weather-sunny:before {\n        content: \"\\F599\"; }\n      :host ::ng-deep .mdi-weather-sunset:before {\n        content: \"\\F59A\"; }\n      :host ::ng-deep .mdi-weather-sunset-down:before {\n        content: \"\\F59B\"; }\n      :host ::ng-deep .mdi-weather-sunset-up:before {\n        content: \"\\F59C\"; }\n      :host ::ng-deep .mdi-weather-windy:before {\n        content: \"\\F59D\"; }\n      :host ::ng-deep .mdi-weather-windy-variant:before {\n        content: \"\\F59E\"; }\n      :host ::ng-deep .mdi-web:before {\n        content: \"\\F59F\"; }\n      :host ::ng-deep .mdi-webcam:before {\n        content: \"\\F5A0\"; }\n      :host ::ng-deep .mdi-webhook:before {\n        content: \"\\F62F\"; }\n      :host ::ng-deep .mdi-webpack:before {\n        content: \"\\F72A\"; }\n      :host ::ng-deep .mdi-wechat:before {\n        content: \"\\F611\"; }\n      :host ::ng-deep .mdi-weight:before {\n        content: \"\\F5A1\"; }\n      :host ::ng-deep .mdi-weight-kilogram:before {\n        content: \"\\F5A2\"; }\n      :host ::ng-deep .mdi-whatsapp:before {\n        content: \"\\F5A3\"; }\n      :host ::ng-deep .mdi-wheelchair-accessibility:before {\n        content: \"\\F5A4\"; }\n      :host ::ng-deep .mdi-white-balance-auto:before {\n        content: \"\\F5A5\"; }\n      :host ::ng-deep .mdi-white-balance-incandescent:before {\n        content: \"\\F5A6\"; }\n      :host ::ng-deep .mdi-white-balance-iridescent:before {\n        content: \"\\F5A7\"; }\n      :host ::ng-deep .mdi-white-balance-sunny:before {\n        content: \"\\F5A8\"; }\n      :host ::ng-deep .mdi-widgets:before {\n        content: \"\\F72B\"; }\n      :host ::ng-deep .mdi-wifi:before {\n        content: \"\\F5A9\"; }\n      :host ::ng-deep .mdi-wifi-off:before {\n        content: \"\\F5AA\"; }\n      :host ::ng-deep .mdi-wii:before {\n        content: \"\\F5AB\"; }\n      :host ::ng-deep .mdi-wiiu:before {\n        content: \"\\F72C\"; }\n      :host ::ng-deep .mdi-wikipedia:before {\n        content: \"\\F5AC\"; }\n      :host ::ng-deep .mdi-window-close:before {\n        content: \"\\F5AD\"; }\n      :host ::ng-deep .mdi-window-closed:before {\n        content: \"\\F5AE\"; }\n      :host ::ng-deep .mdi-window-maximize:before {\n        content: \"\\F5AF\"; }\n      :host ::ng-deep .mdi-window-minimize:before {\n        content: \"\\F5B0\"; }\n      :host ::ng-deep .mdi-window-open:before {\n        content: \"\\F5B1\"; }\n      :host ::ng-deep .mdi-window-restore:before {\n        content: \"\\F5B2\"; }\n      :host ::ng-deep .mdi-windows:before {\n        content: \"\\F5B3\"; }\n      :host ::ng-deep .mdi-wordpress:before {\n        content: \"\\F5B4\"; }\n      :host ::ng-deep .mdi-worker:before {\n        content: \"\\F5B5\"; }\n      :host ::ng-deep .mdi-wrap:before {\n        content: \"\\F5B6\"; }\n      :host ::ng-deep .mdi-wrench:before {\n        content: \"\\F5B7\"; }\n      :host ::ng-deep .mdi-wunderlist:before {\n        content: \"\\F5B8\"; }\n      :host ::ng-deep .mdi-xaml:before {\n        content: \"\\F673\"; }\n      :host ::ng-deep .mdi-xbox:before {\n        content: \"\\F5B9\"; }\n      :host ::ng-deep .mdi-xbox-controller:before {\n        content: \"\\F5BA\"; }\n      :host ::ng-deep .mdi-xbox-controller-off:before {\n        content: \"\\F5BB\"; }\n      :host ::ng-deep .mdi-xda:before {\n        content: \"\\F5BC\"; }\n      :host ::ng-deep .mdi-xing:before {\n        content: \"\\F5BD\"; }\n      :host ::ng-deep .mdi-xing-box:before {\n        content: \"\\F5BE\"; }\n      :host ::ng-deep .mdi-xing-circle:before {\n        content: \"\\F5BF\"; }\n      :host ::ng-deep .mdi-xml:before {\n        content: \"\\F5C0\"; }\n      :host ::ng-deep .mdi-yeast:before {\n        content: \"\\F5C1\"; }\n      :host ::ng-deep .mdi-yelp:before {\n        content: \"\\F5C2\"; }\n      :host ::ng-deep .mdi-yin-yang:before {\n        content: \"\\F67F\"; }\n      :host ::ng-deep .mdi-youtube-play:before {\n        content: \"\\F5C3\"; }\n      :host ::ng-deep .mdi-zip-box:before {\n        content: \"\\F5C4\"; }\n      :host ::ng-deep .mdi-18px.mdi-set, :host ::ng-deep .mdi-18px.mdi:before {\n        font-size: 18px; }\n      :host ::ng-deep .mdi-24px.mdi-set, :host ::ng-deep .mdi-24px.mdi:before {\n        font-size: 24px; }\n      :host ::ng-deep .mdi-36px.mdi-set, :host ::ng-deep .mdi-36px.mdi:before {\n        font-size: 36px; }\n      :host ::ng-deep .mdi-48px.mdi-set, :host ::ng-deep .mdi-48px.mdi:before {\n        font-size: 48px; }\n      :host ::ng-deep .mdi-dark {\n        color: rgba(0, 0, 0, 0.54); }\n      :host ::ng-deep .mdi-dark.mdi-inactive {\n        color: rgba(0, 0, 0, 0.26); }\n      :host ::ng-deep .mdi-light {\n        color: white; }\n      :host ::ng-deep .mdi-light.mdi-inactive {\n        color: rgba(255, 255, 255, 0.3); }\n      :host ::ng-deep .inputs-wrapper {\n        margin-bottom: 30px; }\n      :host ::ng-deep .input-holder {\n        width: 100%;\n        position: relative;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        min-height: 54px;\n        margin-bottom: 11px;\n        -webkit-transition: border-color 0.15s ease-out 0s;\n        transition: border-color 0.15s ease-out 0s; }\n        @media screen and (max-width: 640px) {\n          :host ::ng-deep .input-holder {\n            min-height: 40px; } }\n        :host ::ng-deep .input-holder:last-child {\n          margin-bottom: 0; }\n        :host ::ng-deep .input-holder input ~ label {\n          position: absolute;\n          bottom: 15px;\n          left: 0;\n          padding-left: 20px;\n          -webkit-transform: translateY(0);\n                  transform: translateY(0);\n          -webkit-transform-origin: 0;\n                  transform-origin: 0;\n          font-size: 16px;\n          color: #64686a;\n          pointer-events: none;\n          -webkit-transition: top .15s, font-size .15s, -webkit-transform .15s;\n          transition: top .15s, font-size .15s, -webkit-transform .15s;\n          transition: top .15s, font-size .15s, transform .15s;\n          transition: top .15s, font-size .15s, transform .15s, -webkit-transform .15s; }\n          @media screen and (max-width: 640px) {\n            :host ::ng-deep .input-holder input ~ label {\n              bottom: 12px;\n              font-size: 14px;\n              padding-left: 11px; } }\n        :host ::ng-deep .input-holder input[readonly] {\n          border-color: #30C4F3;\n          pointer-events: none; }\n          :host ::ng-deep .input-holder input[readonly] ~ label {\n            font-size: .55rem;\n            -webkit-transform: translateY(-22px);\n                    transform: translateY(-22px); }\n            @media screen and (max-width: 640px) {\n              :host ::ng-deep .input-holder input[readonly] ~ label {\n                -webkit-transform: translateY(-17px);\n                        transform: translateY(-17px);\n                font-size: 10px; } }\n        :host ::ng-deep .input-holder .input-state {\n          display: block;\n          font-size: 24px; }\n        :host ::ng-deep .input-holder input {\n          display: block;\n          width: 100%;\n          height: 54px;\n          border-radius: 5px;\n          padding: 20px 36px 15px 20px;\n          background: transparent;\n          color: #000;\n          font-size: 16px;\n          overflow: hidden;\n          white-space: nowrap;\n          text-overflow: ellipsis;\n          -webkit-box-shadow: none;\n                  box-shadow: none;\n          border: 1px solid #d5d5d5;\n          -webkit-transition: border .12s ease-in-out;\n          transition: border .12s ease-in-out; }\n          @media screen and (max-width: 640px) {\n            :host ::ng-deep .input-holder input {\n              height: 44px;\n              padding: 14px 18px 10px 10px;\n              font-size: 14px; } }\n          :host ::ng-deep .input-holder input:focus, :host ::ng-deep .input-holder input:valid {\n            border-color: #30C4F3; }\n          :host ::ng-deep .input-holder input.ng-dirty.ng-invalid {\n            border-color: #fc5d2b; }\n            :host ::ng-deep .input-holder input.ng-dirty.ng-invalid ~ .input-state:before {\n              content: \"\\F026\";\n              color: #fc5d2b; }\n          :host ::ng-deep .input-holder input.ng-dirty.ng-valid ~ .input-state:before {\n            content: \"\\F12C\";\n            color: #bfc4c8; }\n        :host ::ng-deep .input-holder.focus-field input, :host ::ng-deep .input-holder.valid-field input {\n          border-color: #30c4f3; }\n        :host ::ng-deep .input-holder.error-input input {\n          border-color: #fc3e6b; }\n        :host ::ng-deep .input-holder .required-marker {\n          position: absolute;\n          top: -3px;\n          right: -13px; }\n          :host ::ng-deep .input-holder .required-marker:before {\n            content: '*';\n            font-size: 1rem;\n            color: #fc3e6b; }\n        :host ::ng-deep .input-holder .mdi {\n          position: absolute;\n          top: 50%;\n          -webkit-transform: translateY(-50%);\n                  transform: translateY(-50%);\n          right: 20px;\n          font-size: 24px; }\n          :host ::ng-deep .input-holder .mdi-alert {\n            color: #fc3e6b; }\n          :host ::ng-deep .input-holder .mdi-check {\n            color: #1daafc; }\n        :host ::ng-deep .input-holder input:focus ~ label, :host ::ng-deep .input-holder input:valid ~ label {\n          font-size: .55rem;\n          -webkit-transform: translateY(-22px);\n                  transform: translateY(-22px); }\n          @media screen and (max-width: 640px) {\n            :host ::ng-deep .input-holder input:focus ~ label, :host ::ng-deep .input-holder input:valid ~ label {\n              -webkit-transform: translateY(-17px);\n                      transform: translateY(-17px);\n              font-size: 10px; } }\n        :host ::ng-deep .input-holder.label-top label {\n          font-size: .55rem !important;\n          -webkit-transform: translateY(-22px);\n                  transform: translateY(-22px); }\n          @media screen and (max-width: 640px) {\n            :host ::ng-deep .input-holder.label-top label {\n              -webkit-transform: translateY(-17px);\n                      transform: translateY(-17px);\n              font-size: 10px; } }\n      :host ::ng-deep button {\n        overflow: visible; }\n      :host ::ng-deep .button, :host ::ng-deep button {\n        border: none;\n        padding: 8px 24px;\n        border-radius: 30px;\n        text-transform: uppercase;\n        line-height: 36px;\n        font-size: .8rem;\n        color: #fff;\n        background: #1daafc;\n        -webkit-box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0.5);\n                box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0.5);\n        outline: none;\n        font-weight: 600;\n        cursor: pointer;\n        -webkit-transition: background .15s, color .15s, border .15s, -webkit-box-shadow .15s;\n        transition: background .15s, color .15s, border .15s, -webkit-box-shadow .15s;\n        transition: background .15s, box-shadow .15s, color .15s, border .15s;\n        transition: background .15s, box-shadow .15s, color .15s, border .15s, -webkit-box-shadow .15s; }\n        @media screen and (max-width: 640px) {\n          :host ::ng-deep .button, :host ::ng-deep button {\n            line-height: 24px;\n            font-size: 12px; } }\n        :host ::ng-deep .button:hover, :host ::ng-deep button:hover {\n          background: #39b6ff; }\n        :host ::ng-deep .button:before, :host ::ng-deep button:before {\n          -webkit-transition: color .15s;\n          transition: color .15s; }\n        :host ::ng-deep .button[disabled], :host ::ng-deep button[disabled] {\n          background: #eee;\n          color: #a5a8a9;\n          pointer-events: none;\n          -webkit-box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0);\n                  box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0); }\n        :host ::ng-deep .button:active, :host ::ng-deep button:active {\n          background: #108fd9;\n          -webkit-box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0);\n                  box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0); }\n      :host ::ng-deep .mdi:before,\n      :host ::ng-deep .mdi-set {\n        display: inline-block;\n        font: normal normal normal 24px/1 \"Material Design Icons\";\n        font-size: inherit;\n        text-rendering: auto;\n        line-height: inherit;\n        -webkit-font-smoothing: antialiased;\n        -moz-osx-font-smoothing: grayscale;\n        -webkit-transform: translate(0, 0);\n                transform: translate(0, 0); }\n      :host ::ng-deep .card-form-holder {\n        position: relative;\n        width: 376px;\n        margin: 0 auto;\n        text-align: center; }\n        @media screen and (max-width: 640px) {\n          :host ::ng-deep .card-form-holder {\n            width: 300px;\n            padding-top: 18px; } }\n        :host ::ng-deep .card-form-holder .title {\n          font-size: 14px;\n          margin-bottom: 25px;\n          color: #64686a;\n          text-align: center; }\n        :host ::ng-deep .card-form-holder .mdi-close {\n          display: inline-block;\n          width: 20px;\n          height: 20px;\n          position: absolute;\n          top: 16px;\n          right: 16px;\n          font-size: 0;\n          cursor: pointer; }\n          :host ::ng-deep .card-form-holder .mdi-close:hover:before {\n            color: #a4abb0; }\n          :host ::ng-deep .card-form-holder .mdi-close:before {\n            font-size: 14px;\n            color: #bfc4c8;\n            -webkit-transition: color .12s ease;\n            transition: color .12s ease; }\n        :host ::ng-deep .card-form-holder .inputs-wrapper {\n          padding: 0 15px; }\n          @media screen and (max-width: 640px) {\n            :host ::ng-deep .card-form-holder .inputs-wrapper {\n              padding: 0 30px; } }\n        :host ::ng-deep .card-form-holder .description {\n          max-width: 250px;\n          margin: 0 auto 35px;\n          font-size: 12px;\n          color: #b2b4b6; }\n          @media screen and (max-width: 640px) {\n            :host ::ng-deep .card-form-holder .description {\n              margin-bottom: 20px; } }\n        :host ::ng-deep .card-form-holder .button-holder {\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n              -ms-flex-align: center;\n                  align-items: center;\n          -webkit-box-pack: end;\n              -ms-flex-pack: end;\n                  justify-content: flex-end;\n          height: 113px;\n          padding: 0 32px;\n          border-top: 1px solid #dadee1; }\n          @media screen and (max-width: 640px) {\n            :host ::ng-deep .card-form-holder .button-holder {\n              height: 70px; } }\n          :host ::ng-deep .card-form-holder .button-holder .button + button {\n            margin-left: 10px; }\n          :host ::ng-deep .card-form-holder .button-holder .button.cancel-button {\n            background-color: transparent;\n            color: #1daafc;\n            -webkit-box-shadow: 0 3px 6px transparent;\n                    box-shadow: 0 3px 6px transparent; }\n            :host ::ng-deep .card-form-holder .button-holder .button.cancel-button:hover {\n              -webkit-box-shadow: 0 3px 6px rgba(29, 170, 252, 0.6);\n                      box-shadow: 0 3px 6px rgba(29, 170, 252, 0.6);\n              color: #fff;\n              background-color: #1daafc; }\n      :host ::ng-deep .single-card .card {\n        max-width: 150px;\n        margin: 0 auto 25px;\n        border-radius: 0 0 8px 8px;\n        cursor: default; }\n        :host ::ng-deep .single-card .card .card-holder {\n          height: 100px; }\n          :host ::ng-deep .single-card .card .card-holder .card-actions {\n            opacity: 0;\n            pointer-events: none; }\n      :host ::ng-deep .single-card .card-name {\n        display: none; }\n      :host ::ng-deep app-card {\n        display: block;\n        height: 100%; }\n      :host ::ng-deep .card {\n        position: relative;\n        width: 172px;\n        height: 100%;\n        min-height: 154px;\n        text-align: center;\n        font-size: 16px;\n        color: #64686a;\n        background-color: #f5f7f8;\n        border-radius: 0 0 8px 8px;\n        cursor: pointer;\n        float: none; }\n        :host ::ng-deep .card .card-holder {\n          height: 112px;\n          border-radius: 8px;\n          border-bottom: 2px solid rgba(0, 0, 0, 0.2);\n          -webkit-transition: -webkit-box-shadow .12s ease;\n          transition: -webkit-box-shadow .12s ease;\n          transition: box-shadow .12s ease;\n          transition: box-shadow .12s ease, -webkit-box-shadow .12s ease;\n          -webkit-box-shadow: 0 19px 19px -14px rgba(251, 99, 125, 0);\n                  box-shadow: 0 19px 19px -14px rgba(251, 99, 125, 0); }\n          :host ::ng-deep .card .card-holder:hover .card-info {\n            opacity: 0; }\n          :host ::ng-deep .card .card-holder:hover .card-actions {\n            opacity: 1; }\n          :host ::ng-deep .card .card-holder.visa {\n            background: linear-gradient(45deg, #4a84fe 0%, #91b3fe 100%); }\n            :host ::ng-deep .card .card-holder.visa .card-info .card-logo {\n              background-image: url(\"assets/card_visa.svg\"); }\n          :host ::ng-deep .card .card-holder.mc, :host ::ng-deep .card .card-holder.mast {\n            background: linear-gradient(45deg, #fb637d 0%, #feb04f 100%); }\n            :host ::ng-deep .card .card-holder.mc .card-info .card-logo, :host ::ng-deep .card .card-holder.mast .card-info .card-logo {\n              background-image: url(\"assets/card_mc.svg\"); }\n          :host ::ng-deep .card .card-holder.add-card {\n            min-height: 154px;\n            height: 100%;\n            display: -webkit-box;\n            display: -ms-flexbox;\n            display: flex;\n            -webkit-box-orient: vertical;\n            -webkit-box-direction: normal;\n                -ms-flex-direction: column;\n                    flex-direction: column;\n            -webkit-box-align: center;\n                -ms-flex-align: center;\n                    align-items: center;\n            -webkit-box-pack: center;\n                -ms-flex-pack: center;\n                    justify-content: center;\n            background-color: #f5f7f8;\n            cursor: pointer;\n            text-align: center;\n            border: none;\n            color: #868a8c; }\n            :host ::ng-deep .card .card-holder.add-card:hover .add-card-button:before {\n              color: #0391e3; }\n            :host ::ng-deep .card .card-holder.add-card .add-card-button {\n              display: block;\n              margin-bottom: 10px; }\n              :host ::ng-deep .card .card-holder.add-card .add-card-button:before {\n                font-size: 40px;\n                line-height: 40px;\n                display: block;\n                color: #1daafc;\n                -webkit-transition: color .12s ease-in-out;\n                transition: color .12s ease-in-out; }\n          :host ::ng-deep .card .card-holder .add-card-text {\n            text-align: center;\n            font-size: 1rem; }\n          :host ::ng-deep .card .card-holder .card-info {\n            padding: 19px 0 0 16px;\n            -webkit-transition: opacity .12s ease-in-out;\n            transition: opacity .12s ease-in-out; }\n            :host ::ng-deep .card .card-holder .card-info .card-logo {\n              width: 50px;\n              height: 24px;\n              margin-bottom: 13px;\n              background-size: 25px;\n              background-repeat: no-repeat; }\n            :host ::ng-deep .card .card-holder .card-info .card-pan {\n              display: block;\n              width: 100%;\n              color: #ffffff;\n              text-align: left;\n              font-size: 13px; }\n          :host ::ng-deep .card .card-holder .card-actions {\n            width: 100%;\n            height: 112px;\n            cursor: pointer;\n            display: table;\n            color: #ffffff;\n            position: absolute;\n            left: 50%;\n            top: 0;\n            -webkit-transform: translate(-50%, 0);\n                    transform: translate(-50%, 0);\n            opacity: 0;\n            -webkit-transition: opacity .12s ease-in-out;\n            transition: opacity .12s ease-in-out; }\n            :host ::ng-deep .card .card-holder .card-actions > * {\n              height: 100%;\n              width: 50%;\n              margin: 0;\n              padding: 0;\n              display: table-cell;\n              overflow: hidden; }\n              :host ::ng-deep .card .card-holder .card-actions > *:nth-child(1) {\n                border-right: 1px solid rgba(0, 0, 0, 0.1); }\n              :host ::ng-deep .card .card-holder .card-actions > *:nth-child(2) {\n                border-left: 1px solid rgba(0, 0, 0, 0.1); }\n              :host ::ng-deep .card .card-holder .card-actions > * .card-action-icon-holder {\n                font-size: 32px;\n                height: 100%;\n                display: -webkit-box;\n                display: -ms-flexbox;\n                display: flex;\n                -webkit-box-pack: center;\n                    -ms-flex-pack: center;\n                        justify-content: center;\n                -webkit-box-align: center;\n                    -ms-flex-align: center;\n                        align-items: center; }\n        :host ::ng-deep .card .card-name {\n          margin: 0;\n          padding: 12px 20px 12px 16px;\n          overflow: visible;\n          text-overflow: initial;\n          white-space: normal;\n          text-align: left;\n          font-size: 11px;\n          word-wrap: break-word; }\n      :host ::ng-deep .card-block {\n        margin: 0 6px 15px; }\n      :host ::ng-deep .cards::-webkit-scrollbar {\n        width: 4px;\n        background-color: transparent; }\n      :host ::ng-deep .cards::-webkit-scrollbar-thumb {\n        background-color: #aaa;\n        width: 4px;\n        border-radius: 6px; }\n      :host ::ng-deep app-select-card {\n        display: block;\n        padding-right: 4px;\n        padding-bottom: 4px; }\n      :host ::ng-deep .cards {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-orient: horizontal;\n        -webkit-box-direction: normal;\n            -ms-flex-flow: row wrap;\n                flex-flow: row wrap;\n        -ms-flex-line-pack: start;\n            align-content: flex-start;\n        width: 800px;\n        max-height: 502px;\n        overflow: auto;\n        padding: 0 30px; }\n        @media screen and (max-width: 640px) {\n          :host ::ng-deep .cards {\n            width: 405px;\n            max-height: 400px;\n            padding: 0 15px 20px; } }\n        @media screen and (max-width: 1024px) {\n          :host ::ng-deep .cards {\n            width: 435px; } }\n  "]
            },] },
];
/**
 * @nocollapse
 */
MosstCardComponent.ctorParameters = function () { return [
    { type: LocaleService, },
    { type: ApiService, },
]; };
MosstCardComponent.propDecorators = {
    'user': [{ type: core.Input },],
    'lang': [{ type: core.Input },],
    'onCardSelect': [{ type: core.Output },],
    'onAddCard': [{ type: core.Output },],
    'onCardDelete': [{ type: core.Output },],
};
var MaskWorker = (function () {
    /**
     * @param {?=} mask
     * @param {?=} output
     * @param {?=} allowedInputs
     * @param {?=} advancedSetter
     * @param {?=} replacement
     * @param {?=} showConstants
     */
    function MaskWorker(mask, output, allowedInputs, advancedSetter, replacement, showConstants) {
        if (mask === void 0) { mask = function (val) { return val; }; }
        if (output === void 0) { output = function (val) { return val; }; }
        if (allowedInputs === void 0) { allowedInputs = '.*'; }
        if (advancedSetter === void 0) { advancedSetter = null; }
        if (replacement === void 0) { replacement = 'X'; }
        if (showConstants === void 0) { showConstants = false; }
        this.mask = mask;
        this.output = output;
        this.allowedInputs = allowedInputs;
        this.advancedSetter = advancedSetter;
        this.replacement = replacement;
        this.showConstants = showConstants;
        this.allowedSymbols = new RegExp(allowedInputs);
        this.constantPositions = this.getConstantPositions(mask);
    }
    /**
     * @param {?=} inputString
     * @param {?=} ignoreReplacement
     * @return {?}
     */
    MaskWorker.prototype.removeNotAllowedSymbolsFromString = function (inputString, ignoreReplacement) {
        if (inputString === void 0) { inputString = ''; }
        if (ignoreReplacement === void 0) { ignoreReplacement = false; }
        var /** @type {?} */ stringWithoutNotAllowedSymbols = '';
        for (var /** @type {?} */ i = 0; i < inputString.length; i++) {
            if (this.allowedSymbols.test(inputString[i]) || (ignoreReplacement && inputString[i] === this.replacement)) {
                stringWithoutNotAllowedSymbols += inputString[i];
            }
        }
        return stringWithoutNotAllowedSymbols;
    };
    /**
     * @param {?} string
     * @return {?}
     */
    MaskWorker.prototype.restoreToTextOutput = function (string) {
        var /** @type {?} */ meaningPart = this.getMeaningPart(string);
        var /** @type {?} */ result = this.output;
        for (var /** @type {?} */ i = 0; i < meaningPart.length; i++) {
            result = result.replace(/X/, meaningPart[i]);
        }
        result = result.replace(/X/g, '');
        return result;
    };
    /**
     * @param {?} string
     * @return {?}
     */
    MaskWorker.prototype.getMeaningPart = function (string) {
        var /** @type {?} */ meaningPart = '';
        if (string.length > this.mask.length) {
            string = string.slice(0, this.mask.length);
        }
        for (var /** @type {?} */ i = 0; i < string.length; i++) {
            if (this.mask[i] === 'X') {
                meaningPart += string[i];
            }
        }
        return meaningPart;
    };
    /**
     * @param {?} str
     * @return {?}
     */
    MaskWorker.prototype.removePredefinedOutput = function (str) {
        var /** @type {?} */ getFirstMatchTail = function (_str, symbol) {
            for (var /** @type {?} */ i = 0; i < _str.length; i++) {
                if (_str[i] === symbol) {
                    return _str.slice(i);
                }
            }
            return '';
        };
        var /** @type {?} */ equalPartsLength = function (string1, string2) {
            var /** @type {?} */ length = 0;
            for (var /** @type {?} */ i = 0; i < string1.length; i++) {
                if (string1[i] === string2[i]) {
                    length++;
                }
                else {
                    return length;
                }
            }
            return length;
        };
        var /** @type {?} */ outputTemplate = this.removeNotAllowedSymbolsFromString(this.output);
        var /** @type {?} */ outputTail = getFirstMatchTail(outputTemplate, str[0]);
        return str.slice(equalPartsLength(outputTail, str));
    };
    /**
     * @param {?} string
     * @return {?}
     */
    MaskWorker.prototype.cutTail = function (string) {
        var _this = this;
        var /** @type {?} */ getPreviousReplacerIndex = function (index) {
            if (!index) {
                return 0;
            }
            for (var /** @type {?} */ i = index - 1; i > 0; i--) {
                if (_this.mask[i] === _this.replacement) {
                    return i + 1;
                }
            }
            return index;
        };
        var /** @type {?} */ re = new RegExp('X', 'g');
        var /** @type {?} */ findReplacer = re.exec(string);
        return findReplacer ? string.slice(0, getPreviousReplacerIndex(findReplacer.index)) : string;
    };
    /**
     * @param {?} string
     * @return {?}
     */
    MaskWorker.prototype.useTextMask = function (string) {
        var /** @type {?} */ allowedLength = this.mask.length - this.mask.replace(/X/g, '').length;
        var /** @type {?} */ stringWithoutNotAllowedSymbols = this.removeNotAllowedSymbolsFromString(string);
        string = this.removePredefinedOutput(stringWithoutNotAllowedSymbols).slice(0, allowedLength);
        var /** @type {?} */ currentString = this.mask;
        for (var /** @type {?} */ i = 0; i < allowedLength; i++) {
            if (i < string.length) {
                currentString = currentString.replace(/X/, string[i]);
            }
            else if (this.showConstants) {
                currentString = currentString.replace(/X/, '');
            }
        }
        currentString = this.cutTail(currentString);
        return currentString;
    };
    /**
     * @param {?} string
     * @return {?}
     */
    MaskWorker.prototype.applyAdvancedSetter = function (string) {
        if (typeof this.advancedSetter === 'function') {
            return this.advancedSetter(string);
        }
        if (this.advancedSetter instanceof RegExp) {
            return string.match(this.advancedSetter) ? string : '';
        }
        return string;
    };
    /**
     * @param {?=} string
     * @return {?}
     */
    MaskWorker.prototype.useMask = function (string) {
        if (string === void 0) { string = ''; }
        string = string.toString();
        if (typeof this.mask === 'function') {
            string = this.removeNotAllowedSymbolsFromString(string);
            return this.mask(string);
        }
        return this.useTextMask(this.applyAdvancedSetter(string));
    };
    /**
     * @param {?=} string
     * @return {?}
     */
    MaskWorker.prototype.unmask = function (string) {
        if (string === void 0) { string = ''; }
        if (typeof this.output === 'function') {
            return this.output(string);
        }
        return this.restoreToTextOutput(string);
    };
    /**
     * @param {?} str
     * @return {?}
     */
    MaskWorker.prototype.getConstantPositions = function (str) {
        if (typeof str === 'function') {
            return [];
        }
        var /** @type {?} */ constantPositions = [];
        for (var /** @type {?} */ i = 0; i < str.length; i++) {
            if (str[i] !== 'X') {
                constantPositions.push(i + 1);
            }
        }
        return constantPositions;
    };
    /**
     * @param {?} pos
     * @param {?=} deletion
     * @return {?}
     */
    MaskWorker.prototype.getCursorPositionWithOffset = function (pos, deletion) {
        if (deletion === void 0) { deletion = false; }
        var /** @type {?} */ res = deletion
            ? pos - this.getCursorOffsetRight(pos)
            : pos + this.getCursorOffsetRight(pos);
        var /** @type {?} */ startPos = this.getStartPosition();
        return res < startPos ? startPos : res;
    };
    /**
     * @return {?}
     */
    MaskWorker.prototype.getStartPosition = function () {
        if (!this.constantPositions.length || this.constantPositions[0] !== 1) {
            return 0;
        }
        var /** @type {?} */ i;
        for (i = 1; i < this.constantPositions.length; ++i) {
            if (this.constantPositions[i] - this.constantPositions[i - 1] !== 1) {
                break;
            }
        }
        return this.constantPositions[i - 1];
    };
    /**
     * @param {?} pos
     * @return {?}
     */
    MaskWorker.prototype.getCursorOffsetRight = function (pos) {
        var /** @type {?} */ constantMatchIndex = this.constantPositions.indexOf(pos);
        if (constantMatchIndex !== -1) {
            var /** @type {?} */ i = constantMatchIndex;
            while (i < this.constantPositions.length) {
                if (this.constantPositions[i + 1] - this.constantPositions[i] !== 1) {
                    break;
                }
                i++;
            }
            return i - constantMatchIndex + 1;
        }
        return 0;
    };
    /**
     * @param {?} pos
     * @return {?}
     */
    MaskWorker.prototype.getCursorOffsetLeft = function (pos) {
        var /** @type {?} */ constantMatchIndex = this.constantPositions.indexOf(pos);
        if (constantMatchIndex !== -1) {
            var /** @type {?} */ i = constantMatchIndex;
            while (i >= 0) {
                if (this.constantPositions[i] - this.constantPositions[i - 1] !== 1) {
                    break;
                }
                i--;
            }
            return constantMatchIndex - i + 1;
        }
        return 0;
    };
    return MaskWorker;
}());
/**
 * @param {?} mask
 * @return {?}
 */
function createMaskWorker(mask) {
    return new MaskWorker(mask.mask, mask.output, mask.allowedInputs, mask.advancedSetter, mask.replacement, mask.showConstants);
}
/**
 * @param {?} mask
 * @return {?}
 */
function format(mask) {
    return function (value) {
        var /** @type {?} */ maskWorker = createMaskWorker(mask);
        return maskWorker.useMask(value);
    };
}
var InputMaskDirective = (function () {
    /**
     * @param {?} model
     * @param {?} el
     */
    function InputMaskDirective(model, el) {
        var _this = this;
        this.model = model;
        this.el = el;
        this.valueChanges = new core.EventEmitter();
        this.previousValue = '';
        this.valueChanges.subscribe(function (val) {
            if (_this.formControl) {
                _this.formControl.setValue(val, {
                    onlySelf: true,
                    emitEvent: false,
                    emitModelToViewChange: false,
                    emitViewToModelChange: false
                });
            }
        });
    }
    /**
     * @return {?}
     */
    InputMaskDirective.prototype.ngOnInit = function () {
        if (!this.maskWorker) {
            if (this.mask) {
                this.initMaskWorker();
            }
        }
        if (this.formControl) {
            if (this.mask.maskedValue) {
                var /** @type {?} */ maskedValue = this.maskWorker.useMask(this.formControl.value || '');
                this.model.valueAccessor.writeValue(maskedValue);
                this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
            }
            else {
                this.formControl.setValue(this.maskWorker.useMask(this.formControl.value || ''));
            }
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InputMaskDirective.prototype.onKeyDown = function (event) {
        if (event.key === 'Delete' && this.customDelete(event)) {
            event.preventDefault();
            return;
        }
        if (event.key === 'Backspace' && this.customBackspace(event)) {
            event.preventDefault();
            return;
        }
        var /** @type {?} */ inputStart = this.maskWorker.getStartPosition();
        var /** @type {?} */ isHomeKey = event.key === 'Home';
        var /** @type {?} */ isArrowLeftKey = event.key === 'ArrowLeft';
        var _a = event.target, selectionStart = _a.selectionStart, selectionEnd = _a.selectionEnd, value = _a.value;
        if (!selectionStart && selectionEnd === value.length) {
            return;
        }
        if (selectionStart <= inputStart || isHomeKey) {
            this.el.nativeElement.selectionStart = inputStart;
            this.el.nativeElement.selectionEnd = inputStart;
            if (!isHomeKey && isArrowLeftKey) {
                event.preventDefault();
            }
        }
        if (isHomeKey) {
            event.preventDefault();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InputMaskDirective.prototype.onInputChange = function (event) {
        if (!this.maskWorker) {
            if (!this.mask) {
                return;
            }
            this.initMaskWorker();
        }
        var /** @type {?} */ start = this.el.nativeElement.selectionStart;
        var /** @type {?} */ maskedValue = this.maskWorker.useMask(event);
        this.model.valueAccessor.writeValue(maskedValue);
        var /** @type {?} */ deletion = this.previousValue.length >= maskedValue.length;
        this.el.nativeElement.selectionStart = this.maskWorker.getCursorPositionWithOffset(start, deletion);
        this.el.nativeElement.selectionEnd = this.maskWorker.getCursorPositionWithOffset(start, deletion);
        this.previousValue = maskedValue;
        this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
    };
    /**
     * @param {?} event
     * @param {?=} left
     * @return {?}
     */
    InputMaskDirective.prototype.customDelete = function (event, left) {
        if (left === void 0) { left = false; }
        var _a = event.target, selectionStart = _a.selectionStart, selectionEnd = _a.selectionEnd;
        var /** @type {?} */ inputStart = this.maskWorker.getStartPosition();
        var /** @type {?} */ value = '';
        var /** @type {?} */ movePos = selectionStart;
        if (selectionStart > 0 && selectionEnd <= event.target.value.length) {
            var /** @type {?} */ offset = left
                ? -this.maskWorker.getCursorOffsetLeft(selectionStart) - 1
                : this.maskWorker.getCursorOffsetRight(selectionStart + 1);
            var /** @type {?} */ pos = selectionStart + offset;
            if (pos < 0) {
                return true;
            }
            if (left && pos >= this.maskWorker.getStartPosition()) {
                movePos = pos;
            }
            var /** @type {?} */ arr = event.target.value.split('');
            arr.splice(pos, 1);
            value = arr.join('');
        }
        else {
            movePos = inputStart;
        }
        var /** @type {?} */ maskedValue = this.maskWorker.useMask(value);
        this.model.valueAccessor.writeValue(maskedValue);
        event.target.selectionStart = movePos;
        event.target.selectionEnd = movePos;
        this.previousValue = maskedValue;
        this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
        return true;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InputMaskDirective.prototype.customBackspace = function (event) {
        return this.customDelete(event, true);
    };
    /**
     * @return {?}
     */
    InputMaskDirective.prototype.initMaskWorker = function () {
        this.maskWorker = createMaskWorker(this.mask);
    };
    return InputMaskDirective;
}());
InputMaskDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[inputMask]',
                providers: [forms.NgModel],
                host: {
                    '(ngModelChange)': 'onInputChange($event)',
                    '(keydown)': 'onKeyDown($event)'
                }
            },] },
];
/**
 * @nocollapse
 */
InputMaskDirective.ctorParameters = function () { return [
    { type: forms.NgModel, },
    { type: core.ElementRef, },
]; };
InputMaskDirective.propDecorators = {
    'formControl': [{ type: core.Input },],
    'mask': [{ type: core.Input },],
    'valueChanges': [{ type: core.Output },],
};
var InputMaskModule = (function () {
    function InputMaskModule() {
    }
    return InputMaskModule;
}());
InputMaskModule.decorators = [
    { type: core.NgModule, args: [{
                declarations: [
                    InputMaskDirective
                ],
                exports: [
                    InputMaskDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
InputMaskModule.ctorParameters = function () { return []; };
var inputMasks = {
    pan: {
        mask: 'XXXX XXXX XXXX XXXX',
        output: 'XXXXXXXXXXXXXXXX',
        allowedInputs: '[0-9|\*]',
        advancedSetter: /^[456].*/g
    },
    exp: {
        mask: 'XX/XX',
        output: function (output) { return output; },
        allowedInputs: '[0-9]',
        maskedValue: true,
        showConstants: true
    }
};
var CCTypes = (function () {
    function CCTypes() {
    }
    /**
     * @param {?} cardName
     * @return {?}
     */
    CCTypes.isAccepted = function (cardName) {
        return cardName === this.AMERICAN_EXPRESS
            || cardName === this.DISCOVER
            || cardName === this.DINERS_CLUB_INTERNATIONAL
            || cardName === this.VISA
            || cardName === this.MASTERCARD;
    };
    return CCTypes;
}());
CCTypes.NONE = 'none';
CCTypes.AMERICAN_EXPRESS = 'amex';
CCTypes.DISCOVER = 'disc';
CCTypes.DINERS_CLUB_INTERNATIONAL = 'dine';
CCTypes.VISA = 'visa';
CCTypes.MASTERCARD = 'mast';
var CCValidator = (function () {
    function CCValidator() {
    }
    /**
     * @param {?} cardNumber
     * @return {?}
     */
    CCValidator.isValid = function (cardNumber) {
        var /** @type {?} */ testNumber = this.__preprocess(cardNumber);
        var /** @type {?} */ cardType = this.getCardType(testNumber);
        var /** @type {?} */ isMasked = /\*/.test(cardNumber);
        if (cardType !== CCTypes.NONE) {
            return this.__lengthValid(testNumber, cardType) && isMasked || this.__luhnValid(testNumber);
        }
        return false;
    };
    /**
     * Return the credit card type based on the card number (provided the card is in the accepted list of cards)
     *
     * @param {?} cardNumber : string - Credit card number, which may include spaces or dashes, i.e. XXXX-YYYY-ZZZZ-ABCD
     *
     * @return {?} string - Credit card (CCTypes member) type, which may be 'none' if the card number is not recognized
     */
    CCValidator.getCardType = function (cardNumber) {
        var /** @type {?} */ cardProps;
        var /** @type {?} */ theCard = this.__preprocess(cardNumber);
        for (var /** @type {?} */ key in this._types) {
            if (this._types.hasOwnProperty(key)) {
                cardProps = this._types[key];
                if (theCard.match(cardProps['pattern'])) {
                    return key;
                }
            }
        }
        return 'none';
    };
    /**
     * @param {?} cardNumber
     * @return {?}
     */
    CCValidator.__preprocess = function (cardNumber) {
        return cardNumber.replace(/[ -]/g, '');
    };
    /**
     * @param {?} cardNumber
     * @param {?} cardType
     * @return {?}
     */
    CCValidator.__lengthValid = function (cardNumber, cardType) {
        var /** @type {?} */ cardProps = this._types[cardType];
        return cardProps ? cardNumber.length === cardProps['length'] : false;
    };
    /**
     * @param {?} cardNumber
     * @return {?}
     */
    CCValidator.__luhnValid = function (cardNumber) {
        var /** @type {?} */ digit;
        var /** @type {?} */ n;
        var /** @type {?} */ sum;
        var /** @type {?} */ j;
        sum = 0;
        var /** @type {?} */ numbers = cardNumber.split('').reverse().map(function (val) { return parseFloat(val); });
        var /** @type {?} */ len = numbers.length;
        n = 0;
        j = 0;
        // there's really nothing new under the sun ...
        while (j < len) {
            digit = numbers[n];
            digit = +digit;
            if (n % 2) {
                digit *= 2;
                if (digit < 10) {
                    sum += digit;
                }
                else {
                    sum += digit - 9;
                }
            }
            else {
                sum += digit;
            }
            n = ++j;
        }
        return sum % 10 === 0;
    };
    return CCValidator;
}());
CCValidator.MIN_LENGTH = 14;
CCValidator._types = {
    // "amex" : { pattern: /^3[47]/, length: 15 }
    disc: { pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/, length: 16 },
    // , "dine" : { pattern: /^36/, length: 14 },
    visa: { pattern: /^4/, length: 16 },
    mast: { pattern: /^5[1-5]/, length: 16 }
};
var CardComponent = (function () {
    function CardComponent() {
        this.options = {};
        this.onSelect = new core.EventEmitter();
        this.onDelete = new core.EventEmitter();
    }
    /**
     * @return {?}
     */
    CardComponent.prototype.ngOnChanges = function () {
        var _this = this;
        setTimeout(function () {
            _this.cardType = CCValidator.getCardType(_this.data.card);
            _this.pan = format(inputMasks.pan)(_this.data.card);
        });
    };
    /**
     * @return {?}
     */
    CardComponent.prototype.select = function () {
        this.onSelect.emit(this.data);
    };
    /**
     * @return {?}
     */
    CardComponent.prototype.delete = function () {
        this.onDelete.emit(this.data);
    };
    return CardComponent;
}());
CardComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'app-card',
                template: "\n    <div class=\"card\">\n      <div\n        class=\"card-holder\"\n        [ngClass]=\"{\n          visa: cardType === 'visa',\n          mc: cardType === 'mast'\n        }\"\n      >\n        <div class=\"card-info\">\n          <div class=\"card-logo\"></div>\n          <span class=\"card-pan\">{{pan}}</span>\n        </div>\n\n        <div class=\"card-actions\">\n          <div class=\"card-action-select\" (click)=\"select()\">\n            <div class=\"card-action-icon-holder\">\n              <span class=\"mdi mdi-cart-plus\"></span>\n            </div>\n          </div>\n          <div class=\"card-action-delete\" (click)=\"delete()\">\n            <div class=\"card-action-icon-holder\">\n              <span class=\"mdi mdi-delete\"></span>\n            </div>\n          </div>\n        </div>\n      </div>\n      <p *ngIf=\"options.name\" class=\"card-name\">{{data.alias}}</p>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
CardComponent.ctorParameters = function () { return []; };
CardComponent.propDecorators = {
    'data': [{ type: core.Input },],
    'options': [{ type: core.Input },],
    'onSelect': [{ type: core.Output },],
    'onDelete': [{ type: core.Output },],
};
var SelectCardComponent = (function () {
    /**
     * @param {?} apiService
     */
    function SelectCardComponent(apiService) {
        this.apiService = apiService;
        this.cards = [];
        this.cardOptions = {
            actions: true,
            name: true
        };
        this.onAdd = new core.EventEmitter();
        this.onSelect = new core.EventEmitter();
        this.onDelete = new core.EventEmitter();
        this.fetch();
    }
    /**
     * @return {?}
     */
    SelectCardComponent.prototype.ngOnChanges = function () {
        var _this = this;
        setTimeout(function () { return _this.fetch(); });
    };
    /**
     * @return {?}
     */
    SelectCardComponent.prototype.add = function () {
        this.onAdd.emit();
    };
    /**
     * @param {?} card
     * @return {?}
     */
    SelectCardComponent.prototype.select = function (card) {
        this.onSelect.emit(card);
    };
    /**
     * @param {?} card
     * @return {?}
     */
    SelectCardComponent.prototype.delete = function (card) {
        this.onDelete.emit(card);
    };
    /**
     * @return {?}
     */
    SelectCardComponent.prototype.fetch = function () {
        var _this = this;
        this.apiService.getCardList()
            .subscribe(function (cards) { return _this.cards = cards; });
    };
    return SelectCardComponent;
}());
SelectCardComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'app-select-card',
                template: "\n    <div class=\"cards\">\n      <div class=\"card-block\" *ngFor=\"let card of cards\">\n        <app-card\n          [data]=\"card\"\n          [options]=\"cardOptions\"\n          (onSelect)=\"select($event)\"\n          (onDelete)=\"delete($event)\"\n        ></app-card>\n      </div>\n\n      <div class=\"card-block\" (click)=\"add()\">\n        <div class=\"card\">\n          <div class=\"card-holder add-card\">\n            <span class=\"mdi mdi-plus-circle add-card-button\"></span>\n            <div class=\"add-card-text\">\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u043A\u0430\u0440\u0442\u0443</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
SelectCardComponent.ctorParameters = function () { return [
    { type: ApiService, },
]; };
SelectCardComponent.propDecorators = {
    'onAdd': [{ type: core.Output },],
    'onSelect': [{ type: core.Output },],
    'onDelete': [{ type: core.Output },],
};
var DeleteCardComponent = (function () {
    /**
     * @param {?} apiService
     */
    function DeleteCardComponent(apiService) {
        this.apiService = apiService;
        this.formGroup = new forms.FormGroup({
            alias: new forms.FormControl(),
            pan: new forms.FormControl(),
            exp: new forms.FormControl()
        });
        this.formGroup = new forms.FormGroup({
            alias: new forms.FormControl(),
            pan: new forms.FormControl(),
            exp: new forms.FormControl()
        });
        this.onDelete = new core.EventEmitter();
        this.onCancel = new core.EventEmitter();
    }
    /**
     * @return {?}
     */
    DeleteCardComponent.prototype.ngOnChanges = function () {
        var _a = this.data, alias = _a.alias, card = _a.card, expiration = _a.expiration;
        this.formGroup.setValue({
            alias: alias,
            pan: card,
            exp: expiration
        });
    };
    /**
     * @return {?}
     */
    DeleteCardComponent.prototype.onSubmit = function () {
        var _this = this;
        this.apiService.deleteCard({ guid: this.data.guid })
            .subscribe(function (res) {
            _this.onDelete.emit(_this.data);
        });
    };
    /**
     * @return {?}
     */
    DeleteCardComponent.prototype.cancel = function () {
        this.onCancel.emit(this.data);
    };
    return DeleteCardComponent;
}());
DeleteCardComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'app-delete-card',
                template: "\n    <div class=\"card-form-holder\" *ngIf=\"data\">\n        <div class=\"card-holder single-card\">\n            <app-card [data]=\"data\"></app-card>\n        </div>\n\n        <form [formGroup]=\"formGroup\" (ngSubmit)=\"onSubmit()\">\n          <div class=\"inputs-wrapper\">\n            <div class=\"input-holder\">\n                <input type=\"text\" id=\"alias\" readonly formControlName=\"alias\" required>\n                <span class=\"mdi input-state\"></span>\n                <label for=\"alias\">{{'CARD.ALIAS' | translate}}</label>\n            </div>\n        \n            <div class=\"input-holder\">\n                <input\n                    type=\"text\"\n                    id=\"pan\"\n                    formControlName=\"pan\"\n                    readonly\n                    required\n                >\n                <span class=\"mdi input-state\"></span>\n                <label for=\"pan\">{{'CARD.PAN' | translate}}</label>\n            </div>\n        \n            <div class=\"input-holder\">\n                <input\n                    type=\"text\"\n                    id=\"exp\"\n                    formControlName=\"exp\"\n                    readonly\n                    required\n                >\n                <span class=\"mdi input-state\"></span>\n                <label for=\"exp\">{{'CARD.EXP' | translate}}</label>\n            </div>\n          </div>\n\n            <div class=\"description\">{{'DELETE_CARD.DESCRIPTION' | translate}}</div>\n        \n          <div class=\"button-holder\">\n            <button class=\"button cancel-button secondary\" type=\"button\" (click)=\"cancel()\">{{'DELETE_CARD.CANCEL_BUTTON' | translate}}</button>\n            <button class=\"button\" type=\"submit\">{{'DELETE_CARD.SUBMIT_BUTTON' | translate}}</button>\n          </div>\n        </form>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
DeleteCardComponent.ctorParameters = function () { return [
    { type: ApiService, },
]; };
DeleteCardComponent.propDecorators = {
    'data': [{ type: core.Input },],
    'onDelete': [{ type: core.Output },],
    'onCancel': [{ type: core.Output },],
};
/**
 * @param {?} control
 * @return {?}
 */
function creditCardValidator(control) {
    var /** @type {?} */ card = control.value.toString();
    if (card.length > 0) {
        // length test
        if (card.length < CCValidator.MIN_LENGTH) {
            return { 'minlength': true };
        }
        // general validation test
        if (!CCValidator.isValid(card)) {
            return { 'invalid': true };
        }
    }
}
var expDateRegExp = /^((0[1-9])|(1[0-2]))\/[0-9][0-9]$/;
/**
 * @param {?} control
 * @return {?}
 */
function expDateValidator(control) {
    var /** @type {?} */ exp = control.value.toString();
    if (exp.length > 0) {
        if (!expDateRegExp.test(exp)) {
            return { 'invalid': true };
        }
    }
}
var AddCardComponent = (function () {
    /**
     * @param {?} apiService
     */
    function AddCardComponent(apiService) {
        this.apiService = apiService;
        this.formGroup = new forms.FormGroup({
            alias: new forms.FormControl(),
            pan: new forms.FormControl(),
            exp: new forms.FormControl()
        });
        this.panMask = inputMasks.pan;
        this.expMask = inputMasks.exp;
        this.formGroup = new forms.FormGroup({
            alias: new forms.FormControl(),
            pan: new forms.FormControl('', [
                creditCardValidator
            ]),
            exp: new forms.FormControl('', [
                expDateValidator
            ])
        });
        this.onAdd = new core.EventEmitter();
        this.onCancel = new core.EventEmitter();
    }
    /**
     * @return {?}
     */
    AddCardComponent.prototype.onSubmit = function () {
        var _this = this;
        var _a = this.formGroup.value, alias = _a.alias, exp = _a.exp;
        var pan = this.pan;
        this.apiService.addCard({ alias: alias, exp: exp, pan: pan })
            .subscribe(function (res) {
            _this.onAdd.emit(_this.data);
        });
    };
    /**
     * @return {?}
     */
    AddCardComponent.prototype.cancel = function () {
        this.onCancel.emit();
    };
    return AddCardComponent;
}());
AddCardComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'app-add-card',
                template: "\n    <div class=\"card-form-holder\">\n      <form [formGroup]=\"formGroup\" (ngSubmit)=\"onSubmit()\">\n        <div class=\"inputs-wrapper\">\n          <div class=\"input-holder\">\n            <input type=\"text\" id=\"alias\" formControlName=\"alias\" required>\n            <span class=\"mdi input-state\"></span>\n            <label for=\"alias\">{{'CARD.ALIAS' | translate}}</label>\n          </div>\n    \n          <div class=\"input-holder\">\n            <input maxlength=\"19\" type=\"text\" id=\"pan\" formControlName=\"pan\" inputMask [mask]=\"panMask\" (valueChanges)=\"pan = $event\" required>\n            <span class=\"mdi input-state\"></span>\n            <label for=\"pan\">{{'CARD.PAN' | translate}}</label>\n          </div>\n    \n          <div class=\"input-holder\">\n            <input maxlength=\"5\" type=\"text\" id=\"exp\" formControlName=\"exp\" inputMask [mask]=\"expMask\" required>\n            <span class=\"mdi input-state\"></span>\n            <label for=\"exp\">{{'CARD.EXP' | translate}}</label>\n          </div>\n        </div>\n\n        <div class=\"description\">{{'ADD_CARD.DESCRIPTION' | translate}}</div>\n    \n        <div class=\"button-holder\">\n          <button class=\"button cancel-button secondary\" type=\"button\" (click)=\"cancel()\">{{'ADD_CARD.CANCEL_BUTTON' | translate}}</button>\n          <button class=\"button\" type=\"submit\" [disabled]=\"!formGroup.valid\">{{'ADD_CARD.SUBMIT_BUTTON' | translate}}</button>\n        </div>\n      </form>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
AddCardComponent.ctorParameters = function () { return [
    { type: ApiService, },
]; };
AddCardComponent.propDecorators = {
    'data': [{ type: core.Input },],
    'onAdd': [{ type: core.Output },],
    'onCancel': [{ type: core.Output },],
};
var TranslatePipe = (function () {
    /**
     * @param {?} localeService
     */
    function TranslatePipe(localeService) {
        this.localeService = localeService;
    }
    /**
     * @param {?} value
     * @param {...?} args
     * @return {?}
     */
    TranslatePipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return this.localeService.translate(value, args);
    };
    return TranslatePipe;
}());
TranslatePipe.decorators = [
    { type: core.Pipe, args: [{
                name: 'translate',
                pure: false
            },] },
];
/**
 * @nocollapse
 */
TranslatePipe.ctorParameters = function () { return [
    { type: LocaleService, },
]; };
// core
// module
// components
// pipe
// services
// modules
// config
var MOSST_CARD_MODULE_CONFIG = new core.InjectionToken('MOSST_CARD_MODULE_CONFIG');
var MosstCardModule = (function () {
    function MosstCardModule() {
    }
    /**
     * @param {?=} config
     * @return {?}
     */
    MosstCardModule.forRoot = function (config) {
        return {
            ngModule: MosstCardModule,
            providers: [
                {
                    provide: MOSST_CARD_MODULE_CONFIG,
                    useValue: config
                },
                {
                    provide: MosstCardModuleConfig,
                    useFactory: provideConfig,
                    deps: [MOSST_CARD_MODULE_CONFIG]
                },
                ConfigService
            ]
        };
    };
    /**
     * @return {?}
     */
    MosstCardModule.forChild = function () {
        return {
            ngModule: MosstCardModule
        };
    };
    return MosstCardModule;
}());
MosstCardModule.decorators = [
    { type: core.NgModule, args: [{
                imports: [
                    common.CommonModule,
                    forms.FormsModule,
                    forms.ReactiveFormsModule,
                    http.HttpModule,
                    InputMaskModule
                ],
                declarations: [
                    MosstCardComponent,
                    CardComponent,
                    SelectCardComponent,
                    DeleteCardComponent,
                    AddCardComponent,
                    TranslatePipe
                ],
                providers: [
                    ApiService,
                    LocaleService,
                    ConfigService
                ],
                exports: [
                    MosstCardComponent
                ]
            },] },
];
/**
 * @nocollapse
 */
MosstCardModule.ctorParameters = function () { return []; };
/**
 * @param {?} config
 * @return {?}
 */
function provideConfig(config) {
    return new MosstCardModuleConfig(config);
}

exports.MOSST_CARD_MODULE_CONFIG = MOSST_CARD_MODULE_CONFIG;
exports.MosstCardModule = MosstCardModule;
exports.provideConfig = provideConfig;
exports.MosstCardComponent = MosstCardComponent;
exports.ɵj = AddCardComponent;
exports.ɵg = CardComponent;
exports.ɵi = DeleteCardComponent;
exports.ɵh = SelectCardComponent;
exports.ɵf = MosstCardModuleConfig;
exports.ɵb = InputMaskDirective;
exports.ɵa = InputMaskModule;
exports.ɵk = TranslatePipe;
exports.ɵd = ApiService;
exports.ɵe = ConfigService;
exports.ɵc = LocaleService;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=mosst-card.umd.js.map
