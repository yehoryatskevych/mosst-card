import { EventEmitter, OnChanges } from '@angular/core';
import { ICard } from '../../interfaces';
import { ApiService } from '../../services/api.service';
export declare class SelectCardComponent implements OnChanges {
    private apiService;
    cards: ICard[];
    cardOptions: {
        actions: boolean;
        name: boolean;
    };
    onAdd: EventEmitter<void>;
    onSelect: EventEmitter<ICard>;
    onDelete: EventEmitter<ICard>;
    constructor(apiService: ApiService);
    ngOnChanges(): void;
    add(): void;
    select(card: ICard): void;
    delete(card: ICard): void;
    private fetch();
}
