import { OnChanges, EventEmitter } from '@angular/core';
import { ICard } from '../../interfaces';
export declare class CardComponent implements OnChanges {
    data: ICard;
    options: {
        actions?: boolean;
        name?: boolean;
    };
    onSelect: EventEmitter<ICard>;
    onDelete: EventEmitter<ICard>;
    cardType: string;
    pan: string;
    constructor();
    ngOnChanges(): void;
    select(): void;
    delete(): void;
}
