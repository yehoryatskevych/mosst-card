declare const _default: {
    en: {};
    ru: {
        DELETE_CARD: {
            TITLE: string;
            DESCRIPTION: string;
            SUBMIT_BUTTON: string;
            CANCEL_BUTTON: string;
        };
        ADD_CARD: {
            TITLE: string;
            DESCRIPTION: string;
            SUBMIT_BUTTON: string;
            CANCEL_BUTTON: string;
        };
        CARD: {
            ALIAS: string;
            PAN: string;
            EXP: string;
        };
    };
    uk: {};
};
export default _default;
