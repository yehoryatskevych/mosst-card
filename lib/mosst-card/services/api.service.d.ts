import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import { ConfigService } from './config.service';
import { ICard, IUser } from '../interfaces';
export declare class ApiService {
    private http;
    private configService;
    private apiHost;
    private user;
    constructor(http: Http, configService: ConfigService);
    setUser(user: IUser): void;
    getCardList(): Observable<ICard[]>;
    addCard(params: {
        alias: string;
        pan: string;
        exp: string;
    }): Observable<any>;
    deleteCard(params: {
        guid: string;
    }): Observable<any>;
    private request(params);
    private getAuthSign();
}
