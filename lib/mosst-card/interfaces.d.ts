export interface ICard {
    alias: string;
    bank_nm: string;
    card: string;
    expiration: string;
    expired: boolean;
    guid: string;
    mask: string;
    status: string;
}
export interface IMosstCardModuleConfig {
    api: {
        host: string;
        sign: {
            key: string;
            memberPointId: number;
        };
    };
}
export interface IUser {
    profile_guid: string;
    profile_session_guid: string;
}
